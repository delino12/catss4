@extends('layouts.group-skin')

@section('title')
    CATSS Live Feeds | Group Trading
@endsection

@section('contents')
<div id="trade_status"></div>
    <div class="popup-box chat-popup" id="qnimate">
      <div class="popup-head">
        <div class="popup-head-left pull-left">
          <i class="fa fa-user"></i> CATSS Agent 
          <a href="javascript:void(0);" style="font-size: 9px;" id="go-end"><i class="fa fa-arrow-down"></i> See new chat</a>
        </div>
        <div class="popup-head-right pull-right">
          <div class="btn-group">
          </div>
          <button data-widget="remove" id="removeClass" class="chat-header-button pull-right" type="button">
            <i class="glyphicon glyphicon-off"></i>
          </button>
        </div>
      </div>    
      <div class="popup-messages">
        <div class="direct-chat-messages">         
          <div id="chat-box">
            
            <div class="msg_msg"></div>
            <div id="end"></div>
          </div>
        </div>  
      </div>
      <div class="popup-messages-footer">
        <form id="chat-form">
          {{ csrf_field() }}
          <div class="form-group">
            <textarea id="msg" placeholder="Type a message..." rows="1" cols="40" class="form-control"></textarea>
          </div>
        <form>
      </div>
    </div>
    <div class="row small" style="margin-left: 17%; margin-right: 17%; font-size: 10px;">
      <div class="col-md-3" style="background-color: rgba(000,000,000,0.90);">
        <br />
        Trade Ticker (Buy)
        <p><i class="fa fa-arrow-up ticker-up"></i> increase in Equity Buy Price</p>
        <p><i class="fa fa-arrow-down ticker-down"></i> decrease in Equity Buy Price</p>
      </div>

      <div class="col-md-3" style="background-color: rgba(000,000,000,0.90);">
        <br />
        Trade Ticker (Sell)
        <p><i class="fa fa-arrow-up ticker-up"></i> increase in Equity Sell Price</p>
        <p><i class="fa fa-arrow-down ticker-down"></i> decrease in Equity Sell Price</p>
      </div>

      <div class="col-md-3" style="background-color: rgba(000,000,000,0.90);">
        <br />
        Trade Ticker (Equillibrum)
        <p><i class="fa fa-stop ticker-middle"></i> at in Break-even Point</p>
        <p><i class="fa fa-stop ticker-down"></i> Danger indication</p>
      </div>

      <div class="col-md-3" style="background-color: rgba(000,000,000,0.90);">
        <br />
        Trade Ticker (Others)
        <p><i class="fa fa-stop ticker-up"></i> at Profit Point</p>
        <p><i class="fa fa-stop" style="color: teal;"></i> Stock Status</p>
      </div>
    </div>

    <div class="header navbar-fixed-top fixed-sidebar">
      @if($group_info)
        @foreach($group_info as $info)
          <span class="ntrade-feeds"><i class="fa fa-cubes"></i> Group {{ $info->name }} </span>
          <hr />
          <h3><i class="fa fa-users"></i> Memebers</h3>
          <form>
            <input type="hidden" name="group_id" value="{{ $info->id }}">
          </form>
          <table class="table">
            <thead>
              <tr>
                <th>Name</th>
                <th>&#8358; Net worth</th>
              </tr>
            </thead>
            <tbody class="load-memebers"></tbody>
          </table>
          

          <script type="text/javascript">
            var groupId = $("input[name=group_id]").val();
            $.get('/load/group/info-screen/'+groupId, function (data){
              // console.log(data);
              $('.load-memebers').html('');
              $.each(data, function (index, value){
                var name = value.name;
                var splitName = name.split(" ");
                $('.load-memebers').append(`
                  <tr>
                    <td><i class="fa fa-user"></i> `+splitName[0]+`</td>
                    <td><a class="dino-link">`+value.net_worth+`</a></td>
                  </tr>
                `);
              });
            });
          </script>
        @endforeach
      @endif
      <h2>News Update</h2>
      <div class="load-news"></div>
    </div>

    <div class="header navbar-fixed-top fixed-sidebar-2">
      <h3><i class="fa fa-pin"></i>Balance &#8358; <span class="account_balance"></span> </h3>
      <hr />
      <div class="load-transactions dino-link"></div>

      <h3><i class="fa fa-pin"></i>Stock Balance &#8358; <span class="total_qty"></span> </h3>
      <hr />
      <div class="load-stocks dino-link small">
        <div class="load-stock-balance"></div>
      </div>
    </div>
    <br />
    <div style="background-color: #fff;color:#000;padding: 1em; position:absolute; box-shadow: 1px 1px 3px 1px #CCC; margin-left: 40%;z-index: 10;border-radius: 4px;">
      You are now trading with Group <i class="fa fa-cubes"></i> {{ $info->name }}
    </div>
    <div class="row small" style="margin-left: 17%; margin-right: 17%; font-size: 12px;">
      <div class="col-md-12" style="background-color: rgba(000,000,000,0.90);">
        <h3>
          <i class="fa fa-bar-chart"></i>
          Today's Equity Market
          <span class="pull-right">
            @if($group_info)
              @foreach($group_info as $info)
                <span class="ntrade-feeds"><i class="fa fa-cubes"></i> Group {{ $info->name }} </span><span>
              @endforeach
            @endif
          </span>
        </h3>
        
        <table class="table dino-link" id="pairs_table">
          <thead>
            <tr class="small">
            <th>Security</th>
            <th>(&#8358;) Open</th>
            <th>(&#8358;) Previous Close</th>
            <th><i class="fa fa-exclamation-triangle"></i> Gap %</th>
            <th>Status</th>
            <th>Request Qty</th>
            <th>Option</th>
            <th><i class="fa fa-money"></i> Trade</th>
            <th>Date</th>
          </thead>
          <tbody></tbody>
        </table>
      </div>
    </div>

    {{-- smart input --}}
    <!-- custom url scripts -->
    <script src="https://riliwanrabo.github.io/smartInput/smartinput.js"></script>

    <script type="text/javascript">
      $("#pairs_table tbody").html(
        '<h3 class="ml2">Loading......</h3>'
      );

      $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip(); 
      });

      // first load data
      $(document).ready(function(){
        $.get('/catss/trade', function (data){
          $("#catss-loading").hide();
          $("#pairs_table tbody").html("");
          $.each(data, function (index, value){
            var pid = value.id;
            var pn = value.security; // equity name
            var sp = value.close_price; // start price
            var cp = value.open_price; // close price
            var pp = value.previous_close; // close price
            var t_stat = value.status;
            var gap = value.change_price;
            var vol = value.daily_volume;
            var date = value.date;

            var stats;
            if(value.previous_close > value.close_price){
              stats = `<i class="fa fa-arrow-down text-danger"></i>`;
            }

            if(value.previous_close < value.close_price){
              stats = `<i class="fa fa-arrow-up text-success"></i>`;
            }

            if(value.previous_close == value.close_price){
              stats = `<i class="fa fa-stop ticker-middle"></i>`;
            }

          $("#pairs_table tbody").append('\
              <tr>\
                <td><a href="#" class="dino-link" data-toggle="tooltip" title="Net Price at '+value.real_price+'">'+pn+'</a></td>\
                <td>'+stats+' &#8358;'+value.real_price+'</td>\
                <td>&#8358;'+pp+'</td>\
                <td>'+gap+'</td>\
                <td>'+t_stat+'</td>\
                <td><input type="number" id="trade_qty" class="dino-input-live " placeholder="100000" required=""></td>\
                <td><select id="trade_type" class="dino-select-live"><option value="buy">buy</option><option value="sell">sell</option></select></td>\
                <td><button class="dino-button-live">Trade</button></td>\
                <td>'+date+'</td>\
                <input type="hidden" id="pair_id" value="'+pid+'" /><input type="hidden" id="pair_name" value="'+pn+'" />\
                <input type="hidden" id="trade_price" value="'+value.real_price+'" /><input type="hidden" id="sell_price" value="'+cp+'" />\
                <input type="hidden" id="token" value="{{ csrf_token() }}">\
              </tr>\
            ');
          });

          $('.dino-button-live').click(function(e){
            e.preventDefault();
            var that = $(this).parent().parent().find("#trade_qty");
            var token = $("#token").val();
            var pid   = $(this).parent().parent().find("#pair_id").val(); // pair id
            var pn    = $(this).parent().parent().find("#pair_name").val(); // pair name
            var tp    = $(this).parent().parent().find("#trade_price").val(); // trade_price start price
            var sp    = $(this).parent().parent().find("#sell_price").val(); // sell price close price
            var type  = $(this).parent().parent().find("#trade_type").val(); // type of trade


            var qty   = $(this).parent().parent().find("#trade_qty").smartInput(); // qty to trade

            // post trade
            $.ajax({
              type: "POST",
              url: "/request-trade",
              data: {
                _token:token,
                pid:pid,
                pn:pn,
                tp:tp,
                sp:sp,
                type:type,
                qty:qty
              },
              cache: false,
              success: function(data){
                $("#trade_status").html(data).show();
                that.val("");
                // console.log('trade request sent !');
                $("#trade_status").html(data).fadeOut(3000);
              }
            });
          });
          // console.log(data);
        });

        // account balance state
        $.get("/accountbalance", function (data){
          $("#ac").text(data.account_balance);
          $(".account_balance").text(data.account_balance);
        });
        // show news notifications
        $.get('/news/notifications', function (data){
          $.each(data, function (index, value){
            $(".load-news").append(
              '\
              <h3>'+value.news_title+'</h3>\
              <p>'+value.news_body+'</p>\
              <span class="small">'+value.news_date+'</span>\
              '
            );
          });
        });
        // show transactions notifications
        $.get('/transactions/notifications', function (data){
          $.each(data, function (index, value){
            var tradeType = value.stock_trade;
            var word;
            if(tradeType == 'buy'){
              word = 'Purchased';
            }
            if(tradeType == 'sell'){
              word = 'Solde';
            }

            $(".load-transactions").append(
              word+'<br /> <b>'+value.stock_qty+'</b> of '+value.stock_name+' for <b>&#8358; '+value.stock_amount+'</b> at &#8358;'+value.stock_unit+
              '<span class="pull-right small">'+value.stock_date+'</span> <br /><br />'
            );
          });
        });
      });

      var refreshSidebar = function (){
        // account balance state
        $.get("/accountbalance", function (data){
          $("#ac").text(data.account_balance);
          $(".account_balance").text(data.account_balance);
        });
        // show news notifications
        $.get('/news/notifications', function (data){
          $(".load-news").html("");
          $.each(data, function (index, value){
            $(".load-news").append(
              '\
              <h3 class="dino-link">'+value.news_title+'</h3>\
              <p>'+value.news_body+'</p>\
              <span class="small">'+value.news_date+'</span>\
              '
            );
          });
        });
        // show transactions notifications
        $.get('/transactions/notifications', function (data){
          $(".load-transactions").html("");
          $.each(data, function (index, value){
            var tradeType = value.stock_trade;
            var word;
            if(tradeType == 'buy'){
              word = 'Purchased';
            }
            if(tradeType == 'sell'){
              word = 'Sold';
            }

            $(".load-transactions").append('Last Updated Transaction <br />\
              <span class="small">'+word+'<br /> <b>'+value.stock_qty+'</b> '+value.stock_name+' at &#8358;'+value.stock_unit+
              ' worth <b>&#8358;'+value.stock_amount+'</b> <span class="pull-right small">'+value.stock_date+'</span> <br /><br /></span>'
            );
          });
        });

        // show stocks balance 
        $.get('/stocks/notifications', function (data){
          $(".load-stock-balance").html("");
          $.each(data, function (index, value){
            $(".load-stock-balance").append(`
                <tr>
                  <td style="padding:0.5em;">`+value.name+`</td>
                  <td style="padding:0.5em;">&#8358; `+value.price+`</td>
                  <td style="padding:0.5em;"><i class="fa fa-database"></i> `+value.qty+`</td>
                </tr>
            `);
          });  
        });
      }
      setInterval(refreshSidebar, 2000);
    </script>
@endsection
