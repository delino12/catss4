@extends('layouts.ntrade-skin')

@section('title')
    CATSS NSE Price List
@endsection

@section('contents')
    <div class="row" style="margin: 3px;">
      <div class="col-md-12" style="background-color: rgba(000,000,000,0.90);">
        <h3>
          <i class="fa fa-bar-chart"></i> Equities Market is Closed !!!, Market Open at 09:00 AM <i class="fa fa-spinner"></i>
          <span class="pull-right">
            <span class="ntrade-feeds"><i class="fa fa-user"></i> {{ Auth::user()->account_id }} </span>
          </span>
        </h3>
        
        <table class="table dino-link" id="pairs_table">
          <thead class="small" style="background-color: #000;">
            <tr>
              <th>Security</th>
              <th> Ref  (&#8358;)</th>
              {{-- <th> Open (&#8358;)</th> --}}
              <th> High (&#8358;)</th>
              <th> Low  (&#8358;)</th>
              <th> Close (&#8358;)</th>
              <th> Previous Close (&#8358;)</th>
              <th><i class="fa fa-exclamation-triangle"></i> Price</th>
              <th>Status</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody class="equity-row small"></tbody>
        </table>
      </div>
    </div>
    <div style="height: 200px;"></div>
    <script type="text/javascript">
      var refreshSidebar = function (){
        // account balance state
        $.get("/accountbalance", function (data){
          $("#ac").text(data.account_balance);
          $(".account_balance").text(data.account_balance);
        });
        // show news notifications
        $.get('/news/notifications', function (data){
          $(".load-news").html("");
          $.each(data, function (index, value){
            $(".load-news").append(
              '\
              <h3>'+value.news_title+'</h3>\
              <p>'+value.news_body+'</p>\
              <span class="small">'+value.news_date+'</span>\
              '
            );
          });
        });
        // show transactions notifications
        $.get('/transactions/notifications', function (data){
          $(".load-transactions").html("");
          $.each(data, function (index, value){
            var tradeType = value.stock_trade;
            var word;
            if(tradeType == 'buy'){
              word = 'Purchased';
            }
            if(tradeType == 'sell'){
              word = 'Sold';
            }

            $(".load-transactions").append('Last Updated Transaction <br />\
              '+word+'<br /> <b>'+value.stock_qty+'</b> of '+value.stock_name+' for <b>&#8358; '+value.stock_amount+'</b> at &#8358;'+value.stock_unit+
              '<span class="pull-right small">'+value.stock_date+'</span> <br /><br />'
            );
          });
        });

        // show stocks balance 
        $.get('/stocks/notifications', function (data){
          $(".load-stock-balance").html("");
          $.each(data, function (index, value){
            $(".load-stock-balance").append(`
                <tr>
                  <td style="padding:0.5em;">`+value.name+`</td>
                  <td style="padding:0.5em;">&#8358; `+value.price+`</td>
                  <td style="padding:0.5em;"><i class="fa fa-database"></i> `+value.qty+`</td>
                </tr>
            `);
          });  
        });
      }
      setInterval(refreshSidebar, 1000 * 10);
      $.get('/market/equity/live', function (data){
        console.log(data);
        $(".equity-row").html("");
        // iterate
        $.each(data, function (index, value){
          var stats;
          if(value.previous_close > value.close_price){
            stats = `<i class="fa fa-arrow-down text-danger"></i>`;
          }

          if(value.previous_close < value.close_price){
            stats = `<i class="fa fa-arrow-up text-success"></i>`;
          }

          if(value.previous_close == value.close_price){
            stats = `<i class="fa fa-stop text-info"></i>`;
          }

          $(".equity-row").append(`
            <tr>
              <td bgcolor="#000">`+value.security+`</td>
              <td>`+value.ref_price+`</td>
              
              <td>`+value.high_price+`</td>
              <td>`+value.low_price+`</td>
              <td>`+stats+` `+value.close_price+`</td>
              <td>`+value.previous_close+`</td>
              <td>`+value.change_price+`</td>
              <td><p class="text-danger">closed</p></td>
              <td><span>`+value.date+`</span></td>
            </tr>
          `);
        });
      });
    </script>
@endsection