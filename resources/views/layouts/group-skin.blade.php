
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> @yield('title') </title>
        <meta name="description" content="CATSS provide users a secure means to trade safe accross the Financial Market">
        <meta name="keywords" content="CATSS stock exchange market.">
        <meta name="author" content="Dipo, cavidel.com">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        
        <!-- Favicons
        ================================================== -->
        <link rel="shortcut icon" href="/img/favicon.png" type="image/x-icon">
        <link rel="apple-touch-icon" href="/img/favicon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon.png">

        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css"  href="/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="/font-awesome-4.2.0/css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="/css/jasny-bootstrap.min.css">

        <!-- Stylesheet
        ================================================== -->
        <link rel="stylesheet" type="text/css"  href="/css/style.css">
        <link rel="stylesheet" type="text/css" href="/css/responsive.css">

        <script type="text/javascript" src="/js/modernizr.custom.js"></script>
        <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
        <script type="text/javascript" src="/js/custom.js"></script>
        <link rel="stylesheet" type="text/css" href="/css/chat-css.css">
        <link rel="stylesheet" type="text/css" href="/css/trade-css.css">
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="/js/jquery.1.11.1.js"></script>
        <!-- Start of Async Drift Code -->
<script>
!function() {
  var t;
  if (t = window.driftt = window.drift = window.driftt || [], !t.init) return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0, 
  t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
  t.factory = function(e) {
    return function() {
      var n;
      return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
    };
  }, t.methods.forEach(function(e) {
    t[e] = t.factory(e);
  }), t.load = function(t) {
    var e, n, o, i;
    e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"), 
    o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js", 
    n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
  });
}();
drift.SNIPPET_VERSION = '0.3.1';
drift.load('xf9aiywc6shz');
</script>
<!-- End of Async Drift Code -->
    </head>
  <body>
    <div id="ntrade-home">
        <!--Simple nav bar-->
        <header class="header navbar-fixed-top">
            <nav class="navbar ntrade-nav" role="navigation" style="border-radius: 0px;">
                <div class="container" style="font-size: 12px;">
                    <div class="menu-container js_nav-item">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="toggle-icon"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse nav-collapse">
                        <div class="menu-container" >
               
                                <ul class="nav navbar-nav navbar-left">
                                    <li><a href="javascript:void(0);">
                                        <span class="text-success" style="font-weight: 900;">Account Bal: &#8358; <span id="ac"></span></span></a></li>
                                    <li><a href="#"><div id="trade_status"></div></a></li>
                                </ul>
                                <ul class="nav navbar-nav navbar-right">
                                    <li> <a class="dino-link" href="/open/trading/pad"> <i class="fa fa-line-chart"></i> Real-Time Market</a></li>
                                    <li> <a class="dino-link" href="/dashboard"> <i class="fa fa-sign-out"></i> Exit {{ $info->name }}</a></li>
                                </ul>
                            
                        </div>
                    </div>
                </div>
            </nav>
        </header>
        @if(session('trade_status'))
            <center>
                <div class="alert alert-success" style="width: 40%;">
                    <p>
                      {{ session('trade_status') }}
                      click <a href="/transactions/logs"> view transactions</a> to see details
                    </p>
                </div>
            </center>   
        @endif
        
        <br />
        <div id="top"></div>
        <!-- Contents Section -->
        @yield('contents')
        
    </div>
    <br /><br /><br />

    <header class="header navbar-fixed-bottom">
        <nav class="navbar" style="background-color: rgba(000,000,000,0.95); color:#FFFFFF;border-radius: 0px;" role="navigation">

            <div class="collapse navbar-collapse nav-collapse">
                <div class="menu-container" >
                    <div class="top-nav" align="left">
                        Stocks News:
                        <marquee direction="right" speed="slow">
                            <div class="slide-stock">
                                <span class="stock-index"></span>
                            </div>
                        </marquee> 
                    </div>
                </div>
            </div>
            
        </nav>
    </header>

    <script type="text/javascript">
        var showIndex = function (){
            $.get('/load/market/index', function (data){
                $(".stock-index").html("");
                $.each(data, function (index, value){
                    var op = value.close;
                    var cp = value.open;
                    var diff;
                    if(op > cp){
                        op = ' &#8358; '+op+'<span class="text-danger"><i class="fa fa-angle-down"></i></span> ';
                        cp = ' &#8358; '+cp+'<span class="text-success"><i class="fa fa-angle-up"></i></span> ';
                    }else{
                        op = ' &#8358; '+op+'<span class="text-success"><i class="fa fa-angle-up"></i></span> ';
                        cp = ' &#8358; '+cp+'<span class="text-danger"><i class="fa fa-angle-down"></i></span> '; 
                    }

                    $(".stock-index").append(`
                      <span class="index-news">
                         <span class="dino-link">`+value.pairs+` </span> `+op+`
                      </span>
                    `);
                });
            });
        };
        // refresh index
        setInterval(showIndex, 1000 * 2);
    </script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="/js/bootstrap.js"></script>
</body>
</html>