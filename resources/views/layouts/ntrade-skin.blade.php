
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> @yield('title') </title>
        <meta name="description" content="CATSS provide users a secure means to trade safe accross the Financial Market">
        <meta name="keywords" content="CATSS stock exchange market.">
        <meta name="author" content="Cavidel">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <link rel="shortcut icon" href="/img/favicon.png" type="image/x-icon">
        
        <!-- Favicons
        ================================================== -->
        <link rel="shortcut icon" href="/img/favicon.png" type="image/x-icon">
        <link rel="apple-touch-icon" href="/img/favicon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon.png">

        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css"  href="/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="/font-awesome-4.2.0/css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="/css/jasny-bootstrap.min.css">

        <!-- Stylesheet
        ================================================== -->
        <link rel="stylesheet" type="text/css"  href="/css/style.css">
        <link rel="stylesheet" type="text/css" href="/css/responsive.css">
        <link rel="stylesheet" type="text/css" href="/css/chat-css.css">
        <link rel="stylesheet" type="text/css" href="/css/trade-css.css">
        
        {{-- custom scripts --}}
        <script type="text/javascript" src="/js/modernizr.custom.js"></script>
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="/js/jquery.1.11.1.js"></script>
        <script type="text/javascript" src="/js/custom.js"></script>
        <!-- Start of Async Drift Code -->

        <link rel="stylesheet" type="text/css" href="{{asset("css/iziToast.css")}}">
        <script src="{{asset("js/iziToast.js")}}"></script>

        {{-- introJs --}}
        <link rel="stylesheet" type="text/css" href="/intro-assets/introjs.css">
        <script type="text/javascript" src="/intro-assets/intro.js"></script>

        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js"></script>

        {{-- drift chats --}}
        <script>
            !function() {
              var t;
              if (t = window.driftt = window.drift = window.driftt || [], !t.init) return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0, 
              t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
              t.factory = function(e) {
                return function() {
                  var n;
                  return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
                };
              }, t.methods.forEach(function(e) {
                t[e] = t.factory(e);
              }), t.load = function(t) {
                var e, n, o, i;
                e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"), 
                o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js", 
                n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
              });
            }();
            drift.SNIPPET_VERSION = '0.3.1';
            drift.load('xf9aiywc6shz');
        </script>
        <!-- End of Async Drift Code -->
    </head>
  <body>
    <div id="ntrade-home">
        <!--Simple nav bar-->
        <nav class="navbar ntrade-nav navbar-fixed-top" role="navigation" style="border-radius: 0px;">
            <div class="menu-container js_nav-item">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="toggle-icon"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse nav-collapse">
                <div class="container">
                    <div class="menu-container" >
                        <div class="top-nav" align="right">
                            <ul class="nav navbar-nav navbar-left">
                                <li>
                                    <a href="javascript:void(0);">
                                    <span class="text-success" style="font-weight: 900;">Account Bal: &#8358; <span id="ac"></span></span></a>
                                </li>
                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <a class="dino-link" href="/transactions"><i class="fa fa-bank"></i> <span class="dino-link">Transactions</span></a>
                                </li>
                                <li>
                                    <a class="dino-link" href="/stocks"><i class="fa fa-area-chart"></i> <span class="dino-link">Stocks</span></a>
                                </li>
                                <li>
                                    <a class="dino-link" href="/open/trading/bonds">
                                        <i class="fa fa-money"></i> Bonds Market
                                    </a>
                                </li>
                                <li>
                                    <a class="dino-link" href="/open/trading/tbills">
                                        <i class="fa fa-money"></i> Tbills Market
                                    </a>
                                </li>
                                <li>
                                    <a class="dino-link" href="/dashboard"> <i class="fa fa-sign-out"></i> Exit to Dashboard</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <nav class="navbar navbar-fixed-top index-news" style="margin-top: -7px;">
            <marquee direction="right" speed="slow">
                <div class="slide-stock">
                    <span class="stock-index"></span>
                </div>
            </marquee> 
        </nav>
        <br />
        {{-- <div id="top"></div> --}}
        <!-- Contents Section -->
        @yield('contents')
        
    </div>
    <br /><br /><br />

    <header class="header navbar-fixed-bottom">
        
    </header>

    <script type="text/javascript">
        $(document).ready(function(e) {

            $.get('/load/market/index', function (data){
                $(".stock-index").html("");
                $.each(data, function (index, value){
                    var op = value.close;
                    var cp = value.open;
                    var diff;
                    if(op > cp){
                        op = ' &#8358; '+op+'<span class="text-danger"><i class="fa fa-angle-down"></i></span> ';
                        cp = ' &#8358; '+cp+'<span class="text-success"><i class="fa fa-angle-up"></i></span> ';
                    }else{
                        op = ' &#8358; '+op+'<span class="text-success"><i class="fa fa-angle-up"></i></span> ';
                        cp = ' &#8358; '+cp+'<span class="text-danger"><i class="fa fa-angle-down"></i></span> '; 
                    }

                    $(".stock-index").append(`
                      <span class="index-news">
                         <span class="dino-link">`+value.pairs+` </span> `+op+`
                      </span>
                    `);
                });
            });


            // check if first time logging !
            var page   = 'click-trade';
            var userid = '{{ Auth::user()->id }}';

            let data = {
                page:page,
                userid:userid
            };

            $.get('/get/user/lesson', data, function(data) {
                // console.log(data);
                if(data.status == 'unseen'){
                }
            });

            // body...
            introJs().start();
        });
    </script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="/js/bootstrap.js"></script>
</body>
</html>