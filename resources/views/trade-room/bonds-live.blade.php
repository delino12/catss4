@extends('layouts.market-skin')

@section('title')
  CATSS Live | Bonds
@endsection

@section('contents')
<div class="container-fluid">
  <div class="row">

    <div class="col-md-2">
      <div class="c-card">
        {{-- simulation section --}}
        <br />
        <p class="text-center">My Transaction</p>
        {{-- <img src="/svg/catss-loading-transactions.svg" class="loading-transactions"> --}}
        <table class="table small">
          <thead>
            <tr>
              <th>Assets</th>
              <th>Qty</th>
            </tr>
          </thead>
          <tbody class="load-transactions-users"></tbody>
        </table>
      </div>
    </div>

    <div class="col-md-7">
      <div class="c-card">
        <table class="table">
          <thead>
            <tr>
              <th>Assets (Securities)</th>
              <th>Price (&#8358;)</th>
              <th>Yield (Y)</th>
              <th>Maturity</th>
              <th><i class="fa fa-database"></i> Quantity</th>
              <th><i class="fa fa-money-bill"></i> Buy</th>
              <th><i class="fa fa-money-bill"></i> Sell</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody class="load-bonds">
            <tr>
              <td><img src="/svg/catss-loading.svg"></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>

    <div class="col-md-3">
      {{-- simulation section --}}
      <div class="c-card simulate-title" style="display: none;">
        {{-- simulation section --}}
        <br />
        <p class="text-center dino-link s-title"></p>
        <table class="table small">
          <thead>
            <tr>
              <td>Details</td>
              <td>Revaluation</td>
            </tr>
          </thead>
          <tbody class="show-asset-revaluation"></tbody>
        </table>
      </div>

      <div class="c-card">
        {{-- simulation section --}}
        <br />
        <p class="text-center">Last Updated Transaction</p>
        <table class="table small">
          <thead>
            <tr>
              <th>Assets</th>
              <th>Price</th>
              <th>Qty</th>
            </tr>
          </thead>
          <tbody class="load-transactions"></tbody>
        </table>
      </div>

      <div class="c-card">
        <hr />
        <div class="load-headlines container-fluid"><img src="/svg/catss-loading-news.svg"></div>
      </div>
    </div>
  </div>

  {{-- modal popup section --}}
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-body">
          <div class="success_msg"></div>
          <div class="error_msg"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  // init document
  $(document).ready(function (){
    loadMarketPrices();
    fetchNews();
    // loadTransactions();
    loadTransactionsAll();
    fetchSummaryBonds();
  });

  // fetch market prices
  function loadMarketPrices() {
    $.get('/fetch/market/prices/bonds', function(data) {
      // console.log(data);
      $(".load-bonds").html("");
      $.each(data, function(index, val) {
        /* iterate through array or object */
        $(".load-bonds").append(`
          <tr>
            <td class="dino-link">${val.Description}</td>
            <td class="dino-link">${val.ClosingPrice}</td>
            <td class="dino-link">${val.Yield}</td>
            <td class="dino-link">${val.Maturity}</td>
            <td class="dino-link">
              <input type="text" onkeyup="formatInputs(${val.ClosingPriceRef})" class="dino-input-live" placeholder="10000" id="qty_${val.ClosingPriceRef}" />
            </td>
            <td class="dino-link">
              <a href="javascript:void(0);" onclick="buySecurity(${val.ClosingPriceRef})" class="btn btn-primary">Buy</a></td>
            <td class="dino-link">
              <a href="javascript:void(0);" 
              onclick="sellSecurity(${val.ClosingPriceRef})" class="btn btn-danger">Sell</a></td>
            <td class="text-info">${val.TradeDate}</td>
          </tr>

          <input type="hidden" id="security_${val.ClosingPriceRef}" value="${val.Description}">
          <input type="hidden" id="price_${val.ClosingPriceRef}" value="${val.ClosingPrice}">
          <input type="hidden" id="maturity_${val.ClosingPriceRef}" value="${val.Maturity}">
          <input type="hidden" id="yield_${val.ClosingPriceRef}" value="${val.Yield}">
        `);
      });
    });
  }

  // fetch bloomberg
  function fetchNews() {
    // dino get data
    $.get('/load/api-news/bloomberg', function(data) {
      // console.log(data);
      let sn = 0;
      $(".load-headlines").html("");
      $.each(data.articles, function(index, val) {
        // console.log(val);
        sn++;
        $(".load-headlines").append(`
          <div class="row">
            <div class="col-xs-3">
              <img src="`+val.urlToImage+`" width="100%" height="auto">
            </div>
            <div class="col-xs-9">
              <span class="dino-link">`+val.title+`</span><br />
              <p>`+val.description+` <a class="c-title" target="_blank" href="`+val.url+`">read more</a></p>
            </div>
          </div>
          <br />
        `);
        if(sn == 10){
          return false;
        }
      });
    });
  }

  // compute assets 
  function computeAsset(price, refid, assets) {
    // body...
    $(".simulate-title").show();

    $(".s-title").html(`
      CATSS is simulating (<b>${assets}<b>)
    `);

    const quantity = $("#qty_"+refid).val();
    const consideration = price * quantity;

    // console.log(price);
    // console.log(refid);
    $(".show-asset-revaluation").html(`
      <tr class="dino-link">
        <td>Consideration</td>
        <td>&#8358;${numeral(consideration).format(0,0)}</td>
      </tr>
      <tr class="dino-link">
        <td>Commission (1.35%)</td>
        <td>&#8358;</td>
      </tr>
      <tr class="dino-link">
        <td>--- (0.3%)</td>
        <td>&#8358;</td>
      </tr>
      <tr class="dino-link">
        <td>--- (0.3%)</td>
        <td>&#8358;</td>
      </tr>
      <tr class="dino-link">
        <td>--- (0.75%)</td>
        <td>&#8358;</td>
      </tr>
      <tr class="dino-link">
        <td>Estimated Total</td>
        <td><b>&#8358;${numeral(consideration).format(0,0)}</b></td>
      </tr>
    `);
  }

  // buy security
  function buySecurity(refid) {
    // body...
    var token     = '{{ csrf_token() }}';
    var security  = $("#security_"+refid).val();
    var qty       = $("#qty_"+refid).val();
    var price     = $("#price_"+refid).val();
    var maturity  = $("#maturity_"+refid).val();
    var secYield  = $("#yield_"+refid).val();

    // validate quantity
    if(qty == ""){
      $(".success_msg").html("");
      $(".error_msg").html(`
        <span class="text-danger">
          Type in the quantity you want to purchase !<br />
        </span>
      `);
      $('#myModal').modal('show');
      return false;
    }

    // init data
    var data = {
      _token: token,
      security:security,
      refid:refid,
      qty:qty,
      price:price,
      maturity:maturity,
      secYield:secYield,
      option: 'buy'
    };

    // post data
    $.post('/process/trade/bonds', data, function(data, textStatus, xhr) {
      /*optional stuff to do after success */
      if(data.status == 'success'){
        // alert(data.message);
        // alert(data.message);
        $(".success_msg").html(`
          <span class="text-success">
            ${data.message}
          </span>
        `);
        $(".error_msg").html("");
        $('#myModal').modal('show');

        // refresh transaction and account balance
        // loadTransactions();
        fetchSummaryBonds();
        loadTransactionsAll();
        displayAccountBalance();

        $(".simulate-title").hide();
        // return qty rack to empty
        $("#qty_"+refid).val("");
      }

      if(data.status == 'error'){
        // alert(data.message);
        $(".error_msg").append(`
          <span class="text-danger">
            ${data.message}
          </span>
        `);
        $(".success_msg").html("");
        $('#myModal').modal('show');
      }
    });
  }

  // sell security
  function sellSecurity(refid) {
    // body...
    // body...
    var token     = '{{ csrf_token() }}';
    var security  = $("#security_"+refid).val();
    var qty       = $("#qty_"+refid).val();
    var price     = $("#price_"+refid).val();
    var maturity  = $("#maturity_"+refid).val();
    var secYield  = $("#yield_"+refid).val();

    // validate quantity
    if(qty == ""){
      $(".success_msg").html("");
      $(".error_msg").html(`
        <span class="text-danger">
          Type in the quantity you want to purchase !<br />
        </span>
      `);
      $('#myModal').modal('show');
      return false;
    }

    // init data
    var data = {
      _token: token,
      security:security,
      refid:refid,
      qty:qty,
      price:price,
      maturity:maturity,
      secYield:secYield,
      option: 'sell'
    };

    // post data
    $.post('/process/trade/bonds', data, function(data, textStatus, xhr) {
      /*optional stuff to do after success */
      if(data.status == 'success'){
        // alert(data.message);
        // alert(data.message);
        $(".success_msg").html(`
          <span class="text-success">
            ${data.message}
          </span>
        `);
        $(".error_msg").html("");
        $('#myModal').modal('show');
        
        // refresh transaction and account balance
        // loadTransactions();
        fetchSummaryBonds();
        loadTransactionsAll();
        displayAccountBalance();

        $(".simulate-title").hide();
        // return qty rack to empty
        $("#qty_"+refid).val("");
      }

      if(data.status == 'error'){
        // alert(data.message);
        $(".success_msg").html("");
        $(".error_msg").html(`
          <span class="text-danger">
            ${data.message} <br />
            ${data.balance}
          </span>
        `);
        $('#myModal').modal('show');
      }
    });
  }

  // load transactions
  function loadTransactions() {
    $.get('/load/user/stock/bonds', function(data) {
      // console.log(data);
      $(".load-transactions-users").html("");
      $.each(data, function(index, val) {
        $(".load-transactions-users").append(`
          <tr>
            <td>${val.security}</td>
            <td>${val.price}</td>
            <td>${val.qty}</td>
          </tr>
        `);
      });
      $(".loading-transactions").hide();
    });
  }

  // load all transactions
  function loadTransactionsAll() {
    $.get('/load/all/stock/bonds', function(data) {
      // console.log(data);
      $(".load-transactions").html("");
      $.each(data, function(index, val) {
        let diffColor = "";
        if(val.TransactionTypeID == 1){
          diffColor = 'text-success';
        }else{
          diffColor = 'text-danger';
        }

        $(".load-transactions").append(`
          <tr class="${diffColor}">
            <td>${val.security}</td>
            <td>${val.price}</td>
            <td>${val.qty}</td>
          </tr>
        `);
      });
      $(".loading-transactions").hide();
    });
  }

  // load account balance
  function displayAccountBalance() {
    // account balance state
    $.get("/accountbalance", function (data){
        $("#ac").html(data.account_balance);
        $(".account_balance").html(data.account_balance);
    });
  }

  // format inputs
  function formatInputs(item_ref) {
    // body...
    var qty = $("#qty_"+item_ref).val();

    // format input value
    $("#qty_"+item_ref).val(numeral(qty).format(0,0));
  }

  // fetch summary 
  function fetchSummaryBonds() {
    $.get('/load/summary/stock/bonds', function(data) {
      // console.log(data);
      $(".load-transactions-users").html("");
      $.each(data, function(index, val) {
        $(".load-transactions-users").append(`
          <tr>
            <td>${val.security}</td>
            <td>${numeral(val.qty).format(0,0)}</td>
          </tr>
        `);
      });
    });
  }

</script>
@endsection
