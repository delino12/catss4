@extends('layouts.market-skin')

@section('title')
  CATSS Live | TBills
@endsection

@section('contents')

<style type="text/css">
  #rss-feeds ul li {
    list-style: none;
    color:#509060;
    padding: 1em;
  }

  #rss-feeds ul li a{
    list-style: none;
    color: #ECB;
    padding: 1em;
  }
</style>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-2">
      <div class="c-card">
        {{-- simulation section --}}
        <br />
        <p class="text-center">My Transaction</p>
        {{-- <img src="/svg/catss-loading-transactions.svg" class="loading-transactions"> --}}
        <table class="table small">
          <thead>
            <tr>
              <th>Assets</th>
              <th>Qty</th>
            </tr>
          </thead>
          <tbody class="load-transactions-users"></tbody>
        </table>
      </div>
    </div>

    <div class="col-md-7">
      <div class="c-card">
        <table class="table">
          <thead>
            <tr>
              <th>
                <select class="form-control" id="base">
                  <option value="EUR">EUR</option>
                  <option value="USD">USD</option>
                  <option value="NGN">NGN</option>
                </select>
              </th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
              <th></th>
            </tr>
            <tr>
              <th><i class="fa fa-file"></i> Symbols</th>
              <th class="text-success">Rate</th>
              <th>Spread</th>
              <th>High</th>
              <th class="text-success">Buy</th>
              <th class="text-danger">Sell</th>
              <th></th>
            </tr>
          </thead>
          <tbody class="load-fx-u2u"></tbody>
        </table>
      </div>
    </div>

    <div class="col-md-3">
      <div class="c-card">
        {{-- simulation section --}}
        <br />
        <p class="text-center">Last Updated Transaction</p>
        {{-- <img src="/svg/catss-loading-transactions.svg" class="loading-transactions"> --}}
        <table class="table small">
          <thead>
            <tr>
              <th>Pairs</th>
              <th class="text-success">Bid</th>
              <th class="text-danger">Ask</th>
            </tr>
          </thead>
          <tbody class="load-orders"></tbody>
        </table>
      </div>
      <div class="c-card">
        <hr />
        <div id="rss-feeds"></div>
      </div>
    </div>
  </div>

  {{-- modal popup section --}}
  <!-- Modal for Biding -->
  <div class="display-modal">
    <div class="modal fade" id="pop_trade_form" role="dialog">
      <div class="modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-body">
            <form method="post" onsubmit="return false">
              <input type="hidden" id="security" value="">
              <a href="javascript:void(0);" class="btn btn-default pull-right" data-dismiss="modal">close</a>
              <div class="col-sm-12"><span class="lead text-info sec-title-a1"></span></div>
              <div class="col-sm-6">
                <h1 class="lead text-success buy-pair">Buy</h1>
                <div class="form-group">
                  <input type="number" min="1" max="9999" step=any maxlength="4" class="form-control" id="bid-price-a1" placeholder="price" required>
                </div>
                <div class="form-group">
                  <input type="number" min="1" max="9999" step=any maxlength="18" class="form-control" id="bid-qty-a1" placeholder="Enter qty">
                </div>
              </div>
              <div class="col-sm-6">
                <h1 class="lead text-danger sell-pair">Sell</h1>
                <div class="form-group">
                  <input type="number" min="1" max="9999" step=any maxlength="4" class="form-control" id="ask-price-a1" placeholder="price" required>
                </div>
                <div class="form-group">
                  <input type="number" min="1" max="9999" step=any maxlength="18" class="form-control" id="ask-qty-a1" placeholder="Enter qty">
                </div>
              </div>

              <div class="col-sm-12">
                <div class="form-group">
                  <button class="btn btn-primary col-md-12 text-center" id="place-order">
                    <span class="option">Place Order</span>
                  </button>
                  <div style="height: 20px;"></div>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-rss/3.3.0/jquery.rss.js"></script>
<script type="text/javascript">
  // init document
  $(document).ready(function (){
    // loadAllFxSymbols();
    loadBaseFxSymbols();
    fetchRssNewsFeeds();
    fetchFxOrders();
    // fetchSummaryTbills();
  });

  // load symbol base exchange 
  function loadBaseFxSymbols() {
    // body...
    $.get('/load/fx/symbols/base', function (data){
      // console.log(data);
      var sn = 0;

      $(".load-fx-u2u").html("");
      $.each(data.rates, function (index, val){
        sn++;

        // console.log(val);
        const spread  = 0.023 * val;
        const high    = 0.056 * val;
        const buy     = val + (0.08/val);
        const sell    = val - (0.07/val);

        // init random

        // display trade data
        $(".load-fx-u2u").append(`
          <tr>
            <td class="dino-link">EUR/${index}</td>
            <td>${val}</td>
            <td>${numeral(spread).format('0.00')}</td>
            <td>${numeral(high).format('0.000')}</td>
            <td>
              <button class="dino-input text-success" onclick="placeOrderRequest(${sn})">
                ${numeral(buy).format('0.000')}
              </button>
            </td>
            <td>
              <button class="dino-input text-danger" onclick="placeOrderRequest(${sn})">
                ${numeral(sell).format('0.000')}
              </button>
            </td>
          </tr>
          <input type="hidden" id="pairs_${sn}" value="${index}">
          <input type="hidden" id="buy_${sn}" value="${buy}">
          <input type="hidden" id="sell_${sn}" value="${sell}">
        `);
      });
    });
  }

  // load RSS news feeds
  function fetchRssNewsFeeds() {
    // body...
    // $(".load-headlines").rss('https://rss.dailyfx.com/feeds/forex_market_news', function(data){
    //   console.log(data);
    // });
    $(function($) {
      $("#rss-feeds").rss("https://rss.dailyfx.com/feeds/forex_market_news")
    });
  }

  // load all orders
  function fetchFxOrders() {
    $.get('/load/fx/orders', function (data){
      // console.log(data);
      $(".load-orders").html("");
      $.each(data, function(index, val){
        console.log(val);
        $(".load-orders").append(`
          <tr>
            <td>EUR/${val.currency}</td>
            <td><i class="fa fa-arrow-up text-success"></i> ${val.bid}</td>
            <td><i class="fa fa-arrow-down text-danger"></i> ${val.ask}</td>
          </tr>
        `);
      });
    });
  }

  // function place order
  function placeOrderRequest(sn) {
    // body...
    var pair  = $("#pairs_"+sn).val();
    var buy   = $("#buy_"+sn).val();
    var sell  = $("#sell_"+sn).val();

    // set name
    $("#security").val(pair);

    // body...
    $("#bid-price-a1").val(numeral(buy).format('0.000'));
    $("#ask-price-a1").val(numeral(sell).format('0.000'));

    // $("#bid-qty-a1").val(bid_qty);
    // $("#ask-qty-a1").val(ask_qty);

    $(".sec-title-a1").html(pair);
    $('#pop_trade_form').modal('show');
  }

  // post order
  $("#place-order").click(function (e){
    e.preventDefault(e);

    var token     = '{{ csrf_token() }}';
    var bidPrice  = $("#bid-price-a1").val();
    var bidQty    = $("#bid-qty-a1").val();
    var askPrice  = $("#ask-price-a1").val();
    var askQty    = $("#ask-qty-a1").val();
    var security  = $("#security").val();

    // option
    var params = {
      _token: token,
      security: security,
      bidPrice: bidPrice,
      bidQty: bidQty,
      askPrice: askPrice,
      askQty: askQty  
    };

    // place order
    $.post('/place/new/fx/order', params, function(data){
      // console.log(data);
      if(data.status == 'success'){
        loadBaseFxSymbols();
        fetchFxOrders();
        $("#pop_trade_form").modal('hide');
      }
    });
  });
</script>
@endsection
