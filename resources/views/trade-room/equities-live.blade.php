@extends('layouts.ntrade-skin')

@section('title')
    CATSS Live Feeds
@endsection

@section('contents')
  <style type="text/css">
    .eq-calculator {
      height: auto;
      display: none;
    }
  </style>
  <div style="height: 85px;"></div>

  {{-- news updates sidebar --}}
  <div class="header navbar-fixed-top fixed-sidebar" style="box-shadow: 1px 1px 14px 1px #999;">
    {{-- calculator div --}}
    <div class="eq-calculator"></div>
    <hr />
    <h3 class="dino-link"><i class="fas fa-volume-on"></i> News Updates</h3>
    <div class="load-news" id="load-flash-news"></div>
    {{-- rules --}}
    <div class="rule-list" id="flash-rules" style="display:none;padding: 0.6em;">
      <ul>
        <li class="rule-li"><img src="/img/rules.gif"> Trading Rules</li>
        <li class="rule-li">1.  Each N1,000.00 subscription credits account with N10million Valid for four weeks.</li>
        <li class="rule-li">2.  Additional N1,000.00 subscribers extends validity by another 4 weeks.</li>
        <li class="rule-li">3.  Account is reset to N10million every Monday for a new trading week.</li>
        <li class="rule-li">4.  Valid subscription are eligible to win weekly prizes.</li>
        <li class="rule-li">5.  Number of winners are determine by the number of subscribers up to a minimum of (3) three.</li>
        <li class="rule-li">6.  All top winner get weekly prizes.</li>
        {{-- <li class="rule-li">7.  Cash prizes credited to winner’s Bank Account on Saturday.</li> --}}
        <li class="rule-li">7.  Every purchase and sale prices includes commission charge.</li>
        <li class="rule-li">8.  Equities Market prices are update daily.</li>
        <li class="rule-li">9.  Stock balances are revalued each time there is a change in market price.</li>
        <li class="rule-li">10. Price changes within the CATSS Market are randomnized.</li>
      </ul>
    </div>
  </div>

  {{-- load transaction updates sidebar --}}
  <div class="header navbar-fixed-top fixed-sidebar-2" style="box-shadow: 1px 1px 14px 1px #000;">
    <div id="trade_status"></div>
    <div class="load-transactions dino-link" data-step="1" data-intro="Tips: display last transaction" data-position='right'></div>
    <h3>
      <i class="fa fa-database"></i> <span class="total_qty"></span> <span data-step="2" data-intro="Tips: display stock balance" data-position='right'>Stocks</span>
    </h3>
    <hr />
    <div class="load-stocks dino-link small">
      <div class="load-stock-balance"></div>
    </div>
  </div>

  <div class="row small" style="margin-top:-120px;margin-left:17%;margin-right:17%;font-size:12px;box-shadow: 1px 1px 14px 1px #000;">
    <div class="col-md-12" style="background-color: rgba(000,000,000,0.90);">
      <h3>
        <i class="fa fa-bar-chart"></i>
        Today's Equity Market <button class="rule btn btn-danger">Read Me (News/Rules)</button>
        <span class="pull-right">
          <span class="text-danger">Note:</span>  1.35% Commission fee is charged on transactions
        </span>
      </h3>

      <table class="table dino-link" id="pairs_table">
        <thead>
          <th><span data-step="3" data-intro="Tips: securities list table" data-position='right'>Security</span></th>
          <th>(&#8358;) Open</th>
          <th>(&#8358;) Previous Close</th>
          <th><i class="fa fa-exclamation-triangle"></i> Gap %</th>
          <th>Status</th>
          <th><span data-step="4" data-intro="input quantity for trade" data-position='right'>Request Qty</span></th>
          <th><span data-step="5" data-intro="use the iBuy button to purchase assets" data-position='right'><i class="fa fa-money"></i> iBuy</span></th>
          <th><span data-step="6" data-intro="use the iSell button to sell assets" data-position='left'><i class="fa fa-money"></i> iSell</th>
          <th>Date</th>
        </thead>
        <tbody></tbody>
      </table>
    </div>
  </div>

  <!-- pusher -->
  <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
  <script type="text/javascript">
    // show rules
    $('.rule').click(function (e){
      e.preventDefault();
      $('.rule-list').toggle();
      $('.load-news').toggle();
      $('.eq-calculator').toggle();
      $('#addClass').toggle();
      // alert('yes');
    });

    var user_id = '{{ Auth::user()->id }}';
    var logged_email = '{{ Auth::user()->email }}';
    // Pusher 
    // Pusher.logToConsole = true;
    var pusher = new Pusher('{{env("PUSHER_APP_KEY")}}', {
      encrypted: false
    });

    // Transactions Channel
    var transaction = pusher.subscribe('transaction-updates');
    transaction.bind('App\\Events\\TransactionNotifications', function(data) {
      var value = data;
      var tradeType = value.stock_trade;

      if(user_id == value.user_id){
        // to sentence
        if(tradeType == 'buy'){
          var word = 'Purchased';
        }
        if(tradeType == 'sell'){
          var word = 'Sold';
        }

        $(".load-transactions").html(
          word+'<br /> <b>'+value.stock_qty+'</b> of '+value.stock_name+' for <b>&#8358; '+value.stock_amount+'</b> at &#8358;'+value.stock_unit+
          '<span class="pull-right small">'+value.stock_date+'</span> <br /><br />'
        );
      } 
    });

    // Stock Channel
    var stocks = pusher.subscribe('stock-updates');
    stocks.bind('App\\Events\\StockBalanceNotifications', function(data) {
      // console.log();
      if(data.stocks[0].user_id == user_id){
        $(".load-stock-balance").html("");
        $.each(data.stocks, function (index, value){
          // console.log(value);
          
            $(".load-stock-balance").append(`
                <tr>
                  <td style="padding:0.5em;">`+value.name+`</td>
                  <td style="padding:0.5em;">&#8358; `+value.price+`</td>
                  <td style="padding:0.5em;"><i class="fa fa-database"></i> `+value.qty+`</td>
                </tr>
            `);
            $(".total_qty").html(value.total_qty); 
          
        });
      }
    });

    // Account Balance Channel
    var account = pusher.subscribe('update-account');
    account.bind('App\\Events\\AccountUpdate', function(data) {
      // console.log(data);
      if(data.user_id == user_id){
        $("#ac").text(data.bal);
        $(".account_balance").text(data.bal);
      }
    });

    // update trade screen
    var trade_screen = pusher.subscribe('feed-equities-prices');
    trade_screen.bind('App\\Events\\EquityUpdateChange', function(data) {
      // console.log(data);
      refreshTradeData();
    });

    $("#pairs_table tbody").html(
      '<h3 class="ml2">Equities is loading......</h3>'
    );

    // check for updates
    var checkUpdates = function (){
      // show stocks balance 
      $.get('/stocks/notifications', function (data){
        $(".load-stock-balance").html("");
        $.each(data, function (index, value){
          $(".load-stock-balance").append(`
            <tr>
              <td style="padding:0.5em;">`+value.name+`</td>
              <td style="padding:0.5em;">&#8358; `+value.price+`</td>
              <td style="padding:0.5em;"><i class="fa fa-database"></i> `+value.qty+`</td>
            </tr>
          `);
          $(".total_qty").html(value.total_qty);
        });  
      });

      // account balance state
      $.get("/accountbalance", function (data){
        $("#ac").text(data.account_balance);
        $(".account_balance").text(data.account_balance);
      });
      
      // show transactions notifications
      $.get('/transactions/notifications', function (data){
        $.each(data, function (index, value){
          var tradeType = value.stock_trade;
          if(tradeType == 'buy'){
            var word = 'Purchased';
          }
          if(tradeType == 'sell'){
            var word = 'Sold';
          }

          $(".load-transactions").append(
            word+'<br /> <b>'+value.stock_qty+'</b> of '+value.stock_name+' for <b>&#8358; '+value.stock_amount+'</b> at &#8358;'+value.stock_unit+
            '<span class="pull-right small">'+value.stock_date+'</span> <br /><br />'
          );
        });
      });
    };

      // first load data
    $(document).ready(function(){
      // show stocks balance 
      $.get('/stocks/notifications', function (data){
        $(".load-stock-balance").html("");
        $.each(data, function (index, value){
          $(".load-stock-balance").append(`
              <tr>
                <td style="padding:0.5em;">`+value.name+`</td>
                <td style="padding:0.5em;">&#8358; `+value.price+`</td>
                <td style="padding:0.5em;"><i class="fa fa-database"></i> `+value.qty+`</td>
              </tr>
          `);
          $(".total_qty").html(value.total_qty);
        });  
      });

      // account balance state
      $.get("/accountbalance", function (data){
        $("#ac").text(data.account_balance);
        $(".account_balance").text(data.account_balance);
      });

      // show news notifications
      $.get('/news/notifications', function (data){
        $.each(data, function (index, value){
          $(".load-news").append(`
            <h3>`+value.news_title+`</h3>
            <p>`+value.news_body+`</p>
            <span class="small">`+value.news_date+`</span>
          `);
        });
      });

      // show transactions notifications
      $.get('/transactions/notifications', function (data){
        $.each(data, function (index, value){
          var tradeType = value.stock_trade;
          if(tradeType == 'buy'){
            var word = 'Purchased';
          }
          if(tradeType == 'sell'){
            var word = 'Sold';
          }

          $(".load-transactions").append(
            word+'<br /> <b>'+value.stock_qty+'</b> of '+value.stock_name+' for <b>&#8358; '+value.stock_amount+'</b> at &#8358;'+value.stock_unit+
            '<span class="pull-right small">'+value.stock_date+'</span> <br /><br />'
          );
        });
      });
    });

    $.get('/catss/trade/random', function (data){
      $("#catss-loading").hide();
      $("#pairs_table tbody").html("");
        
      $.each(data, function (index, value){
        var pid    = value.id;
        var pn     = value.security; // equity name
        var sp     = value.close_price; // start price
        var cp     = value.open_price; // close price
        var pp     = value.previous_close; // close price
        var t_stat = value.status;
        var gap    = value.change_price;
        var vol    = value.daily_volume;
        var date   = value.date;

        var stats;
        if(value.previous_close > value.close_price){
          stats = `<td><i class="fa fa-arrow-down text-danger"></i> &#8358;`+value.close_price+`</td>`;
        }

        if(value.previous_close < value.close_price){
          stats = `<td bgcolor="#006400"><i class="fa fa-arrow-up text-success"></i> &#8358;`+value.close_price+`</td>`;
        }

        if(value.previous_close == value.close_price){
          stats = `<td><i class="fa fa-stop ticker-middle"></i> &#8358;`+value.close_price+`</td>`;
        }

        $("#pairs_table tbody").append(`
          <tr>
            <td>
              <a href="/equities/`+pn+`/`+pid+`" class="dino-link" data-toggle="tooltip" title="`+pn+`">
                <span>`+pn+`</span>
              </a>
            </td>
            `+stats+`
            <td>&#8358;`+pp+`</td>
            <td>`+gap+`</td>
            <td>`+t_stat+`</td>
            <td>
              <input type="text" id="trade_qty_`+pid+`" onkeyup="goDirty('`+pid+`', '`+pn+`', '`+value.new_band+`')" class="dino-input-live" placeholder="1000" required="">
            </td>
            <td><button class="dino-buy-live">Buy</button></td>
            <td><button class="dino-sell-live">Sell</button></td>
            <td>`+date+`</td>
            <input type="hidden" id="pair_id" value="`+pid+`" />
            <input type="hidden" id="pair_name" value="`+pn+`" />
            <input type="hidden" id="trade_price" value="`+sp+`" />
            <input type="hidden" id="sell_price" value="`+cp+`" />
            <input type="hidden" id="token" value="{{ csrf_token() }}">
          </tr>
        `);    
      });

      // buy request
      $('.dino-buy-live').click(function(e){
        e.preventDefault();
        var that  = $(this).parent().parent().find("#trade_qty");
        var token = $("#token").val();
        var pid   = $(this).parent().parent().find("#pair_id").val(); // pair id
        var pn    = $(this).parent().parent().find("#pair_name").val(); // pair name
        var tp    = $(this).parent().parent().find("#trade_price").val(); // trade_price start price
        var sp    = $(this).parent().parent().find("#sell_price").val(); // sell price close price
        var type  = 'buy' // type of trade
        var qty   = $(this).parent().parent().find("#trade_qty_"+pid).val(); // qty to trade

        // post trade
        $.ajax({
          type: "POST",
          url: "/request-trade",
          data: {
            _token:token,
            pid:pid,
            pn:pn,
            tp:tp,
            sp:sp,
            type:type,
            qty:qty
          },
          cache: false,
          success: function(data){
            if(data.status == 'success'){
              iziToast.show({
                title: "Successful",
                message: data.message,
                position: 'bottomLeft'
              });
              $("#trade_status").html(`
                <div class="popup-status">
                  `+data.message+`
                </div>
              `).show();
              that.val("");

              // console.log('trade request sent !');
              $("#trade_status").html(`
                <div class="popup-status">
                  `+data.message+`
                </div>
              `).fadeOut(8000);
            }else{
              $("#trade_status").html(`
                <div class="popup-status">
                  <p class="text-danger">`+data.message+`</p>
                </div>
              `).show();
              that.val("");
              // console.log('trade request sent !');
              $("#trade_status").html(`
                  <div class="popup-status">
                    <p class="text-danger">`+data.message+`</p>
                  </div>
              `).fadeOut(8000);

              iziToast.show({
                title: "Failed",
                message: data.message,
                position: 'bottomLeft'
              }); 

            }
          }
        });
      });

      // sell request
      $('.dino-sell-live').click(function(e){
        e.preventDefault();
        var that  = $(this).parent().parent().find("#trade_qty");
        var token = $("#token").val();
        var pid   = $(this).parent().parent().find("#pair_id").val(); // pair id
        var pn    = $(this).parent().parent().find("#pair_name").val(); // pair name
        var tp    = $(this).parent().parent().find("#trade_price").val(); // trade_price start price
        var sp    = $(this).parent().parent().find("#sell_price").val(); // sell price close price
        var type  = 'sell'; // type of trade
        var qty   = $(this).parent().parent().find("#trade_qty_"+pid).val(); // qty to trade

        // post trade
        $.ajax({
          type: "POST",
          url: "/request-trade",
          data: {
            _token:token,
            pid:pid,
            pn:pn,
            tp:tp,
            sp:sp,
            type:type,
            qty:qty
          },
          cache: false,
          success: function(data){
            if(data.status == 'success'){
              iziToast.show({
                title: "Success",
                message: data.message,
                position: 'bottomLeft'
              });

              $("#trade_status").html(`
                <div class="popup-status">
                  `+data.message+`
                </div>
              `).show();
              that.val("");
              // console.log('trade request sent !');
              $("#trade_status").html(`
                  <div class="popup-status">
                    `+data.message+`
                  </div>
              `).fadeOut(8000);

            }else{

              iziToast.show({
                title: "Error",
                message: data.message,
                position: 'bottomLeft'
              });

              $("#trade_status").html(`
                <div class="popup-status">
                  <p class="text-danger">`+data.message+`</p>
                </div>
              `).show();
              that.val("");
              // console.log('trade request sent !');
              $("#trade_status").html(`
                  <div class="popup-status">
                    <p class="text-danger">`+data.message+`</p>
                  </div>
              `).fadeOut(8000);
            }
          }
        });
      });
    });

    // get load trading functions
    function refreshTradeData(){
      $.get('/catss/trade/random', function (data){

        $("#catss-loading").hide();
        $("#pairs_table tbody").html("");

        $.each(data, function (index, value){
          var pid    = value.id;
          var pn     = value.security; // equity name
          var sp     = value.close_price; // start price
          var cp     = value.open_price; // close price
          var pp     = value.previous_close; // close price
          var t_stat = value.status;
          var gap    = value.change_price;
          var vol    = value.daily_volume;
          var date   = value.date;

          var stats;
          if(value.previous_close > value.new_band){
            stats = `<td><i class="fa fa-arrow-down text-danger"></i> &#8358;`+value.new_band+`</td>`;
          }

          if(value.previous_close < value.new_band){
            stats = `<td bgcolor="#006400"><i class="fa fa-arrow-up text-success"></i> &#8358;`+value.new_band+`</td>`;
          }

          if(value.previous_close == value.new_band){
            stats = `<td><i class="fa fa-stop ticker-middle"></i> &#8358;`+value.new_band+`</td>`;
          }
          $("#pairs_table tbody").append(`
              <tr>
                <td><a href="/equities/`+pn+`/`+pid+`" class="dino-link" data-toggle="tooltip" title="`+pn+`">`+pn+`</a></td>
                `+stats+`
                <td>&#8358;`+pp+`</td>
                <td>`+gap+`</td>
                <td>`+t_stat+`</td>
                <td><input type="number" id="trade_qty_`+pid+`" onkeyup="goDirty('`+pid+`', '`+pn+`', '`+value.new_band+`')" class="dino-input-live" placeholder="100000" required=""></td>
                <td><button class="dino-buy-live">Buy</button></td>
                <td><button class="dino-sell-live">Sell</button></td>
                <td>`+date+`</td>
                <input type="hidden" id="pair_id" value="`+pid+`" /><input type="hidden" id="pair_name" value="`+pn+`" />
                <input type="hidden" id="trade_price" value="`+value.new_band+`" /><input type="hidden" id="sell_price" value="`+cp+`" />
                <input type="hidden" id="token" value="{{ csrf_token() }}">
              </tr>
          `);
        });

        // buy request
        $('.dino-buy-live').click(function(e){
          e.preventDefault();
          var that  = $(this).parent().parent().find("#trade_qty");
          var token = $("#token").val();
          var pid   = $(this).parent().parent().find("#pair_id").val(); // pair id
          var pn    = $(this).parent().parent().find("#pair_name").val(); // pair name
          var tp    = $(this).parent().parent().find("#trade_price").val(); // trade_price start price
          var sp    = $(this).parent().parent().find("#sell_price").val(); // sell price close price
          var type  = 'buy' // type of trade
          var qty   = $(this).parent().parent().find("#trade_qty_"+pid).val(); // qty to trade

          // post trade
          $.ajax({
            type: "POST",
            url: "/request-trade",
            data: {
              _token:token,
              pid:pid,
              pn:pn,
              tp:tp,
              sp:sp,
              type:type,
              qty:qty
            },
            cache: false,
            success: function(data){
              if(data.status == 'success'){
                $("#trade_status").html(`
                  <div class="popup-status">
                    `+data.message+`
                  </div>
                `).show();
                that.val("");
                // console.log('trade request sent !');
                $("#trade_status").html(`
                    <div class="popup-status">
                      `+data.message+`
                    </div>
                `).fadeOut(8000);
              }else{
                $("#trade_status").html(`
                  <div class="popup-status">
                    <p class="text-danger">`+data.message+`</p>
                  </div>
                `).show();
                that.val("");
                // console.log('trade request sent !');
                $("#trade_status").html(`
                    <div class="popup-status">
                      <p class="text-danger">`+data.message+`</p>
                    </div>
                `).fadeOut(8000);
              }
            }
          });
        });

        // sell request
        $('.dino-sell-live').click(function(e){
          e.preventDefault();
          var that  = $(this).parent().parent().find("#trade_qty");
          var token = $("#token").val();
          var pid   = $(this).parent().parent().find("#pair_id").val(); // pair id
          var pn    = $(this).parent().parent().find("#pair_name").val(); // pair name
          var tp    = $(this).parent().parent().find("#trade_price").val(); // trade_price start price
          var sp    = $(this).parent().parent().find("#sell_price").val(); // sell price close price
          var type  = 'sell'; // type of trade
          var qty   = $(this).parent().parent().find("#trade_qty_"+pid).val(); // qty to trade

          // post trade
          $.ajax({
            type: "POST",
            url: "/request-trade",
            data: {
              _token:token,
              pid:pid,
              pn:pn,
              tp:tp,
              sp:sp,
              type:type,
              qty:qty
            },
            cache: false,
            success: function(data){
              if(data.status == 'success'){
                $("#trade_status").html(`
                  <div class="popup-status">
                    `+data.message+`
                  </div>
                `).show();
                that.val("");
                // console.log('trade request sent !');
                $("#trade_status").html(`
                    <div class="popup-status">
                      `+data.message+`
                    </div>
                `).fadeOut(8000);
              }else{
                $("#trade_status").html(`
                  <div class="popup-status">
                    <p class="text-danger">`+data.message+`</p>
                  </div>
                `).show();
                that.val("");
                // console.log('trade request sent !');
                $("#trade_status").html(`
                    <div class="popup-status">
                      <p class="text-danger">`+data.message+`</p>
                    </div>
                `).fadeOut(8000);
              }
            }
          });
        });
      });
    }

    var channel = pusher.subscribe('new-chat');
    channel.bind('App\\Events\\NewChatMsg', function(data) {
      // console.log(data);
      var value = data;
      var from = value.from;
      var to = value.to;

      if(to == logged_email){
        if(from == 'Agent'){
            $(".msg_msg").append(`
              <div class="inner_msg text-warning">
              <i class="fa fa-user"></i> `+from+ ` <br />
              `+value.body+` <br />
              <span style="font-size:9px;">`+value.date+` </span>
              <br /><br />
              <span class="line-br"></span>
              </div>
              <br />
            `);
        }else{
          $(".msg_msg").append(`
            <div class="inner_msg text-success">
            <i class="fa fa-user"></i> `+from+ ` <br />
            `+value.body+` <br />
            <span style="font-size:9px;">`+value.date+` </span>
            <br /><br />
            <span class="line-br"></span>
            </div>
            <br />
          `);
        }
      }
    });

    // This calculate the current Equities
    function goDirty(sec_id, sec_name, sec_price) {
      // get item quantity
      var qty = $('#trade_qty_'+sec_id).val();

      // get calculated details
      var initial_amount    = qty * sec_price;
      const nse_fee         = (0.3/100) * initial_amount;
      const stamp_fee       = (0.075/100) * initial_amount;
      const commission_fee  = (1.35/100) * initial_amount;

      // cscs fee only when selling equities
      const cscs_fee = (0.3/100) * initial_amount;

      // all fee charged + cscs alert
      var estimated_bid_total = nse_fee + stamp_fee + commission_fee + 4.00 + initial_amount;
      var estimated_ask_total = initial_amount - (nse_fee + cscs_fee + stamp_fee + commission_fee + 4.00);

      // body...
      $(".eq-calculator").show();
      $(".eq-calculator").html(`
        <span class="dino-link">`+sec_name+`</span> <p class="pull-right">Mkt (&#8358;`+sec_price+`)</p>
        <table class="table">
          <tr>
            <td>Consideration</td>
            <td><span class="dino-link">`+initial_amount.toLocaleString()+`</span></td>
          </tr>
          <tr>
            <td>Commission(1.35)</td>
            <td><span class="dino-link">`+commission_fee.toLocaleString()+`</span></td>
          </tr>
          <tr>
            <td>CSCS Alert(&#8358;4.00)</td>
            <td>4.00</td>
          </tr>
          <tr>
            <td>CSCS Fee(0.3%)</td>
            <td><span class="dino-link">`+cscs_fee.toLocaleString()+` (- sell)</span></td>
          </tr>
          <tr>
            <td>NSE/SEC Fee(0.3%)</td>
            <td><span class="dino-link">`+nse_fee.toLocaleString()+`</span></td>
          </tr>
          <tr>
            <td>Stamp Duties(0.075)</td>
            <td><span class="dino-link">`+stamp_fee.toLocaleString()+`</span></td>
          </tr>
          <tr>
            <td>
              Buy <br />
              <span class="text-info h5">&#8358;`+estimated_bid_total.toLocaleString()+`</span>
            </td>
            <td>
              <span class="dino-link">Sell </span><br />
              <span class="text-success h5">&#8358;`+estimated_ask_total.toLocaleString()+`</span>
            </td>
          </tr>
        </table>
      `);
    }
  </script>
@endsection
