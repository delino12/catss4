@extends('layouts.admin-skin')

@section('title')
  CATSS | Add New Stock Broker
@endsection

@section('contents')
  <style type="text/css">
    .typeahead.tt-input {
      width: 100%;
    }  
  </style>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h3>Create/Add a Stock Broker</h3>
          <div class="card">
            <div class="card-header btn-info">Add Stock Broker</div>
            <div class="card-body" style="padding: 1em;">
              <div class="row">
                <div class="col-md-5">
                  <form id="create-agent-form" method="post" onsubmit="return addStockBroker()">
                    <div class="form-group">
                      <label>Select a Brokerage firm</label>
                      <input type="text" class="form-control" id="broker_firm" placeholder="Eg. Afritrust, InvestmentOne..." required="">
                    </div>

                    <div class="form-group">
                      <label>Enter Agent's Name</label>
                      <input type="text" class="form-control" id="broker_name" placeholder="First name, Last name" name="">
                    </div>

                    <div class="form-group">
                      <label>Enter Agent's Email</label>
                      <input type="text" class="form-control" id="broker_email" placeholder="someone@domain.com" name="">
                    </div>

                    <div class="form-group">
                      <label>Choose a default password</label>
                      <input type="password" class="form-control" id="broker_password" placeholder="*****" name="">
                    </div>

                    <div class="form-group">
                      <button class="btn btn-info" id="add-new-agent">Add New Agent</button>
                    </div>
                  </form>
                </div>
                <div class="col-md-7">
                  <table class="table" id="brokerage-table">
                    <thead>
                      <tr>
                        <th>Brokerage Firm</th>
                        <th>Agent Name</th>
                        <th>Total Client</th>
                        <th>Option</th>
                      </tr>
                    </thead>
                    <tbody class="load-firms-agent"></tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('scripts')
  <script type="text/javascript">
      // get all stock broking firm
      loadStockFirm();
      getAllFirms();

      function loadStockFirm() {
        $.get('{{ url('admin/list/firm/names') }}', function(data) {
          $('#broker_firm').typeahead({
            source: data
          });
        });
      }

      function addStockBroker() {
        $("#add-new-agent").prop('disabled', true);
        $("#add-new-agent").html(`Creating account please wait!`);
        var token           = $("#token").val();
        var broker_firm     = $("#broker_firm").val();
        var broker_name     = $("#broker_name").val();
        var broker_email    = $("#broker_email").val();
        var broker_password = $("#broker_password").val();

        var params = {
          broker_firm: broker_firm,
          broker_name: broker_name,
          broker_email: broker_email,
          broker_password: broker_password,
          _token: token
        };

        $.post('{{ url('admin/save/broker') }}', params, function(data, textStatus, xhr) {
          /*optional stuff to do after success */
          if(data.status == "success"){
            swal(
              "success",
              data.message,
              data.status
            );
            $("#create-agent-form")[0].reset();
            $("#add-new-agent").html(`Add New Agent`);
            getAllFirms();
          }else{
            swal(
              "oops",
              data.message,
              data.status
            );
          }

          $("#add-new-agent").prop('disabled', false);
        });

        // return 
        return false;
      }

      function getAllFirms() {
        $.get('{{ url('admin/all/firms') }}', function(data) {
          /*optional stuff to do after success */
          $.each(data, function(index, val) {
            $(".load-firms-agent").append(`
              <tr>
                <td>${val.firm_name}</td>
                <td>${val.name}</td>
                <td>${val.total_client}</td>
                <td>
                  <a href="javascript:void(0);">view</a>
                </td>
              </tr>
            `);
          });

          // $("#brokerage-table").datatable();
        });
      }


    </script>
@endsection