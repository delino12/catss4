@extends('layouts.admin-skin')

@section('title')
    Notifications 
@endsection

@section('contents')
    <div class="container notifications">
      <div class="row">
        <div class="col-md-5">
          <h2>Notifications</h2>
          @if(session('msg_delete_status'))
            <p class="text-warning">{{ session('msg_delete_status') }}</p>
          @endif
          <hr />  
          <div class="msg_msg"></div>
        </div>
      </div>
    </div>

    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
    <script type="text/javascript">
      $.get('/admin/loadchat', function (data){
        $(".msg_msg").html("");
        $.each(data, function (index, value){
          // console.log(value);
          var from = value.from;
          var to = value.to;
          $(".msg_msg").append(`
            <div class="panel panel-info">
              <div class="panel-heading">
              1 <i class="fa fa-envelope-o"></i>  
              from  `+from+ ` <br /> <br />
               `+value.body+` <br /><br />
              <span class"small pull-right">`+value.date+` </span> 
              <br /><br />
              <span class="dino-link">
                <a href="/admin/delete-message/`+value.id+`"> <i class="fa fa-trash"></i> delete</a> ---
                <a href="#" class="click-reply"> <i class="fa fa-reply"></i> reply</a>
              </span>
              <br /><br />
              <p class="reply-stat"></p>
              <div class="reply_user" style="display: none;">
                `+value.id+`
                <hr />
                <form class="reply-form" method="post" onsubmit="return sendReply()">
                {{ csrf_field() }}
                  <div class="form-group">
                    <input type="hidden" id="id" value="`+value.id+`">
                    <textarea class="form-control" rows="2" cols="20" placeholder="type a response" id="msg"></textarea>
                  </div>
                  <div class="form-group">
                    <button class="btn btn-primary">Send Reply</button>
                  </div>
                </form>
              </div>
              </div>
            </div>
          `);
        });
        $('.click-reply').click(function(e){
          // console.log('me');
          $(this).parent().parent().find('.reply_user').show();

        })

      });

      function sendReply(){
        var token = $("input[name=_token]").val();
        var id = $("#id").val();
        var msg = $("#msg").val();
      
        $.ajax({
          type: "post",
          url: "/admin/reply/send",
          data: {
            _token:token,
            id:id,
            msg:msg
          },
          cache: false,
          success: function (data){
            $(".reply-stat").html(data);
            $(".reply_user").hide();
            $(".reply-form")[0].reset();
          },
          error: function (){
            alert('fail to sent reply to client ');
          }
        });

        return false;
      }

      // Pusher 
      // Pusher.logToConsole = true;
      var pusher = new Pusher('{{env("PUSHER_APP_KEY")}}', {
        encrypted: false
      });

      var channel = pusher.subscribe('new-chat');
      channel.bind('App\\Events\\NewChatMsg', function(data) {
        var value = data;
        var from = value.from;
        var to = value.to;
        var container = $(".msg_msg");
   
        var markup = `
           <div class="panel panel-info">
            <div class="panel-heading">
            1 <i class="fa fa-envelope-o"></i>  
            from  `+from+ ` <br /> <br />
             `+value.body+` <br /><br />
            <span class"small pull-right">`+value.date+` </span> 
            <br /><br />
            <span class="dino-link">
              <a href="/admin/delete-message/`+value.id+`"> <i class="fa fa-trash"></i> delete</a> ---
              <a href="#" class="click-reply"> <i class="fa fa-reply"></i> reply</a>
            </span>
            <br /><br />
            <p class="reply-stat"></p>
            <div class="reply_user" style="display: none;">
              <hr />
              <form method="post" action="" onsubmit="return sendReply()">
              {{ csrf_field() }}
                <div class="form-group">
                  <input type="hidden" id="id" value="`+value.id+`">
                  <textarea class="form-control" rows="2" cols="20" placeholder="type a response" id="msg"></textarea>
                </div>
                <div class="form-group">
                  <button class="btn btn-primary">Send Reply</button>
                </div>
              </form>
            </div>
            </div>
          </div>
        `;

        container.prepend(markup);

        console.log(data);
      });
    </script>
@endsection
