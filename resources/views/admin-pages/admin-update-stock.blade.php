@extends('layouts.admin-skin')

@section('title')
  Upload Daily Equitie
@endsection

@section('contents')
  <div class="container">
    <div  class="row">
      <div class="col-md-4">
        <h3>Upload Today's Equities</h3>
        <div class="paneln panel-info">
          <div class="panel-heading">
            Locate Equity Excel Docx <div class="pull-right" id="upload-status"></div>
          </div>
          <div class="panel-body card card-body">
            <div class="">
              <div class="dropzone">
                {{ csrf_field() }}
                <hr />
              </div>
            </div>
            <div class="upload-status"></div>
          </div>
        </div>
      </div>

      <div class="col-md-8">
        <h3>Equities List</h3>
        <div class="paneln panel-info">
          <div class="panel-heading">
            Daily price list
          </div>
          <div class="panel-body">
            <div class="nse-equities-list">
              <div class="thumbnail"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  {{-- create a smaple height --}}
  <div style="height: 200px;"></div>
  <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $.get('/admin/update/price', function(data) {
        /*optional stuff to do after success */
        console.log('new price updates sent !');
        console.log(data);
      });

      // body...
      $.get('/admin/scan/orders/buy', function(data) {
        /*optional stuff to do after success */
        console.log('drone released looking for buy orders, scanning...');
        console.log(data);
      });
      
      $.get('/admin/scan/orders/sell', function(data) {
        /*optional stuff to do after success */
        console.log('drone released looking for sell orders, scanning...');
        console.log(data);
      });
    });

    function refreshTradeData(){
      $.get('/admin/update/price', function(data) {
        // console.log('new price updates sent !');
        console.log(data);
      });
    }

    // scan orders for execution
    function scanOrders() {
      // body...
      $.get('/admin/scan/orders/buy', function(data) {
        /*optional stuff to do after success */
        console.log('drone released looking for buy orders, scanning...');
        console.log(data);
      });
      
      $.get('/admin/scan/orders/sell', function(data) {
        /*optional stuff to do after success */
        console.log('drone released looking for sell orders, scanning...');
        console.log(data);
      });
    }

    // call trade every 15 secs
    window.setInterval(function (){
      // alert('hello');
      refreshTradeData();
      scanOrders();
    }, 1000 * 18);

    // init Laravel token
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN' : '{{ csrf_token() }}'
      }
    });

    // set dropzone
    $(".dropzone").dropzone({ 
      url: "/admin/upload/equities",
      params: {
        _token: "{{ csrf_token() }}"
      },
      success: function (data){
        // console.log(data);
        // alert('File upload successful !');
        $(".upload-status").html(`
          <div class="alert alert-success">
            <p style="color:#fff;">
              File Upload successful !!
            </p>
          </div>
        `);
      } 
    });

    // load nse List
    $.get('/load/nse/list', function (data){
      // console.log(data);
      $(".nse-equities-list").html("");
      var sn = 0;
      $.each(data, function (index, value){
        sn++;

        $(".nse-equities-list").append(`
          <div style="padding:0.4em;">
            <span>`+value.date+` </span>
            <span class="pull-right">
              <a href="/equities/`+value.name+`"> <i class="fa fa-download"></i> Download  Equities  </a>
            </span>
          </div>
        `);

        if(sn > 10){
          return false;
        }

         $(".nse-equities-list").append(`
          <div style="padding:0.4em;">
            <span>`+value.date+` </span>
            <span class="pull-right">
              <a href="/equities/`+value.name+`"> <i class="fa fa-download"></i> Download  Equities  </a>
            </span>
          </div>
        `);
      });
    });

    // Pusher 
    // Pusher.logToConsole = true;

    var pusher = new Pusher('{{env("PUSHER_APP_KEY")}}', {
      encrypted: false
    });

    var channel = pusher.subscribe('new-stock-upload');
    channel.bind('App\\Events\\NewStockUpload', function(data) {
      var id = data.id;
      var name = data.name;
      var date = data.date;

      var container = $(".nse-equities-list");
 
      var markup = `
        <div>
          <span>`+data.date+` </span>
          <span class="pull-right">
            <a href="/equities/`+data.name+`"> <i class="fa fa-download"></i> Download  Equities  </a>
          </span>
        </div>
      `;
      container.prepend(markup);
      // console.log(data);
    });
  </script>
@endsection