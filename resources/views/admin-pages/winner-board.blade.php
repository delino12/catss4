@extends('layouts.admin-skin')

@section('title')
   Rewarrd Users
@endsection

@section('contents')
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        <h3>Reward Winner</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 small">
        <div class="card">
          <div class="card-header" data-background-color="orange">
            <h4 class="title"><i class="fa fa-trophy"></i> Weekly Winner Stats</h4>
            <p class="category">New Winner on {{ date("M Y ") }}</p>
          </div>
          <div class="card-content table-responsive">
            <table class="table table-hover" id="ranking-table">
              <thead class="text-warning">
                <th>S/N</th>
                <th><i class="fa fa-users"></i> Name</th>
                <th>&#8358; Profits</th>
                <th>Date</th>
                <th>Action</th>
              </thead>
              <tbody class="list-ranking"></tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="first">
          Postion: {{ $p }} <br />
          Reward: {{ $user_info->name }}
          <form method="post" onsubmit="return rewardUsers()">
            {{-- user_id --}}
            <input type="hidden" id="user_id" value="{{ $id }}">
            <input type="hidden" id="postion" value="{{ $p }}">
            <input type="hidden" id="amount" value="{{ $n }}">
            <div class="form-group">
              <button class="btn btn-primary">Reward {{ $user_info->name }}</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <hr />

  {{-- scripts --}}
  <script type="text/javascript">
    // load ranking 
    $.get('/load/ranking', function(data) {
      // sort data 
      var data = data.ranking.sort(function (a, b){
          return b.profit - a.profit; 
      });

      var sn = 0;
      $.each(data, function(index, el) {
          // console.log(el);
          sn++;
          var name   = el.user;
          var profit = el.profit.toLocaleString();
          var date   = el.date;

          // reward first top 3
          if(sn > 3){
              var btn = '----';
            }else{
               var btn = '<a href="/admin/reward/winner/'+el.id+'/'+sn+'/'+profit+'">Reward</a>';
            }

          // show only users with actual figures
          if(el.profit > 0){
            $(".list-ranking").append(`
                <tr>
                    <td>`+sn+`</td>
                    <td>`+name+`</td>
                    <td>&#8358;`+profit+`</td>
                    <td>`+date+`</td>
                    <td>`+btn+`</td>
                </tr>
            `);
          }else{

            $(".list-ranking").append(`
                <tr>
                    <td>`+sn+`</td>
                    <td>`+name+`</td>
                    <td><span class="text-danger"> &#8358; `+profit+` </span></td>
                    <td>`+date+`</td>
                    <td>`+btn+`</td>
                </tr>
            `);
          }
      });

      $("#ranking-table").dataTable();
    });

    // reward players
    function rewardUsers(){
      // get form data
      var token    = '{{ csrf_token() }}';
      var userId   = $('#user_id').val();
      var position = $('#postion').val();
      var amount   = $('#amount').val();
    
      // data to json
      var data = {
        _token:token,
        userId:userId,
        position:position, 
        amount:amount
      }
    
      // data to ajax
      $.ajax({
        type: 'POST',
        url: '/reward/winner',
        dataType: 'json',
        data: data,
        success: function (data) {
          console.log(data);
          alert('Successful, check console data');
        },
        error: function (data) {
          console.log(data);
          alert('Error, fail to send request !');
        }
      });
    
      return false;
    }
  </script>
@endsection