@extends('layouts.admin-skin')

@section('title')
  CATSS | Trade Ranking 
@endsection

@section('contents')
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <h3>Trade Ranking List</h3>

        <div class="card">
              <div class="card-header" data-background-color="orange">
                  <h4 class="title">Weekly Winner Stats</h4>
                  <p class="category">New Winner on {{ date("M Y ") }}</p>
              </div>
              <div class="card-content table-responsive">
                  <table class="table table-hover" id="ranking-table">
                      <thead class="text-warning">
                          <th>S/N</th>
                  <th><i class="fa fa-users"></i> Name</th>
                  <th>&#8358; Profits</th>
                  <th>Date</th>
                      </thead>
                      <tbody class="list-ranking"></tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    // load ranking 
    $.get('/load/ranking', function(data) {
        /*optional stuff to do after success */
        // console.log(data);
        // sort data 
        var data = data.ranking.sort(function (a, b){
            return b.profit - a.profit; 
        });

        var sn = 0;
        $.each(data, function(index, el) {
            // console.log(el);
            sn++;
            var name   = el.user;
            var profit = el.profit.toLocaleString();
            var date   = el.date;

            // show only users with actual figures
            if(el.profit > 0){
              $(".list-ranking").append(`
                  <tr>
                      <td>`+sn+`</td>
                      <td>`+name+`</td>
                      <td>&#8358;`+profit+`</td>
                      <td>`+date+`</td>
                  </tr>
              `);
            }else{
              $(".list-ranking").append(`
                  <tr>
                      <td>`+sn+`</td>
                      <td>`+name+`</td>
                      <td><span class="text-danger"> &#8358; `+profit+` </span></td>
                      <td>`+date+`</td>
                  </tr>
              `);
            }
        });

        $("#ranking-table").dataTable();
    });
    </script>
@endsection