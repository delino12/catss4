@extends('layouts.admin-login-skin')

@section('title')
    Login to CATSS
@endsection

@section('contents')
    <br /><br /><br /><br />
    <div class="container">
        <div class="row">
            <div class="col-md-6" style="box-shadow: 1px 1px 5px 1px;">
                <h2>Admin control panel</h2>
                <br /><hr />

                <div class="form">
                    <form method="POST" action="/admin-login">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-2">
                                     
                                </div>
                                <div class="col-sm-8">
                                    <i class="fa fa-user"></i> <label>Username</label>
                                    <input type="email" name="email" placeholder="admin username" class="form-control" style="padding: 0.9em;" required="">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-2">
                                     
                                </div>
                                <div class="col-sm-8">
                                    <i class="fa fa-lock"></i> <label>Password</label>
                                    <input type="password" name="password" placeholder="**********" class="form-control" style="padding: 0.9em;" required="">
                                    <br />
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-2">
                                   
                                </div>
                                <div class="col-sm-5">
                                    <button class="btn btn-info"><i class="fa fa-lock"></i> Login Account</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                @if(session('login-status'))
                    <hr />
                    <p class="text-danger">{{ session('login-status') }}</p>
                @endif

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li class="text-danger">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-md-6 bg">
                <div style="height: 395px;"></div>
            </div>
        </div>
    </div>
@endsection
