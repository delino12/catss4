@extends('layouts.admin-skin')

@section('title')
  CATSS | Commission Fee 
@endsection

@section('contents')
  <div class="row">
    <div class="col-md-10">
          <div class="card">
              <div class="card-header" data-background-color="purple">
                  <h4 class="title">Commission Fee history</h4>
                  <p class="category">Last updated {{ date("d M Y ") }}</p>
              </div>
              <div class="card-content table-responsive">
                  <table class="table table-hover" id="commission-table">
                      <thead class="text-warning">
                          <th>S/N</th>
                          <th>Name</th>
                          <th>Balance</th>
                          <th>Qty</th>
                          <th>Percent</th>
                          <th>Amount (&#8358;)</th>
                          <th>Tran. Type</th>
                          <th>Date</th>
                      </thead>
                      <tbody class="commission-charge"></tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>

 
  <script type="text/javascript">
    // get total commission charges
    $.get('/admin/load-commission', function(data) {
      /*optional stuff to do after success */
      // console.log(data);
      $('.commission-charge').html();
      var sn = 0;
      $.each(data.details, function(index, val) {
        /* iterate through array or object */
        // console.log(val);
        sn++;
        $('.commission-charge').append(`
          <tr>
            <td>`+sn+`</td>
            <td>`+val.name+`</td>
            <td>`+val.acc_bal+`</td>
            <td>`+val.qty+`</td>
            <td>`+val.percent+`</td>
            <td>&#8358;`+val.amount+`</td>
            <td>`+val.type+`</td>
            <td>`+val.date+`</td>
          </tr>
        `);
      });

      $('#commission-table').dataTable();

      $('.total-sum').html(`
        &#8358; `+data.total+`
      `);

      $('.no_of_trade').html(`
        +`+data.count+`
      `);
    });

    // refresh data 
    function refreshData(){
      $.get('/admin/load-commission', function(data) {
          /*optional stuff to do after success */
          console.log(data);
          $('.commission-charge').html();
          var sn = 0;
          $.each(data.details, function(index, val) {
            /* iterate through array or object */
            // console.log(val);
            sn++;
            $('.commission-charge').append(`
              <tr>
                <td>`+sn+`</td>
                <td>`+val.name+`</td>
                <td>&#8358;`+val.acc_bal+`</td>
                <td>`+val.qty+`</td>
                <td>`+val.percent+`</td>
                <td>&#8358;`+val.amount+`</td>
                <td>`+val.type+`</td>
                <td>`+val.date+`</td>
              </tr>
            `);
          });
          // $('.commission-charge').append(`
          //   <tr>
          //     <td></td>
          //     <td></td>
          //     <td>Total:</td>
          //     <td class="text-success">&#8358; `+data.total+`</td>
          //     <td></td>
          //   </tr>
          // `);

          $('.total-sum').html(`
            &#8358; `+data.total+`
          `);

          $('.no_of_trade').html(`
            +`+data.count+`
          `);
      });
    }
</script>
@endsection
