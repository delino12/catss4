@extends('layouts.admin-skin')

@section('title')
    Customer Care 
@endsection

@section('contents')
  <div class="container notifications">
    <div class="row">
      <div class="col-md-8">
        <h2>All Activity logs..</h2>
        <table class="table">
          <thead>
            <tr>
              <td>S/N</td>
              <td>Caused By</td>
              <td>Description</td>
              <td>Last updated</td>
            </tr>
          </thead>
          <tbody class="load-activity-logs"></tbody>
        </table>
      </div>

      <div class="col-md-4">
        <h2>Extra</h2>
        <hr />
      </div>
    </div>
  </div>


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
  <script type="text/javascript">
    // load activity logs 
    $.get('/load/activity/log', function(data) {
      // console.log(data);
      $(".load-activity-logs").html();
      var sn = 0;
      $.each(data, function(index, val) {
        // console.log(val)
        sn++;
        $(".load-activity-logs").append(`
          <tr>
            <td>`+sn+`</td>
            <td>`+val.by+`</td>
            <td>`+val.activity+`</td>
            <td>`+val.created+`</td>
          </tr>
        `);
      });
    });
  </script>
@endsection
