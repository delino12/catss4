@extends('layouts.forum-skin')

@section('title')
	CATSS Forum
@endsection

@section('contents')
<!--Simple nav bar-->
   
	<br />
	<div class="container-fluid">
	<div class="row">
		<div class="col-md-8" style="box-shadow:1px 1px 2px 1px #000;background-color:rgba(000,000,000,0.80);">
			<h3><i class="fa fa-pencil"></i> Start a topic (Tell us what we can do, Client Feedback and Issues)</h3>
					<form method="post" onsubmit="return sendPost()" id="forum-form">
						{{ csrf_field() }}

						@if(Auth::user())
							<div class="form-group">
								<input type="hidden" name="name" value="{{ Auth::user()->name }}" class="form-control">
							</div>
							<div class="form-group">
								<input type="hidden" name="email" value="{{ Auth::user()->email }}" class="form-control">
							</div>
						@else
							<div class="form-group">
								<label>Name</label>
								<input type="text" name="name" placeholder="Your name" class="form-control" required="">
							</div>
							<div class="form-group">
								<label>Email</label>
								<input type="email" name="email" placeholder="Your email" class="form-control" required="">
							</div>
						@endif

						<div class="form-group">
							<textarea cols="20" style="background-color:rgba(000,000,000,0.50)" rows="5" id="post" placeholder="Start a discussion !" class="form-control" required=""></textarea>
						</div>
						<div class="form-group">
							<button class="btn btn-primary">Post Topic</button>
						</div>
						<div class="post-status"></div>
					</form>
			<hr />
			<!-- Load All Topics -->
			<div class="list-topics"></div>
		</div>
		<div class="col-md-4" style="box-shadow:1px 1px 2px 1px #000;background-color:rgba(000,000,000,0.90);">
			<h4 class="small">Recent Topics</h4>
			<div class="top-recent"></div>
		</div>
	</div>
</div>

	<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
	<script type="text/javascript">
		// Pusher 
	    // Pusher.logToConsole = true;
	    var pusher = new Pusher('{{env("PUSHER_APP_KEY")}}', {
	      encrypted: false
	    });

	    var channel = pusher.subscribe('new-forum-post');
	    channel.bind('App\\Events\\NewForumPost', function(data) {
	    	console.log(data);
	    	var value = data;
	    	$(".list-topics").prepend(`
				<div style="border-radius:4px;padding:1em;border: 1px solid #FFF;">
			      	<div class="media">
				      	<p class="pull-right">By: `+value.name+`</p>
				  		<div class="media-body">
				  			<h4 class="media-heading text-success">`+value.title+` </h4>
					        <br />
				       	</div>
			    	</div>
			    	<p style="padding:0.8em;">
			    		<span class="small btn btn-link pull-left"><i class="fa fa-clock-o"></i> `+value.date+` </span>
					    <a href="/forum/reply/post/`+value.id+`"><span class="small btn btn-link pull-left"><i class="fa fa-comment"></i> 2 comments</span></a>
					    <span class="small btn btn-link pull-right" style="padding:0.8em;">
					    	<span style="padding:0.8em;">120 <i class="fa fa-thumbs-o-down"></i> </span>
					    	<span style="padding:0.8em;">8 <i class="fa fa-thumbs-o-up"></i></span>
					    </span>
			    	</p>
			    	<br />
			  	</div>
			  	<br /><br />
			`);
			$(".top-recent").prepend(`
				<div style="box-shadow:1px 1px 2px 1px #CCC;padding:1em;">
				 <a href="/forum/reply/post/`+value.title+`/`+value.id+`"><i class="fa fa-tags"></i>	`+value.title+`</a>
				</div>
				<br />
			`);
	    });

	    // send post via ajax
		function sendPost()
		{
			var token = $("input[name=_token]").val();
			var name  = $("input[name=name]").val();
			var email = $("input[name=email]").val();
			var post  = $("#post").val();

			var data = {
				_token:token,
				name:name,
				email:email,
				post:post
			};

			$.ajax({
				type: "post",
				url: "/send/forum-post",
				data: data,
				cache: false,
				success: function (data){
					// console.log(data);
					$("#forum-form")[0].reset();
					if(data.status == 'error'){
						$('.post-status').html(`
							<p class="text-danger">`+data.message+`</p>
						`);
					}else{
						$('.post-status').html(`
							<p class="text-success">`+data.message+`</p>
						`);
					}

					setTimeout(refreshForum(), 500);
				},
				error: function (data){
					// console.log(data);
					alert('fail to post ');
				}
			});

			return false;
		}

		// refresh post void reload
		function refreshForum(){
			$.get('/load/forum-post', function (data){
				// console.log(data);
				$(".list-topics").html("");
				$(".top-recent").html("");
				$.each(data, function (index, value){
					$(".list-topics").append(`
						<div style="border-radius:4px;padding:1em;border: 1px solid #FFF;">
					      	<div class="media">
						      	<p class="pull-right">By: `+value.name+`</p>
						  		<div class="media-body">
						  			<h4 class="media-heading">`+value.title+` </h4>
							        <br />
						       	</div>
					    	</div>
					    	<p style="padding:0.8em;">
					    		<span class="small btn btn-link pull-left"><i class="fa fa-clock-o"></i> `+value.date+` </span>
							    <a href="/forum/reply/post/`+value.id+`"><span class="small btn btn-link pull-left"><i class="fa fa-comment"></i> `+value.no_comment+` comments</span></a>
							    <span class="small btn btn-link pull-right" style="padding:0.8em;">
							    	<span style="padding:0.8em;">120 <i class="fa fa-thumbs-o-down"></i> </span>
							    	<span style="padding:0.8em;">8 <i class="fa fa-thumbs-o-up"></i></span>
							    </span>
					    	</p>
					    	<br />
					  	</div>
					  	<br /><br />
					`);
					$(".top-recent").append(`
						<div style="box-shadow:1px 1px 2px 1px #CCC;padding:1em;">
						 <a href="/forum/reply/post/`+value.id+`"><i class="fa fa-tags"></i>	`+value.title+`</a>
						</div>
						<br />
					`);
				});
			});
		}

		$.get('/load/forum-post', function (data){
			// console.log(data);
			$(".list-topics").html("");
			$(".top-recent").html("");
			$.each(data, function (index, value){
				$(".list-topics").append(`
					<div style="border-radius:4px;padding:1em;border: 1px solid #FFF;">
				      	<div class="media">
					      	<p class="pull-right">By: `+value.name+`</p>
					  		<div class="media-body">
					  			<h4 class="media-heading">`+value.title+` </h4>
						        <br />
					       	</div>
				    	</div>
				    	<p style="padding:0.8em;">
				    		<span class="small btn btn-link pull-left"><i class="fa fa-clock-o"></i> `+value.date+` </span>
						    <a href="/forum/reply/post/`+value.id+`"><span class="small btn btn-link pull-left"><i class="fa fa-comment"></i> `+value.no_comment+` comments</span></a>
						    <span class="small btn btn-link pull-right" style="padding:0.8em;">
						    	<span style="padding:0.8em;">120 <i class="fa fa-thumbs-o-down"></i> </span>
						    	<span style="padding:0.8em;">8 <i class="fa fa-thumbs-o-up"></i></span>
						    </span>
				    	</p>
				    	<br />
				  	</div>
				  	<br /><br />
				`);
				$(".top-recent").append(`
					<div style="box-shadow:1px 1px 2px 1px #CCC;padding:1em;">
					 <a href="/forum/reply/post/`+value.id+`"><i class="fa fa-tags"></i>	`+value.title+`</a>
					</div>
					<br />
				`);
			});
		});
	</script>
@endsection
