@extends('layouts.forum-skin')

@section('title')
	{{ $title }} | CATSS Forum
@endsection

@section('contents')
	<!--Simple nav bar-->
	<br />
    <div class="container-fluid">
		<div class="row">
			<div class="col-md-8" style="box-shadow:1px 1px 2px 1px #000;background-color:rgba(000,000,000,0.80);text-align: left;">
				<!-- Load All Topics -->
				<h2 style="margin-left: 20px;"><a href="javascript:void(0);">{{ $title }}</a></h2>
				<h3 style="margin-left: 20px;"><i class="fa fa-comments-o"></i> Comments</h3>
				<div class="col-md-6">
					<div class="load-comments" style="margin-left: 20px;"></div>
					<form method="post" onsubmit="return sendComments()" id="forum-form">
						{{ csrf_field() }}
						@if(Auth::user())
							<div class="form-group">
								<input type="hidden" name="name" value="{{ Auth::user()->name }}" class="form-control">
							</div>
							<div class="form-group">
								<input type="hidden" name="email" value="{{ Auth::user()->email }}" class="form-control">
							</div>
						@else
							<div class="form-group">
								<label>Name</label>
								<input type="text" name="name" placeholder="Your name" class="form-control" required="">
							</div>
							<div class="form-group">
								<label>Email</label>
								<input type="email" name="email" placeholder="Your email" class="form-control" required="">
							</div>
						@endif

						<div class="form-group">
							<textarea cols="20" style="background-color:rgba(000,000,000,0.50);" rows="2" id="comments" placeholder="Start a discussion !" class="form-control" required=""></textarea>
						</div>
						<div class="form-group">
							<button class="btn btn-primary">Post Comments</button>
						</div>

						<div class="post-status"></div>
					</form><input type="hidden" value="{{ $id }}" name="title_id">
				</div>
				
				<div class="list-title-topic"></div>
			</div>
			<div class="col-md-4" style="box-shadow:1px 1px 2px 1px #000;background-color:rgba(000,000,000,0.90);">
				<h4 class="small">Recent Topics</h4>
				<div class="top-recent"></div>
			</div>
		</div>
	</div>

	<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
	<script type="text/javascript">

		// Pusher.logToConsole = true;
	    var pusher = new Pusher('{{env("PUSHER_APP_KEY")}}', {
	      encrypted: false
	    });

	    var channel = pusher.subscribe('new-forum-comment');
	    channel.bind('App\\Events\\NewComment', function(data) {
	    	console.log(data);
	    	var value = data;
	    	$('.load-comments').append(`
				<div class="row">
					<div class="col-xm-2">
						<a><i class="fa fa-user"></i> `+value.name+`</a>
					</div>
				</div>
				<div class="row">
					<div class="col-xm-2">
						<p class="text-success">`+value.body+`</p>
						<span class="small"><i class="fa fa-clock-o"></i> `+value.date+`</span>
					</div>
				</div>
				<br /><br />
			`);
	    });


		var titleId = $("input[name=title_id]").val();
		$.get('/load/post/title/'+titleId, function (data){
			// console.log(data);
		});

		$.get('/load/comments/'+titleId, function (data){
			console.log(data);
			$('.load-comments').html();
			$.each(data, function (index, value){
				$('.load-comments').append(`
					<div class="row">
						<div class="col-xm-2">
							<a><i class="fa fa-user"></i> `+value.name+`</a>
						</div>
					</div>
					<div class="row">
						<div class="col-xm-2">
							<p>`+value.body+`</p>
							<span class="small"><i class="fa fa-clock-o"></i> `+value.date+`</span>
						</div>
					</div>
					<br /><br />
				`);
			});
		});

		function sendComments()
		{
			var token    = $("input[name=_token]").val();
			var name     = $("input[name=name]").val();
			var email    = $("input[name=email]").val();
			var comment  = $("#comments").val();
			var titleId  = $("input[name=title_id]").val();

			var data = {
				_token:token,
				id: titleId,
				name:name,
				email:email,
				comment:comment
			};

			$.ajax({
				type: "post",
				url: "/send/forum-comment",
				data: data,
				cache: false,
				success: function (data){
					// console.log(data);
					$("#forum-form")[0].reset();
					if(data.status == 'error'){
						$('.post-status').html(`
							<p class="text-danger">`+data.message+`</p>
						`);
					}else{
						$('.post-status').html(`
							<p class="text-success">`+data.message+`</p>
						`);
					}

					setTimeout(refreshFeeds(), 500);
				},
				error: function (data){
					// console.log(data);
					alert('fail to post ');
				}
			});

			return false;
		}

		function refreshFeeds() {
			// body...
			$.get('/load/comments/'+titleId, function (data){
				// console.log(data);
				$('.load-comments').html('');
				$.each(data, function (index, value){
					$('.load-comments').append(`
						<div class="row">
							<div class="col-xm-2">
								<a><i class="fa fa-user"></i> `+value.name+`</a>
							</div>
						</div>
						<div class="row">
							<div class="col-xm-2">
								<p>`+value.body+`</p>
								<span class="small"><i class="fa fa-clock-o"></i> `+value.date+`</span>
							</div>
						</div>
						<br /><br />
					`);
				});
			});
		}

		$.get('/load/forum-post', function (data){
			// console.log(data);
			$(".list-topics").html("");
			$(".top-recent").html("");
			$.each(data, function (index, value){
				$(".top-recent").append(`
					<div style="box-shadow:1px 1px 2px 1px #CCC;padding:1em;">
					 <a href="/forum/reply/post/`+value.id+`"><i class="fa fa-tags"></i>	`+value.title+`</a>
					</div>
					<br />
				`);
			});
		});
	</script>
	<div style="height: 120px;">
		
	</div>
@endsection
