@extends('layouts.demo-ntrade-skin')

@section('title')
    CATSS Live Feeds
@endsection

@section('contents')
  <style type="text/css">
    .flash_demo {
      background-color:rgba(255,255,255, 0.8);
      position: absolute;
      top: 80px;
      right: 10px;
      z-index: 10;
      height: 260px;
      width: 250px;
      margin-right: 20px;
      box-shadow: 1px 1px 3px 1px #FFF;
    }
  </style>

  <div class="alert alert-info flash_demo" id="flash_demo">
    <span class="pull-right"><a href="#" id="closeFlash">close</a></span>
    <br />
    <p class="text-info">

     <b> CATSS Group version</b>
      <br /><br />
      <ul class="list-inline">
        <li><br />Create a Group or Join a group </li>
        <li><br />Learn from the stock broker, professionals and trade expert</li>
        <li><br />Each Stage completion will unlock the next stage. </li>
      </ul>
    </p>
  </div>

  @if(session('error_status'))
    <div class="alert alert-danger col-sm-10">
      <p class="text-danger"> {{ session('error_status') }} </p>
    </div>
  @endif

  @if(session('update_status'))
    <div class="alert alert-success col-sm-10">
      <p class="text-success"> {{ session('update_status') }} </p>
    </div>
  @endif

  <script type="text/javascript">
   $("#closeFlash").click(function (){
    $("#flash_demo").hide();
   });

   function createLesson(){
    $("#lesson").toggle();
    $("#lesson-list").hide();
   }

   function showLessonList(){
    window.location.href = '/ranking'
   }
  </script>


  <div class="row">
    <div class="col-md-6">
      <button class="btn btn-success" onclick="return createLesson()" style="height: 50px; background-color: rgba(255,505,505,0.70); color:blue;"><i class="fa fa-users"></i> Create Trading Group </button>
    </div>
    <div class="col-md-6">
      <button class="btn btn-success" onclick="return showLessonList()" style="height: 50px; background-color: rgba(255,505,505,0.70); color:blue;"><i class="fa fa-trophy"></i> Ranking</button>
    </div>
  </div>
  <br />
  <br />

  <div class="row" id="lesson" style="display: none;">
    <div class="col-md-10 alert alert-info" style=" background-color: rgba(255,505,505,0.70); color:blue;">
      <form action="/create/trade_group" method="post">
        {{ csrf_field() }}
        <div class="form-group">
          <div class="row">
            <div class="col-sm-2">
              <p>Group Name</p>
            </div>
            <div class="col-sm-4">
              <input type="text" placeholder="Group Name" class="form-control" name="group_name" required="" maxlength="10">
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-sm-2">
              <p>Send Invitations</p>
            </div>
            <div class="col-sm-8">
              <textarea class="form-control" rows="5" cols="30" placeholder="Paste email addresses here, separated by comma. Eg someon@example.com, another@example.com " name="group_memebers"></textarea>
            </div>
          </div>
        </div>

        <div class="form-group">
          <div class="row">
            <div class="col-sm-2">
            </div>
            <div class="col-sm-10">
              <button class="btn btn-primary">Save & Send Invitations Links </button>
            </div>
          </div>
        </div>
      </form> 
    </div>
  </div>

  <div class="row" id="lesson-list" style="display: none; ">
    <div class="col-md-10 alert alert-info">
      <form action="#" method="post">
        <table class="table">
          <thead>
            <tr>
              <th>S/N</th>
              <th>Name</th>
              <th>Bage</th>
              <th>&#8358; Profits</th>
              <th>date</th>
            </tr>
          </thead>
          <tbody></tbody>
        </table>
      </form>
    </div>
  </div>
  <script type="text/javascript">
      $.get('/load/ranking', function (data){
        // console.log(data);
        $('.ranking-list').html(``);
        var sn    = 0;
        var total = data.length;
        $.each(data, function (index, value){
            sn++;
            $('.ranking-list').append(`
                <tr style="color:gold;">
                    <td>`+sn+`</td>
                    <td><i class="fa fa-user"></i> `+value.name+`</td>
                    <td><i class="fa fa-angle-up text-success"></i> &#8358 `+value.amount+`</td>
                    <td>`+value.date+`</td>
                </tr>
            `);
            $('.count-memebers').html(`Top `+sn+` Trade Expert this week`);
        });
      });
    </script>

  <br />

  <div class="row">
    <div class="col-md-8">
      <div class="alert alert-info" id="flash_demo">
        <br />
        <p class="text-info">

         <b> CATSS Group version</b>
          <br /><br />

            <p>Create a Group or Join a group </p><br />
            <p>Learn from the stock broker, professionals & trade experts.</p><br />
            {{-- <p>Each Stage completion will unlock the next stage. </p><br /> --}}
        </p>
      </div>
    </div>
  </div>
@endsection