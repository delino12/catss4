@extends('layouts.catss-skin')

@section('title')
  CATSS Online Wallets
@endsection

@section('contents')
<br /><br /><br />
<div class="container-fluid">
  <div class="row text-center">
    {{-- trading balamce --}}
    <div class="col-md-4">
      <div class="panel panel-info">
        <div class="panel-heading">
          <i class="fa fa-money"></i> Balance <br />
          <span class="trading-balance"></span>
        </div>
        <div class="panel-body">

        </div>
      </div>
    </div>

    {{-- Wallet --}}
    <div class="col-md-4">
      <div class="panel panel-info">
        <div class="panel-heading">
          <i class="fa fa-clock"></i> Quota<br />
          <span class="wallet-balance"></span>
        </div>
        <div class="panel-body">

        </div>
      </div>
    </div>

    {{-- Balance --}}
    <div class="col-md-4">
      <div class="panel panel-info">
        <div class="panel-heading">
          <i class="fa fa-money"></i> Earnings <br />
          <span class="total-earning">&#8358; 00.00</span>
        </div>
        <div class="panel-body">

        </div>
      </div>
    </div>
  </div>
  <hr />
  <br /><br /><br /> <br />
  <div class="row text-center">
    <div class="col-md-6">
      <img src="/img/deposit.png" class="img"><br /><br />
      <form method="post" onsubmit="return deposit()">
        <input type="number" id="fund_amount" style="padding: 8px; width: 200;border-radius: 5px;border:1px solid #999;margin-bottom: 5px;" placeholder="0.00" name="" required=""><br />
        <button class="btn btn-default col-md-6">Deposit</button>
      </form>
    </div>
    <div class="col-md-6">
      <img src="/img/withdraw.png" class="img"><br /><br />
      <form method="post" onsubmit="return false">
        <input type="number" id="withdraw_amount" style="padding: 8px; width: 200;border-radius: 5px;border:1px solid #999;margin-bottom: 5px;" placeholder="0.00" name=""><br />
        <button class="btn btn-default col-md-6" onclick="withdraw()">Withdraw</button>
      </form>
    </div>
  </div>

  {{-- wallet service --}}
</div>
<br /><br />
{{-- paystack integration --}}
<script src="https://js.paystack.co/v1/inline.js"></script>
<script type="text/javascript">
  // load modules
  loadPayment();
  loadAccountBalance();
  loadWalletBalance();

  // start initliazing payments
  function deposit() {
    // navigate to payment system
      var amount = parseFloat($("#fund_amount").val());
      var handler = PaystackPop.setup({
        key: 'pk_test_ec6da628ba73e57a21663ec7ed569a4fc56645b8',
        // key: 'pk_live_35fc2795afc2ad31cf850bdd9efeac5693ead4db',
        email: '{{ Auth::user()->email }}',
        amount:  (amount * 100),
        ref: 'CATSS-'+Math.floor((Math.random() * 1000000000) + 1),
        metadata: {
          custom_fields: [
            {
              display_name: '',
              variable_name: '',
              value: ''
            }
          ]
        },
        callback: function(response){
          // alert('window closed');
          logPayment(response.reference, amount);
        },
        onClose: function(){
          // alert('window closed');
        }
    });
    
    handler.openIframe();


    // return
    return false;
  }

  // function check withdraw
  function withdraw() {
    // body...
    alert('Wallet balance must be min. of NGN2,000.00 !!!');
  }

  // start loggin payments
  function logPayment(refId, amount) {
    // set data
    var token  = '{{ csrf_token() }}';
    var email  = '{{ Auth::user()->email }}';
    var refId  = refId;
    
    // data to json
    var data = {
      _token:token,
      email:email,
      refId:refId,
      amount: amount
    }

    // data to ajax
    $.ajax({
      type: 'POST',
      url: '/log-payment/details',
      dataType: 'json',
      data: data,
      success: function (data) {
        // console.log(data);
        if(data.status == 'success'){
          swal(
            "success",
            data.message,
            data.status
          );
          loadAccountBalance();
        }else{
          swal(
            "oops",
            data.message,
            data.status
          );
        }
      },
      error: function (data) {
        // console.log(data);
        alert('Error, fail to send request !');
      }
    });
  }

  // load payment
  function loadPayment() {
    $.get('/load/payment', function (data){
      /* load url json response */
      // console.log(data);
      $('.wallet-balance').append(`
        <i class="fa fa-database"></i> `+data.quota+` remaining..
      `);
    });
  }

  // load account balance
  function loadAccountBalance() {
    $.get("/accountbalance", function (data){
      // load account balance
      $(".trading-balance").append(`
        &#8358; `+data.account_balance+`
      `);
    });
  }

  // load wallet balance
  function loadWalletBalance() {
    $.get('/load/wallet/balance', function (data){
      /* load url json response */
      // console.log(data);
      $('.total-earning').html(`
        &#8358; `+data.balance+`
      `);
    });
  }
</script>
@endsection
