@extends('layouts.catss-cover-skin')

@section('title')
    Account Recovery | CATSS
@endsection

@section('contents')
  <div>
    <div class="page login-page">
      <div class="container d-flex align-items-center">
        <div class="form-holder has-shadow">
          <div class="row">
            <!-- Logo & Information Panel-->
            <div class="col-lg-6">
              <div class="info d-flex align-items-center">
                <div class="content">
                  <div class="logo">
                    <h1>CATSS will help you get back into your account</h1>
                  </div>
                  <p>Trade Smart, Trade More</p> 
                  <p> Specify your email address</p>
                </div>
              </div>
            </div>
            <!-- Form Panel    -->
            <div class="col-lg-6 bg-white">
              <div class="form d-flex align-items-center">
                <div class="content">
                    @if (session('status'))
                        <p class="text-success small">{{ session('status') }}</p>
                    @endif
                    @if (session('login-status'))
                        <p class="text-danger small">{{ session('login-status') }}</p>
                    @endif
                    @if ($errors->any())
                        @foreach ($errors->all() as $error)
                            <p class="text-danger">{{ $error }}</p>
                        @endforeach
                    @endif
                    @if (session('search_status'))
                        <p class="text-success">
                            {{ session('search_status') }}
                        </p>
                    @endif
                  <form id="login-form" method="post" action="/search-account">
                    {{ csrf_field() }}
                    <div class="form-group">
                      <input id="login-username" type="email" name="email" required="" class="input-material">
                      <label for="login-username" class="label-material">Email</label>
                    </div>
                    <!-- This should be submit button but I replaced it with <a> for demo purposes-->
                        <button class="btn btn-primary">Recover My Account </button>
                  </form>
                  <hr />
                  <small>I Do have an account </small>
                  <a href="/login-account" class="signup">Sigin</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyrights text-center">
        <p>Design by <a href="https://cavidel.com" class="external">Cavidel</a></p>
      </div>
    </div>
  </div>
@endsection



