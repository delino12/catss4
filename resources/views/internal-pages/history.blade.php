@extends('layouts.catss-skin')

@section('title')
  CATSS Stock History
@endsection

@section('contents')
<style type="text/css">
  .eq {
    border-radius: 4px; 
    padding: 0.4em; 
    box-shadow: 1px 1px 3px 1px #CCC;
  }

  .w_a {
    font-size: 48px;
    color: #3e3344;
  }
</style>

<br /><br />
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <h1>{{ $pair }}</h1>
      <hr />
        @foreach($transaction_history as $history)
          <div class="eq"> 
            @if($history->stock_trade == 'buy')
              Purchase {{ $pair }}
            @else
              Sold {{ $pair }}
            @endif
            <table class="table">
              <thead>
                <tr>
                  <th>Quantity</th>
                  <th>Price</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><i class="fa fa-database"></i> {{ number_format($history->stock_qty) }}</td>
                  <td>&#8358; {{ number_format($history->stock_unit, 2) }}</td>
                  <td bgcolor="#CCC">&#8358; {{ number_format($history->stock_amount, 2) }}</td>
                </tr>
              </tbody>
            </table>
            <br />
            <span class="small pull-right">{{ $history->created_at->diffForHumans() }}</span>

            <br /><br />
          </div>
          <br />
        @endforeach
    </div>
    <div class="col-md-6">
      <h1>Current W/Average</h1>
      <hr />
      <div class="panel panel-info panel-heading">
        <div class="w_average"></div>
      </div>
    </div>
  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.1/Chart.min.js"></script>
<script type="text/javascript">
  $.get('/load/users/stocks', function (data){
    // alert('loading ajsx');
    // console.log(data);
    var pair = '{{ $pair }}';
    $.each(data, function(index, val) {
       /* iterate through array or object */
       if(val.equity == pair){
        $(".w_average").html(`
          <span class="w_a">&#8358;`+val.w_average+` </span>
        `);
       }
    });
  });
</script>
@endsection
