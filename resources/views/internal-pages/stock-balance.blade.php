@extends('layouts.catss-skin')

@section('title')
  CATSS Stock Statments
@endsection

@section('contents')
<br />
<div class="container-fluid"> 
  <div class="row">
    <div class="col-md-12 small">
      <div class="panel panel-info card card-body">
        <div class="panel-heading">
            <i class="fa fa-money"></i> Equity Valuation (Worth) Using W/Average 
        </div>
        <div class="panel-body">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>Equity</th>
              <th>Qty</th>
              <th>W/Average(&#8358;)</th>
              <th>Mkt Price(&#8358;) </th>
              <th>Market Value</th>
              <th>Holding Value </th>
              <th>Profit/Loss</th>
              <th>Comments </th>
            </tr>
            </thead>
            <tbody>
              @foreach($stock_basket as $data)
                @if( $data['stock'] == 0)

                @else
                  <tr>
                    <td><a href="/transactions/history/{{ $data['equity'] }}">{{ $data['equity'] }}</a></td>
                    <td>{{ number_format($data['stock']) }}</td>
                    <td>&#8358; {{ number_format($data['w_average'], 2) }}</td>
                    <td>&#8358; {{ number_format($data['price'], 2) }}</td>
                    <td>&#8358; {{ number_format($data['amount'], 2) }}</td>
                    <td>&#8358; {{ number_format($data['stock'] * $data['w_average'], 2) }}</td>
                    
                    @if($data['stock'] * $data['w_average'] < $data['amount'])
                      <td><p class="text-success">&#8358; {{ number_format($data['swa'], 2) }}</p></td>
                    @elseif($data['stock'] * $data['w_average'] == $data['amount'])
                      <td><p class="text-info">&#8358; {{ number_format($data['swa'], 2) }}</p></td>
                    @else
                      <td><p class="text-danger">&#8358; -{{ number_format($data['swa'], 2) }}</p></td>
                    @endif
                    
                    @if($data['stock'] * $data['w_average'] < $data['amount'])
                      <td> <p class="text-success">profit</p> </td>
                    @elseif($data['stock'] * $data['w_average'] == $data['amount'])
                      <td> <p class="text-info">break-even</p> </td>
                    @else
                      <td> <p class="text-danger">loss</p> </td>
                    @endif
                  </tr>
                @endif
              @endforeach
              <tr style="background-color: #000;">
                  <th>Summary (Last updated)</th>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td> Profit: <p class="text-success">{{ number_format($total_profit, 2) }}</p></td>
                  <td> Loss: <p class="text-danger">{{ number_format($total_loss, 2) }}</p></td>
                  <td> 
                    Net: 
                    @if($total_profit > $total_loss)
                      <p class="text-success">{{ number_format($total_profit - $total_loss, 2) }}</p>
                    @else
                      <p class="text-danger">{{ number_format($total_profit - $total_loss, 2) }}</p>
                    @endif
                  </td>
                </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<br /><br />
<script type="text/javascript">
  $.get('/load/users/stocks', function (data){
    // alert('loading ajsx');
    // console.log(data);
  });

  $.get('/load/stock/qty', function(data) {
    /*optional stuff to do after success */
    // console.log('qty: '+data);
    $(".total_qty").text(data.total_stock);
  });
</script>
@endsection
