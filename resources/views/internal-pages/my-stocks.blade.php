@extends('layouts.catss-skin')

@section('title')
  CATSS Personal Holding Stock
@endsection

@section('contents')
	<style type="text/css">
		.push {
			width: 200px;
			position: absolute;
			top: 140px;
			right: 20px;
			z-index: 20;
		}
	</style>

	@if(session('success'))
		<div class="alert alert-success push">
			<p class="text-success">{{ session('success') }}</p>
		</div>
	@endif

	@if(session('warning'))
		<div class="alert alert-warning push">
			<p class="text-warning">{{ session('warning') }}</p>
		</div>
	@endif

	<div class="container-fluid">
	  <br /><br />
	  <p>Update Personal Acquired Stock informations here</p>
	  <p class="small">We can simulate your real-time personal stock with real-time market value</p>
	  <div class="row">
	  	<div class="col-md-4 card card-body">
	  		<br />
	  		<h3 class="small">Add Equity (Realtime Equity Assets)</h3>
	  		<hr />
	  		<span id="status" class="small text-success" style="display: none;"></span>
	  		<form action="#" method="post" onsubmit="return saveStock()" id="assets-form">
	  			{{ csrf_field() }}
	  			<div class="form-group">
	  				<label>Equity Symbols</label>
	  				<select class="form-control" id="stock_name"></select>
	  			</div>

	  			<div class="form-group">
	  				<label>&#8358; Acquired Price</label>
	  				<input type="text" pattern="[0-9]+(\.[0-9]{0,2})?%?" id="stock_buy_price" placeholder="00.00" class="form-control" required="">
	  			</div>

	  			<div class="form-group">
	  				<label>&#8358; Released Price</label>
	  				<input type="text" pattern="[0-9]+(\.[0-9]{0,2})?%?" id="stock_sell_price" placeholder="00.00" class="form-control" required="">
	  			</div>

	  			<div class="form-group">
	  				<label><i class="fa fa-cubes"></i> Stock Units (QTY)</label>
	  				<input type="number" pattern="[0-9]*" id="stock_qty" placeholder="00.00" class="form-control" required="">
	  			</div>

	  			<div class="form-group">
	  				<button class="btn btn-primary">
	  					<img src="/img/loading.gif" width="20" height="20" class="spin-loading" style="display: none;">
	  					Add Stock
	  				</button> 
	  			</div>
	  		</form>
	  	</div>
	  	<div class="col-md-1"></div>
	  	<div class="col-md-7 card card-body small">
	  		<h3 class="lead">My Realtime Stock</h3>
	  		<table class="table">
	  			<thead>
	  				<tr>
	  					<th>Equity</th>
	  					<th>Qty</th>
	  					<th>&#8358; Acquired at </th>
	  					<th>&#8358; Released at </th>
	  					<th>Date</th>
	  				</tr>
	  			</thead>
	  			<tbody class="assets-body"></tbody>
	  		</table>
	  	</div>
	  </div>
	  <br /><br />
	  <div class="row">
	  	<div class="col-md-12" style="box-shadow: 1px 1px 3px 1px #ccc; "><br />
	  		<h3 class="lead">Realtime Stock, Financial Evaluation at Holding Cost</h3>
	  		<table class="table small">
		  		<thead>
	  				<tr>
	  					<th>Equity</th>
	  					<th>Qty</th>
	  					<th>Asset unit price (&#8358;)</th>
	  					<th>M2M price (&#8358;) </th>
	  					<th>Mkt at current (&#8358;)</th>
	  					<th>Amount (&#8358;)</th>
	  					<th>P/L</th>
	  					<th>Date</th>
	  					<th>Option</th>
	  				</tr>
	  			</thead>
	  			<tbody class="eval-assets-body"></tbody>

	  		</table>
	  	</div>
	  </div>
	  <br />
	</div>

	<script type="text/javascript">
	    // save stocks 
		function saveStock(){
			$(".spin-loading").show();

			var token 			= $("input[name=_token").val();
			var stockName 		= $("#stock_name").val();
			var stockBuyPrice 	= $("#stock_buy_price").val();
			var stockSellPrice 	= $("#stock_sell_price").val();
			var stockQty 		= $("#stock_qty").val();

			$.ajax({
				type: "POST",
				url: "/save-personal-stock",
				data: {
					_token: token,
					stockName:stockName,
					stockBuyPrice:stockBuyPrice,
					stockSellPrice:stockSellPrice,
					stockQty:stockQty
				},
				cache: false,
				success: function (data){
					$(".spin-loading").hide();
					$("#status").html(data).show();
					// console.log(data);
					$("#assets-form")[0].reset();
				},
				error: function (data){
					alert('fail to post data...');
					$(".spin-loading").hide();
				}
			});
			return false;
		}

		// load realtime stock update
		$.get('/realtime/stock', function (data){
			$(".assets-body").html("");
			$(".eval-assets-body").html("");
			$.each(data, function (index, value){
				// console.log(value);
				$(".assets-body").append(`
					<tr>
	  					<td>`+value.name+`</td>
	  					<td>`+value.qty+`</td>
	  					<td>`+value.buy+`</td>
	  					<td>`+value.sell+`</td>
	  					<td>`+value.date+`</td>
	  				</tr>
				`);

				var status = value.status;

				if(status == 'profit'){
					status = '<span class="text-success">'+status+'</span>';
				}else{
					if(status == 'loss'){
						status = '<span class="text-danger">'+status+'</span>';
					}else{
						status = '<span class="text-info">'+status+'</span>';
					}
				}
				
				$(".eval-assets-body").append(`
					<tr>
	  					<td>`+value.name+`</td>
	  					<td>`+value.qty+`</td>
	  					<td>`+value.buy+`</td>
	  					<td>`+value.sell+`</td>
	  					<td>`+value.bal+`</td>
	  					<td>`+value.amount+`</td>
	  					<td>`+status+`</td>
	  					<td>`+value.date+`</td>
	  					<td><a href="javascript:void(0);" onclick="notYet()">Release</a></td>
	  				</tr>
				`);
			});
		});

		// Pusher 
		// Pusher.logToConsole = true;

		var pusher = new Pusher('0495dd188ce5c0c6c192', {
			encrypted: true
		});

		var channel = pusher.subscribe('new-assets');
		channel.bind('App\\Events\\NewAssets', function(data) {
			$(".assets-body").prepend(`
				<tr  class="text-success">
  					<td>`+data.name+`</td>
  					<td>`+data.qty+`</td>
  					<td>`+data.buy+`</td>
  					<td>`+data.sell+`</td>
  					<td>`+data.date+`</td>
  				</tr>
			`);
		});

		// get securities 
		$.get('/load/security/all', function(data) {
		    /*optional stuff to do after success */
		    // console.log(data);
		    $("#stock_name").html();
		    $.each(data, function(index, val) {
		      	// console.log(val);
		      	$("#stock_name").append(`
		        	<option value="`+val.security+`"> `+val.security+`  (&#8358; `+val.price+`)</option>
		      	`);
		    });
		});

		function notYet() {
			// body...
			alert('You can not release stock to CATSS Market now. Features is allowed for premium users only');
		}
	</script>

@endsection

