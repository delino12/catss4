@extends('layouts.catss-skin')

@section('title')
  CATSS | Clients
@endsection

@section('contents')
<br /><br />
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <br />
      <div class="card">
        <div class="card-header">
          <p><i class="fa fa-users"></i> Hire a broker</p>
        </div>
        <div class="card-body">
          <table class="table" id="brokers-table">
            <thead>
              <tr>
                <td>SN</td>
                <td>Brokerage Firm</td>
                <td>Stock Broker</td>
                <td>Client Rating</td>
                <td>Option</td>
              </tr>
            </thead>
            <tbody class="load-all-stock-broker">
              <tr>
                <td>Loading...</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
{{-- scripts --}}
<script type="text/javascript">

  // load modules
  loadStockBroker();

  // load all stock brokers
  function loadStockBroker() {
    $.get('{{ url('load/all/stock/brokers') }}', function(data) {
      $(".load-all-stock-broker").html("");
      var sn = 0;
      $.each(data, function(index, val) {
        sn++;
        $(".load-all-stock-broker").append(`
          <tr>
            <td>${sn}</td>
            <td>${val.firm}</td>
            <td>${val.name}</td>
            <td>${val.rating}</td>
            <td>
              <a href="javascript:void(0);" onclick="hireService(${val.id})" class="btn btn-default btn-sm">
                <i class="fa fa-chart-line text-success"></i> Hire
              </a>
            </td>
          </tr>
        `);
      });

      // $("#brokers-table").datatable();
    });
  }

  function hireService(agent_id) {
    swal({
      title: 'Are you sure?',
      text: "This contract will attract a cost fee",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Hire Agent'
    }).then((result) => {
      if (result.value) {
        var token = $("#token").val();
        var params = {
          _token: token,
          broker_id: agent_id
        };
        // hire agent
        $.post('{{ url('hire/stock/broker') }}', params, function(data, textStatus, xhr) {
          /*optional stuff to do after success */
          if(data.status == "success"){
            swal(
              "success",
              data.message,
              data.status
            );
          }else{
            swal(
              "oops",
              data.message,
              data.status
            );
          }
        });
      }
    })
    
  }
</script>
@endsection
