@extends('layouts.catss-cover-skin')

@section('title')
    Login CATSS
@endsection

@section('contents')
<div>
  <div class="page login-page">
    <div class="container d-flex align-items-center">
      <div class="form-holder has-shadow">
        <div class="row">
          <!-- Logo & Information Panel-->
          <div class="col-lg-6">
            <div class="info d-flex align-items-center">
              <div class="content">
                <div class="logo">
                  <h1>Login CATSS</h1>
                </div>
                <p>Trade Smart, Trade More</p> 
                <p> Join CATSS Now and earn up to N10,000.00 Weekly, Play Smart, Work Smart</p>
              </div>
            </div>
          </div>
          <!-- Form Panel    -->
          <div class="col-lg-6 bg-white">
            <div class="form d-flex align-items-center">
              <div class="content">
                <hr />
                @if (session('status'))
                    <p class="text-success small">{{ session('status') }}</p>
                @endif

                @if (session('error_status'))
                    <p class="text-danger small">{{ session('error_status') }}</p>
                @endif
                @if ($errors->any())
                    <div class="small">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li class="text-danger">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form id="login-form" method="POST" action="/login-account">
                  {{ csrf_field() }}
                  <div class="form-group">
                    <input id="login-username" type="email" name="email" required="" class="input-material">
                    <label for="login-username" class="label-material">Email</label>
                  </div>
                  <div class="form-group">
                    <input id="login-password" type="password" name="password" required="" class="input-material">
                    <label for="login-password" class="label-material">Password</label>
                  </div>
                  <button class="btn btn-primary">Login </button> 
                  
                  <br /><br />
                  <div class="social-login">
                    <div class="g-signin2" style="float: left; width: 90px;height: 25px;" data-onsuccess="onSignIn"></div>
                    <div align="center"> 
                      <a class="signup" href="javascript:void(0);"> 
                        <fb:login-button 
                          scope="public_profile,email"
                          onlogin="checkLoginState();">
                        </fb:login-button>
                        Login via Facebook
                      </a>
                    </div>
                  </div>
                  <hr />
                  <!-- This should be submit button but I replaced it with <a> for demo purposes-->
                </form>
                <div class="login-option" style="display: none;">
                  <form onsubmit="return false" method="post">
                    <div class="form-group">
                      <input value="+234" class="input-material col-md-2" id="country_code" />
                      <input placeholder="enter phone number" class="input-material col-md-6" id="phone_number"/>
                    </div>

                    <div class="form-group">
                      <br />
                      <button class="btn btn-primary" onclick="smsLogin();">Login via SMS</button>
                    </div>

                  </form>
                </div>
                <a href="/login-account-reset" class="forgot-pass">Forgot Password?</a><br>
                {{-- <small>Do not have an account? </small>
                <a href="/create-account" class="signup">Signup</a> <br /> <br /> --}}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div >
      <div class="copyrights text-center" style="padding: 0.5em;background-color: rgba(000,000,000,0.90);">
          <p>Design by <a href="https://cavidel.com" class="external">Cavidel</a></p>
        </div>
    </div>
  </div>
</div>

{{-- login via facebook --}}
<script type="text/javascript">
  // init google login
  function onSignIn(googleUser) {
    var profile = googleUser.getBasicProfile();
    console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
    console.log('Name: ' + profile.getName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.

    loginViaEmail(profile.getEmail());
  }

  // initialize Account Kit with CSRF protection

  // get login status
  $(document).ready(function($) {
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
        console.log(response);
    });
  });

  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  function statusChangeCallback(response) {
    // body...
    if(response.status === 'connected'){
      // setElements(true);
      let userId = response.authResponse.userID;
      // console.log(userId);
      console.log('login');
      getUserInfo(userId);
      
    }else{
      // setElements(false);
      console.log('not logged in !');
    }
  }

  function getUserInfo(userId) {
    // body...
    FB.api(
      '/'+userId+'/?fields=id,name,email',
      'GET',
      {},
      function(response) {
        // Insert your code here
        // console.log(response);
        let email = response.email;
        loginViaEmail(email);
      }
    );
  }

  // login option
  function showLoginOption() {
    // body...
    $("#login-form").hide();
    $(".login-option").show();
  }

  // login via phone 
  function  loginViaPhone(){
    let token = '{{ csrf_token() }}';
    let phone = '08127160258';

    let data = {
      _token:token,
      phone:phone
    }

    $.ajax({
      url: '/login/via/phone',    
      type: 'POST',
      dataType: 'json',
      data: data,
      success: function(data){
        // console.log(data);
      },
      error: function(data){
        console.log(data);
        alert('Fail to send login request !');
      }
    });
  }

  // login via email
  function loginViaEmail(email) {
    // body...
    let token = '{{ csrf_token() }}';

    let data = {
      _token:token,
      email:email
    }

    $.ajax({
      url: '/login/via/email',    
      type: 'POST',
      dataType: 'json',
      data: data,
      success: function(data){
        console.log(data);
        if(data.status == 'success'){
          window.location.href = '/dashboard';
        }

        if(data.status == 'info'){
          window.location.href = '/create-account'; 
        }
      },
      error: function(data){
        console.log('Error logging in via email !');
        // alert('Fail to send login request !');
      }
    });
  }
</script>
@endsection