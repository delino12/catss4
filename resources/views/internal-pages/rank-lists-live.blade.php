@extends('layouts.ntrade-skin')

@section('title')
    CATSS Live Feeds
@endsection

@section('contents')
  <style type="text/css">
    .flash_demo {
      background-color:rgba(255,255,255, 0.8);
      position: absolute;
      top: 120px;
      right: 10px;
      z-index: 10;
      height: 260px;
      width: 250px;
      margin-right: 20px;
      box-shadow: 1px 1px 3px 1px #FFF;
    }
  </style>

  <div class="alert alert-info flash_demo" id="flash_demo">
    <span class="pull-right"><a href="#" id="closeFlash">close</a></span>
    <p class="text-info">
     <b> CATSS Group version</b>
      <br /><br />
      <ul class="list-inline">
        <li><br />Create a Group or Join a group </li>
        <li><br />Learn from the stock broker, professionals and trade expert</li>
        <li><br />Each Stage completion will unlock the next stage. </li>
      </ul>
    </p>
  </div>
  <div class="modal-content" style="background-color:rgba(000,000,000,0.80);color:#FFF;">
    <p>Today's Ranking... </p>
    <table class="table index-pairs">
      <thead>
        <tr>
          <th>S/N</th>
          <th><i class="fa fa-users"></i> Name</th>
          <th>Profit / loss (&#8358;)</th>
          <th>Date</th>
        </tr>
      </thead>
      <tbody class="list-ranking"></tbody>
    </table>
  </div>

  <script type="text/javascript">
    // load ranking 
    $.get('/load/ranking', function(data) {
        /*optional stuff to do after success */

        // sort data 
        var data = data.ranking.sort(function (a, b){
            return b.profit - a.profit; 
        });

        var sn = 0;
        $.each(data, function(index, el) {
             // console.log(el);
            sn++;
            var name   = el.user;
            var profit = el.profit.toLocaleString();
            var date   = el.date;

            // show only users with actual figures
            if(el.profit > 0){
              $(".list-ranking").append(`
                  <tr>
                      <td>`+sn+`</td>
                      <td>`+name+`</td>
                      <td>&#8358;`+profit+`</td>
                      <td>`+date+`</td>
                  </tr>
              `);
            }else{
              $(".list-ranking").append(`
                  <tr>
                      <td>`+sn+`</td>
                      <td>`+name+`</td>
                      <td><span class="text-danger"> &#8358; `+profit+` </span></td>
                      <td>`+date+`</td>
                  </tr>
              `);
            }
        });
    });

    $("#closeFlash").click(function (){
      $("#flash_demo").hide();
    });

    $(function() {
      var on = false;
      window.setInterval(function() {
          on = !on;
          if (on) {
              $('.invalid').addClass('invalid-blink')
          } else {
              $('.invalid-blink').removeClass('invalid-blink')
          }
      }, 1000);
    });

    window.setTimeout(function (){
      modal.style.display = "block";
      $("#close-modal").click(function (){
          modal.style.display = "none";
      });
    }, 1000 * 5);

    // Get the modal
    var modal = document.getElementById('myModal');
    window.onclick = function(event) {
      if (event.target == modal) {
          modal.style.display = "none";
      }
    }

    // account balance state
    $.get("/accountbalance", function (data){
      $("#ac").text(data.account_balance);
      $(".account_balance").text(data.account_balance);
    });

    // show news notifications
    $.get('/news/notifications', function (data){
      $(".load-news").html("");
      $.each(data, function (index, value){
        $(".load-news").append(
          '\
          <h3>'+value.news_title+'</h3>\
          <p>'+value.news_body+'</p>\
          <span class="small">'+value.news_date+'</span>\
          '
        );
      });
    });
  </script>
@endsection