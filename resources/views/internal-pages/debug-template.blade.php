@extends('layouts.catss-skin')

@section('title')
    Dashboard
@endsection

@section('contents')
  <br />
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4">
        <div class="activate-form"></div>
      </div>
      <div class="col-md-8">
        <div class="activation-div"></div>
        <div class="invites-box"></div>
        <script type="text/javascript">
          $.get('/account-status', function(data){
              // console.log(data);
              if(data.status == 'error'){
                $(".activation-div").html(`
                    <div class="alert alert-danger">`+data.message+`</div> 
                `);

                $(".activate-form").html(`
                  <form method="post" action="/activate-account">
                    <p>Enter Confirmation code</p>
                    <div class="form-group">
                      {{ csrf_field() }}
                      <input type="text" placeholder="00-000" class="form-control col-md-5" required="" name="code">
                    </div>
                    <div class="form-group">
                      <button class="btn btn-warning">Verify Account</button>
                    </div>
                  </form>
                `);
              }
          });

          $.get('/check-invites/users', function (data){
            $('.invites-box').html('');
            $.each(data, function (index, value){
              $('.invites-box').append(`
                <div class="alert alert-info small">
                 You have 1 new Group Trading Invitation from <b> `+value.host+`</b> about `+value.date+`
                 <br /><br />
                 <a href="/accept-invites/`+value.id+`"><button class="btn btn-success">Accept</button></a>
                 <a href="/reject-invites/`+value.id+`"><button class="btn btn-danger">Reject</button></a> 
                </div> 
              `);
            });
          });
        </script>
      </div>
    </div>
  </div>

  <!-- Updates Section                                                -->
  <section class="updates no-padding-top">
    <div class="container-fluid">
      <div class="row">
        <!-- Recent Updates-->
        <div class="col-lg-4">
          <div class="recent-updates card">
            <div class="card-close">
              <div class="dropdown">
                <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
              </div>
            </div>
            <div class="card-header">
              <h3 class="h4" data-step="15" data-intro="get latest news updates">News Updates</h3>
            </div>
            <div class="card-body no-padding">
              @if($news)
                @foreach($news as $new)
                  <!-- Item-->
                    <div class="item d-flex justify-content-between">
                      <div class="info d-flex">
                        <div class="icon"><i class="icon-rss-feed"></i></div>
                        <div class="title">
                          <h5>{{ $new->title }}</h5>
                          <p>{{ $new->body }}</p>
                        </div>
                      </div>
                      <div class="date text-right"><span class="small">
                       {{ $new->updated_at->diffForHumans() }}
                      </span></div>
                    </div>
                @endforeach
              @else
                <!-- Item-->
                  <div class="item d-flex justify-content-between">
                    <div class="info d-flex">
                      <div class="icon"><i class="icon-rss-feed"></i></div>
                      <div class="title">
                        <h5>No news update yet</h5>
                        <p>....</p>
                      </div>
                    </div>
                    <div class="date text-right"><span class="small">
                     ...
                    </span></div>
                  </div>
              @endif
                
            </div>
          </div>
        </div>
        <!-- Daily Feeds -->
        <div class="col-lg-4">
          <div class="daily-feeds card"> 
            <div class="card-close">
              <div class="dropdown">
                <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
              </div>
            </div>
            <div class="card-header">
              <h3 class="h4" data-step="16" data-intro="follow forum discussion updates">Forum Discussion </h3>
            </div>
            <div class="card-body no-padding">
              <!-- Item-->
              <div class="list-topics"></div>
            </div>
          </div>
        </div>

        <script type="text/javascript">
          $.get('/load/forum-post', function (data){
            // console.log(data);
            $(".list-topics").html("");
            $(".top-recent").html("");
            $.each(data, function (index, value){
              // console.log(value);
              if(value.avatar == null){
                var avatar = '<img src="http://www.iconninja.com/files/373/611/612/person-user-profile-male-man-avatar-account-icon.svg" alt="person" class="img-fluid rounded-square">';
              }else{
                var avatar = '<img src="/uploads/'+value.avatar+'" alt="person" class="img-fluid rounded-square">';
              }

              // console.log(data);
              $(".list-topics").append(`
                <div class="item">
                  <div class="feed d-flex justify-content-between">
                    <div class="feed-body d-flex justify-content-between">
                      <a href="#" class="feed-profile">
                        `+avatar+`
                      </a>
                      <div class="content">
                        <a class="link" href="/forum/reply/post/`+value.id+`">
                        <h5> `+value.name+`</h5><span class="small">Posted a new discussion in Forum</span> 
                        <br /><br /> <span style="color:#000;">`+value.title+`</span> 
                        <div class="full-date"><small>`+value.date+`</small></div>
                        </a>
                      </div>
                    </div>
                    <div class="date text-right"><small>`+value.date+`</small></div>
                  </div>
                </div>
              `);

              $(".top-recent").append(`
                <div style="box-shadow:1px 1px 2px 1px #CCC;padding:1em;">
                  `+value.title+`
                </div>
                <br />
              `);
            });
          });
        </script>

        <!-- Recent Activities -->
        <div class="col-lg-4">
          <div class="recent-activities card">
            <div class="card-close">
              <div class="dropdown">
                <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
              </div>
            </div>
            <div class="card-header">
              <h3 class="h4" data-step="17" data-intro="show transactions activity logs">Recent Trading Activities</h3>
            </div>
            <div class="card-body no-padding">

              @if($all_transactions)
                @foreach($all_transactions as $transaction)
                <div class="item">
                  <div class="row">
                    <div class="col-4 date-holder text-right">
                      <div class="icon"><i class="icon-clock"></i></div>
                      <div class="date"> {{ $transaction->created_at->diffForHumans() }} </div>
                    </div>
                    <div class="col-8 content">
                      <h5>{{ $transaction->stock_name }}</h5>
                      <p>
                        
                        @if($transaction->stock_trade == "buy")
                          Purchase
                        @elseif($transaction->stock_trade == "sell")
                          Sold
                        @endif
                        {{ number_format($transaction->stock_qty) }} 
                        of  <b>{{ $transaction->stock_name }}</b>, at unit price of 
                        <b>{{ number_format($transaction->stock_unit, 2) }} </b>
                        for <b>&#8358;{{ number_format($transaction->stock_amount, 2) }}</b> <br />
                        <a href="/transactions">view</a>
                      </p>
                    </div>
                  </div>
                </div>
                @endforeach
              @else
                <div class="item">
                  <div class="row">
                    <div class="col-4 date-holder text-right">
                      <div class="icon"><i class="icon-clock"></i></div>
                      <div class="date"> --- </div>
                    </div>
                    <div class="col-8 content">
                      <h5>No Transactions made yet</h5>
                      <p>----</p>
                    </div>
                  </div>
                </div>
              @endif           
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <script type="text/javascript">
    $(document).ready(function(e) {
      // check if first time logging !
      var page   = 'dashboard';
      var userid = '{{ Auth::user()->id }}';

      let data = {
        page:page,
        userid:userid
      };

      $.get('/get/user/lesson', data, function(data) {
        console.log(data);
        if(data.status == 'unseen'){
          
        }
      });

      // body...
      introJs().start();
    });
   
  </script>
@endsection
