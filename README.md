# CATSS Application
This applications allows users to trade on real-time equities, the NSE financial market

# database Setup
Database uses microsoft sql server catss. database = catss

After running composer install 
After generating keys...

# installation
Then run artisan migration to setup database

# first step
go to the url param https://localhost:8000/
this will lunch the application

# second step
login as admin using the admin route param eg. https://127.0.0.1:8000/admin/login
login as user using the admin route param eg. https://127.0.0.1:8000/login

# security
admin has types and level for now the super admin here is "omega"
has right to create others and delete other admin

# API Section 
Navigate to the api.php, API documentation will be on a seprate URL