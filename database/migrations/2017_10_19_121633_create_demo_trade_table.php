<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDemoTradeTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('demotrades', function (Blueprint $table) {
			$table->increments('id');
			$table->string('equity');
			$table->decimal('start_price', 13, 4);
			$table->decimal('close_price', 13, 4);
			$table->string('status');
			$table->string('traffic');
			$table->float('stock_qty');
			$table->string('start_time');
			$table->string('close_time');
			$table->string('timing');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('demotrades');
	}
}
