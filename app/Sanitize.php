<?php

namespace CATSS;

use Illuminate\Database\Eloquent\Model;

class Sanitize extends Model
{
    //
    // accept array and scan data
    public function scanInputs($data)
    {
    	$scan_box = [];
    	for ($i=0; $i < count($data); $i++) { 
    		# scan and remove malicious data
    		$slate = trim(strip_tags(stripslashes($data[$i])));
    		array_push($scan_box, $slate);
    	}

    	return $scan_box;
    }
}
