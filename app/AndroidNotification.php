<?php

namespace CATSS;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;

class AndroidNotification extends Model
{
    /*
    |--------------------------------
    | Push Notification to Android
    |--------------------------------
    |
    */
    public function fire($title, $body){
    	
    	$pushNotifications = new \Pusher\PushNotifications\PushNotifications(
	    	array(
			  "instanceId" => 'a231382e-216e-407c-aad3-54fc74168216',
			  "secretKey"  => '1191382DAC36550EC1C592D2382DFB1'
			)
	    );
	    
		$instance_id = 'a231382e-216e-407c-aad3-54fc74168216';
		$app_secret = '1191382DAC36550EC1C592D2382DFB1';

		$body = [
			"interests" => ["hello"],
			"fcm" => [
				"notification" => [
					"title" => $title,
					"body" 	=> $body
				]
			]
		];
		$body = json_encode($body);

		// return $publishResponse;
		$headers = [
			'Content-Type' 	=> 'application/x-www-form-urlencoded',
			'Content-Type' 	=> 'application/json',
			'Authorization' => 'Bearer '.$app_secret
		];

		$base_uri = 'https://'.$instance_id.'.pushnotifications.pusher.com/publish_api/v1/instances/'.$instance_id.'/publishes';
		$post_notification = new Client();
		$post_notification_response = $post_notification->request('POST', $base_uri, [
			'headers' => $headers,
			'body' => $body
		]);

		// set response body
		$response_data = $post_notification_response->getBody();

		// return response
		return response()->json($response_data);
    }
}
