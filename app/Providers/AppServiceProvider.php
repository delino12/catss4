<?php

namespace CATSS\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    
    public function boot()
    {
        define('APP_TOKEN',"6b971eac2f876685b4ff2d07ffeb545c41B756F2DCAC80BFD910D1BED0633974");
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
