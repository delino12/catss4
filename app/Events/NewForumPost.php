<?php

namespace CATSS\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class NewForumPost implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    protected $data;

    public function __construct($data)
    {
        // get data 
        $this->data = $data;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('new-forum-post');
    }

    public function broadcastWith()
    {
       return [
            'id'    => $this->data['id'],
            'name'  => $this->data['name'],
            'email' => $this->data['email'],
            'title' => $this->data['title'],
            'vote'  => $this->data['vote'],
            'date'  => $this->data['date']
       ];
    }
}
