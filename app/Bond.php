<?php

namespace CATSS;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use CATSS\Account;
use DB;

class Bond extends Model
{
	/*
	|--------------------------------
	| handle bonds when purchasing
	|--------------------------------
	|
	*/
	public function buyBond($user_id, $price, $qty, $refid, $asset, $maturity, $yield, $option){

		
		$this->user_id 	= $user_id;
		$this->ref_id   = $refid;
		$this->price 	= $price;
		$this->qty 		= $qty;
		$this->security = $asset;
		$this->yield 	= $yield;
		$this->maturity = $maturity;
		$this->amount 	= $price * $qty;
		$this->TransactionTypeID = 1;
		$this->tradeDate = Carbon::now(); 
		
		// save stock purchase
		if($this->save()){

			// debit account
			$this->debitAccount($user_id, $this->amount);

			$data = [
				'status' => 'success',
				'message'=> '<b>'.$asset.'</b> Trade was successful !'
			];
		}else{
			$data = [
				'status' => 'error',
				'message'=> '<b>'.$asset.'</b> Trade was not successful !'
			];
		}
		

		// return callback
		return $data;
	}


	/*
	|--------------------------------
	| handle bonds when selling
	|--------------------------------
	|
	*/
	public function sellBond($user_id, $price, $qty, $refid, $asset, $maturity, $yield, $option){

		// validate bond balance
		$qty_balance = $this->fetchBondsQtyBalance($user_id, $maturity);

		if($qty_balance >= $qty){
			// valid 
			$this->user_id 	= $user_id;
			$this->ref_id   = $refid;
			$this->price 	= $price;
			$this->qty 		= $qty;
			$this->security = $asset;
			$this->yield 	= $yield;
			$this->maturity = $maturity;
			$this->amount 	= $price * $qty;
			$this->TransactionTypeID = 2;
			$this->tradeDate = Carbon::now(); 
			
			// save stock transaction
			if($this->save()){

				// debit account
				$this->creditAccount($user_id, $this->amount);

				$data = [
					'status' => 'success',
					'message'=> '<b>'.$asset.'</b> Trade was successful !'
				];
			}else{
				$data = [
					'status' => 'error',
					'message'=> '<b>'.$asset.'</b> Trade was not successful !'
				];
			}
		}else{
			$data = [
				'status' 	=> 'error',
				'message'	=> '<b>'.$asset.'</b> Insufficient balance !',
				'balance' 	=> 'You have <i class="fa fa-database"></i> '.number_format($qty_balance)
			];
		}

		// return callback
		return $data;
	}

	/*
	|--------------------------------
	| handle load user bonds
	|--------------------------------
	|
	*/
	public function loadUserBond($user_id){
		$bonds = Bond::where('user_id', $user_id)->orderBy('id', 'desc')->get();
		if(count($bonds) > 0){
			$bond_box = [];
			foreach ($bonds as $bond) {
				# code...
				$data = [
					'id'				=> $bond->id,
					'user_id'			=> $bond->user_id,
					'security'			=> $bond->security,
					'TransactionTypeID'	=> $bond->TransactionTypeID,
					'price'				=> number_format($bond->price, 2),
					'qty'				=> number_format($bond->qty),
					'yield'				=> $bond->yield,
					'amount'			=> $bond->amount,
					'maturity'			=> $bond->maturity,
					'tradeDate'			=> $bond->tradeDate
				];

				array_push($bond_box, $data);
			}
		}else{
			$bond_box = [];
		}

		return $bond_box;
	}


	/*
	|--------------------------------
	| handle check on user bonds
	|--------------------------------
	|
	*/
	public function getBondDetails($user_id, $ref_id){
		$bonds = Bond::where([['user_id', $user_id], ['']])->orderBy('id', 'desc')->get();
		if(count($bonds) > 0){
			$bond_box = [];
			foreach ($bonds as $bond) {
				# code...
				$data = [
					'id'				=> $bond->id,
					'user_id'			=> $bond->user_id,
					'security'			=> $bond->security,
					'TransactionTypeID'	=> $bond->TransactionTypeID,
					'price'				=> number_format($bond->price, 2),
					'qty'				=> number_format($bond->qty),
					'yield'				=> $bond->yield,
					'amount'			=> $bond->amount,
					'maturity'			=> $bond->maturity,
					'tradeDate'			=> $bond->tradeDate
				];

				array_push($bond_box, $data);
			}
		}else{
			$bond_box = [];
		}

		return $bond_box;
	}


	/*
	|--------------------------------
	| handle load all bonds
	|--------------------------------
	|
	*/
	public function loadAllBond(){
		$bonds = Bond::orderBy('id', 'desc')->get();
		if(count($bonds) > 0){
			$bond_box = [];
			foreach ($bonds as $bond) {
				# code...
				$data = [
					'id'				=> $bond->id,
					'user_id'			=> $bond->user_id,
					'security'			=> $bond->security,
					'TransactionTypeID'	=> $bond->TransactionTypeID,
					'price'				=> number_format($bond->price, 2),
					'qty'				=> number_format($bond->qty),
					'yield'				=> $bond->yield,
					'amount'			=> $bond->amount,
					'maturity'			=> $bond->maturity,
					'tradeDate'			=> $bond->tradeDate
				];

				array_push($bond_box, $data);
			}
		}else{
			$bond_box = [];
		}

		return $bond_box;
	}


	/*
    |--------------------------------------------------------------------------
    | SERVICES FOR HANDLING TRADES PURCHASE REQUEST
    |--------------------------------------------------------------------------
    |
    | Debit Cash account
    |
    */
    // debit account
    public function debitAccount($id, $amount){
        # code...
        // process trading and debit account balance 
        $account                    = Account::find($id);
        $account->account_balance   = $account->account_balance - $amount;
        $account->update();

        return true;
    }

    /*
    |--------------------------------------------------------------------------
    | SERVICES FOR HANDLING TRADES SALES REQUEST
    |--------------------------------------------------------------------------
    |
    | Credit Cash account
    |
    */
    // credit user bank account
    public function creditAccount($id, $amount)
    {

        // process trading and debit account balance 
        $account                    = Account::find($id);
        $account->account_balance   = $account->account_balance + $amount;
        $account->update();

        return true;
    }

    /*
    |--------------------------------
    | 	Handle bond balance validation
    |--------------------------------
    |
    */
    public function validateBondBalanceSell($user_id, $maturity){

    	$fetch_data = DB::select("EXEC procBondsQtyBalance $user_id, '$maturity' ");
    	$qty_balance = collect($fetch_data);
       	// debug
       	// dd($qty_balance);
        // return response()->json($qty_balance);

        return $qty_balance->qty;
    }

    /*
    |--------------------------------
    | Debit Tbills Stock balance
    |--------------------------------
    |
    */
    public function debitBondsAccount($user_id, $maturity, $qty){
    	$check_qty_balance = Bond::where([
    		['user_id', $user_id], 
    		['maturity', $maturity], 
    		['TransactionTypeID', "1"]
    	])->get();

    	if(count($check_qty_balance) > 0){
    		// qty balance 
    		$qty_box = [];
    		$balance = 0;
    		foreach ($check_qty_balance as $tbill) {
    			// case for multiple qty
    			// check account with large qty

				// find qty to fit balance
				$scan_tbills = Bond::find($tbill->id);
				
				if($scan_tbills->qty >= $qty){

					// update balance drawn
					$scan_tbills->qty = $scan_tbills->qty - $qty;
					$scan_tbills->update();

					// break out work done
					break;

				}elseif($scan_tbills->qty < $qty && $scan_tbills->qty !== 0){
					// get balance differences
					$scanned_bal = $qty - $scan_tbills->qty;

					$balance = $scanned_bal;

					// update balance drawn
					$scan_tbills->qty = $scan_tbills->qty - $scanned_bal;
					$scan_tbills->update();
				}
    		}
    	}
    }


    /*
    |--------------------------------
    | 	Handle bond balance validation
    |--------------------------------
    |
    */
    public function fetchBondsQtyBalance($user_id, $maturity){

    	$check_qty_balance = DB::select(" EXEC procBondsQtyBalance $user_id, '$maturity' ");
    	if(count($check_qty_balance) > 0){
			$qty_data = collect($check_qty_balance);
			$qty = $qty_data[0]->qty;
			return $qty;
    	}else{
    		$qty = 0;
    		return $qty;
    	}
    }

    /*
    |--------------------------------
    | Fetch user summary for Bonds
    |--------------------------------
    |
    */
    public function fetchBondSummary($user_id){

    	$fetch_summary = DB::select(" EXEC procBondsUserSummary $user_id ");
    	return $fetch_summary;
    }
}
