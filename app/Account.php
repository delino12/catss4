<?php

namespace CATSS;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    // get the user that own this account
    public function users()
    {
        return $this->belongsTo(User::class);
    }

    /*
    |-----------------------------------------
    | lock account
    |-----------------------------------------
    */
    public function lock($user_id){
    	// body
    	$account = Account::where("user_id", $user_id)->first();
    	$block_account 					= Account::find($account->id);
    	$block_account->account_status 	= "lock";
    	if($block_account->update()){
    		$data = [
    			'status'	=> 'error',
    			'message' 	=> 'This account has been blocked!'
    		];
    	}else{
    		$data = [
    			'status'	=> 'error',
    			'message' 	=> 'Could not block user account, kindly verify user account is valid'
    		];
    	}

    	// return
    	return $data;
    }

    /*
    |-----------------------------------------
    | unlock account
    |-----------------------------------------
    */
    public function unlock($user_id){
    	// body
    	$account = Account::where("user_id", $user_id)->first();
    	$block_account 					= Account::find($account->id);
    	$block_account->account_status 	= "open";
    	if($block_account->update()){
    		$data = [
    			'status'	=> 'success',
    			'message' 	=> 'This account has been unlocked!'
    		];
    	}else{
    		$data = [
    			'status'	=> 'error',
    			'message' 	=> 'Could not unblock user account, kindly verify user account is valid'
    		];
    	}

    	// return
    	return $data;
    }
}
