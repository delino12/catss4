<?php

namespace CATSS;

// use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;

class FxTrade
{
	protected $api_key;
	protected $base_url;

	// init constructor
    public function __construct(){
    	$this->api_key 	= env("FX_KEY");
    	$base_url 		= "http://data.fixer.io/api/"; 
    }


    /*
    |-----------------------------------------
    | Fetch all Symbols
    |-----------------------------------------
    */
    public function loadAllFxSymbols(){

    	// body
    	$endpoint 		= 'http://data.fixer.io/api/symbols?access_key='.$this->api_key;
    	$client 		= new Client();
    	$fx_response 	= $client->request('GET', $endpoint);

    	// filter response
        $res_code = $fx_response->getStatusCode();
        $res_body = $fx_response->getBody()->getContents();

        // return body contents
        return json_decode($res_body);
    }

    /*
    |-----------------------------------------
    | Fetch all Symbols
    |-----------------------------------------
    */
    public function loadSingleFxApi(){

    	// body
    	$endpoint = 'http://data.fixer.io/api/latest?access_key='.$this->api_key.'&base=EUR&symbols=GBP,JPY,USD,NGN,BTC,ZAR,AUD,ARS,CAD';
    	$client 		= new Client();
    	$fx_response 	= $client->request('GET', $endpoint);

    	// filter response
        $res_code = $fx_response->getStatusCode();
        $res_body = $fx_response->getBody()->getContents();

        // return body contents
        return json_decode($res_body);
    }

    /*
    |-----------------------------------------
    | Load All Fx using base
    |-----------------------------------------
    */
    public function loadFxTradeFull(){
        // body
        $endpoint       = 'http://data.fixer.io/api/latest?access_key='.$this->api_key;
        $client         = new Client();
        $fx_response    = $client->request('GET', $endpoint);

        // filter response
        $res_code = $fx_response->getStatusCode();
        $res_body = $fx_response->getBody()->getContents();

        // return body contents
        return json_decode($res_body);
    }
}
