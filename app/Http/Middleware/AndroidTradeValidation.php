<?php

namespace CATSS\Http\Middleware;

use Closure;

class AndroidTradeValidation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $this->verifyInputs($request);

        if(!$request->has('userid')){

            $data = [
                'status'    => 'error',
                'message'   => 'userid is missing and must not be empty!'
            ];
            
            return response()->json($data);
        }

        if(!$request->has('secid')){

            $data = [
                'status'    => 'error',
                'message'   => 'security id is missing and must not be empty!'
            ];
            
            return response()->json($data);
        }

        if(!$request->has('total')){

            $data = [
                'status'    => 'error',
                'message'   => 'Trade quantity is missing and must not be empty!'
            ];
            
            return response()->json($data);
        }
        return $next($request);
    }


    /*
    |--------------------------------
    | VERIFY TRADE INPUTS  
    |--------------------------------
    |
    */
    public function verifyInputs($request){


    }
}
