<?php

namespace CATSS\Http\Controllers;

use Illuminate\Http\Request;
use CATSS\News;

class NewsRoomController extends Controller
{
    // load news 
    public function loadApiNews(News $news)
    {
    	# code...
    	$load_news = new News();
    	$news_contents = $load_news->loadNews();

    	// filtered data
    	$data = json_decode($news_contents);

    	// return news
    	return response()->json($data);
    }

    // load bloomberg news 
    public function loadApiNewsBloomberg()
    {
    	# code...
    	$load_news = new News();
    	$news_contents = $load_news->loadNewsBloomberg();

    	// filtered data
    	$data = json_decode($news_contents);

    	// return news
    	return response()->json($data);
    }
}
