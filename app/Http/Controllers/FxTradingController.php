<?php

namespace CATSS\Http\Controllers;

use Illuminate\Http\Request;
use CATSS\FxTrade;

class FxTradingController extends Controller
{
    /*
    |-----------------------------------------
    | load all fx symbols
    |-----------------------------------------
    */
    public function loadAllFxSymbols(){
    	// body
    	$fx_trade 	= new FxTrade();
    	$data 		= $fx_trade->loadAllFxSymbols();

    	// return response
    	return response()->json($data);
    }



    /*
    |-----------------------------------------
    | load all fx symbols
    |-----------------------------------------
    */
    public function loadSingleFxSymbols(){
    	// body
    	$fx_trade 	= new FxTrade();
    	$data 		= $fx_trade->loadSingleFxApi();

    	// return response
    	return response()->json($data);
    }

    /*
    |-----------------------------------------
    | load all fx trading 
    |-----------------------------------------
    */
    public function loadFxFull(Request $request){
        // body
        $fx_trade   = new FxTrade();
        $data       =  $fx_trade->loadFxTradeFull();

        // return response 
        return response()->json($data);
    }
}
