<?php

namespace CATSS\Http\Controllers\Droid;

use Illuminate\Http\Request;
use CATSS\Http\Controllers\Controller;
use CATSS\Http\Controllers\Droid\Functions\BasicFunc;
use CATSS\User;
use CATSS\Basic;

class LoginController extends Controller {

    private $func;
    
    public function __construct() {
        $this->middleware('guest');
        $this->func = new BasicFunc();
    }

    public function login(Request $request) {

         if ($request->header("User-Agent") == APP_TOKEN) {
            $response = array();
            $data = json_decode($request->getContent(), true);
            $username = $data['email'];
            $password = $this->func->decodePassword($data['password']);


            $user = User::where('email', $username)->first();

            if ($user !== null && password_verify($password, $user['password'])) {
                                
                 $basic = Basic::where('user_id', $user->id)->first();#
                 $response["status"] = "00";
                $response["id"] = $user['id'];
                $response["name"] = $user['name'];
                $response["acc_id"] = $user['account_id']; 
                $response["avatar"] = $basic->avatar; 
                 
            }
            else{
            $response["status"] = "96";
            $response["message"] = 'Fail to login, please check your login credentials';
            }
            return json_encode($response);
        }
    }


}
