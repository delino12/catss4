<?php

namespace CATSS\Http\Controllers\Droid;

use Illuminate\Http\Request;
use CATSS\Http\Controllers\Controller;
use CATSS\Events\AccountUpdate;
use CATSS\Events\TransactionNotifications;
use CATSS\Events\StockBalanceNotifications;
use CATSS\Http\Controllers\Droid\Functions\BasicFunc;
use CATSS\User;
use CATSS\Group;
use CATSS\Account;
use CATSS\Transaction;
use CATSS\Statement;
use CATSS\Stock;
use CATSS\Asset;
use CATSS\Basic;
use CATSS\Security;
use CATSS\News;
use DB;

class GenController extends Controller {

      private $func;
    
    public function __construct() {
        $this->middleware('guest');
        $this->func = new BasicFunc();
    }
    
     public function loadAcceptedGroups(Request $request)
    { 
       if ($request->header("User-Agent") == APP_TOKEN) {
            $response = array();
            $data = json_decode($request->getContent(), true);
            $username = $data['email'];        
        // grouped user
        $groups = Group::all();

        // return $groups;
        $group_box = [];
        // scan group
        foreach ($groups as $group) {
            # scan members inside the group
            $group_check = explode(',', $group->members);
            
            for ($i=0; $i < count($group_check); $i++) { 
                # code...
                $group_check[$i] = str_replace(' ', '', $group_check[$i]);
                
                if($group_check[$i] == $username){
                    array_push($group_box, $group);
                }
            }
        } 

        return json_encode($group_box );
    }
    }
    
    
     public function loadAllGroups(Request $request)
    { 
       if ($request->header("User-Agent") == APP_TOKEN) {
            $response = array();
            $data = json_decode($request->getContent(), true);
            $username = $data['email'];        
        // grouped user
        $groups = Group::all();
        return json_encode($groups);
    }
    }
//get news
     public function getNews(Request $request)
    {
         if ($request->header("User-Agent") == APP_TOKEN) {
                     
        // fetch all recent news
        $news = News::orderBy('id','desc')->take('10')->get();
        
        return  json_encode($news);
    }
    }
    
    // get stock news
    public function getStockNews(Request $request) {

        if ($request->header("User-Agent") == APP_TOKEN) {
                     
            $pairs = Security::orderBy('id', 'desc')->get();
            $securities = array();
            foreach ($pairs as $pair) {
# code...
                $data = array(
                    "id" => $pair->id,
                    "pairs" => $pair->security,
                    "open" => number_format($pair->close_price, 2),
                    "close" => number_format($pair->previous_close, 2),
                );
                array_push($securities, $data);
            }
          return json_encode($securities);
        }
    }
// search account information
    public function getExperts(Request $request) {

        if ($request->header("User-Agent") == APP_TOKEN) {
// first get all users 
            $all_users = User::all();
            $rank_box = [];
            foreach ($all_users as $user) {
// scanned users stock by user_id 
                $amount = DB::table('stocks')->where('user_id', $user->id)->sum('amount');

                $data = array(
                    'name' => $user->name,
                    'profit' => number_format($amount, 2),
                    'date' => date("d' D M Y")
                );

                array_push($rank_box, $data);
            }
            return json_encode($rank_box);
        }
    }
    public function getNewsAndExperts(Request $request) {

        if ($request->header("User-Agent") == APP_TOKEN) {
            $response = array();
            $response["experts"] = getExperts($request);
            $response["stock_news"] = getNews($request);

            return json_encode($response);
        }
    }

#

    public function equityPairs(Request $request) {

// load pairs
        if ($request->header("User-Agent") == APP_TOKEN) {
            $response = array();
            $pairs = Security::all();

            $pair_box = [];
            foreach ($pairs as $equity) {

                $two_percent = 2.00;
                $total_percent = 100;
                $diff = $total_percent - $two_percent;

                $charge_percent = $diff / $total_percent;
                $map_price = $charge_percent * $equity->close_price;
                $map_diff = $equity->close_price - $map_price;

                $real_price = $map_diff + $equity->close_price;

                $gap = $real_price - $equity->previous_close;


                $data = array(
                    'id' => $equity->id,
                    'board' => $equity->board,
                    'security' => $equity->security,
                    'ref_price' => number_format($equity->ref_price, 2),
                    'open_price' => number_format($equity->open_price, 2),
                    'high_price' => number_format($equity->high_price, 2),
                    'low_price' => number_format($equity->low_price, 2),
                    'close_price' => number_format($equity->close_price, 2),
                    'change_price' => number_format($gap, 2),
                    'daily_volume' => number_format($equity->daily_volume, 2),
                    'daily_value' => number_format($equity->daily_value, 2),
                    'dvmv_today' => $equity->dvmv_today,
                    'mvtn_trade' => $equity->mvtn_trade,
                    'previous_close' => number_format($equity->previous_close, 2),
                    'no_of_trade' => number_format($equity->no_of_trade),
                    'real_price' => number_format($real_price, 2),
                    'status' => $equity->status,
                    'date' => $equity->created_at->toFormattedDateString()
                );
                array_push($pair_box, $data);
            }

// return json response in array box
            return json_encode($pair_box);
        }
    }

    public function reqTrade(Request $request) {

        if ($request->header("User-Agent") == APP_TOKEN) {
            $response = array();

            $data = json_decode($request->getContent(), true);
            $pid = $data['email'];
            $pair_name = $data['name'];
            $trade_qty = $data['qtty'];
            $trade_type = $data['type'];
            $trade_price = $data['trade_price'];
            $sell_price = $data['sell_price'];

            $username = $data['email'];
            $password = $this->func->decodePassword($data['password']);


            $user = User::where('email', $username)->first();

            if ($user !== null && password_verify($password, $user['password'])) {
                $id = $user->id;

// do stock calculations
                $amount = $trade_qty * $trade_price;
// proceed to buying stock
                if ($trade_type == "buy") {
// get current logged in user
// fetch account information 
                    $account_details = Account::where('user_id', $id)->first();


# check if balance is enough
                    if ($account_details->account_balance < $amount) {

                        $response["status"] = "96";
                        $response["message"] = 'Insuffient Funds to purchase ' . $pair_name;
                        return json_encode($response);
                    } else {
// update user account balance
                        $update_account = Account::find($id);
                        $update_account->account_balance = $update_account->account_balance - $amount;
                        $update_account->update();

// create transaction information
                        $transaction = new Transaction();
                        $transaction->user_id = $id;
                        $transaction->stock_name = $pair_name;
                        $transaction->stock_unit = $trade_price;
                        $transaction->stock_qty = $trade_qty;
                        $transaction->stock_trade = $trade_type;
                        $transaction->stock_amount = $amount;
                        $transaction->stock_timing = time();
                        $transaction->save();

// first check if same stock already exits, if same stock exits update existing...
                        $stocks = Stock::where([['name', $pair_name], ['user_id', $id]])->first();


// if pair is new
                        if ($stocks == null) {

// no stock exits so W/A is new
                            $f1 = $trade_qty * $trade_price;
                            $fresh_wa = $f1 / $trade_qty;

// else save new Stock
                            $stocks = new Stock();
                            $stocks->user_id = $id;
                            $stocks->name = $pair_name;
                            $stocks->qty = $trade_qty;
                            $stocks->price = $trade_price;
                            $stocks->amount = $trade_qty * $trade_price;
                            $stocks->w_average = $fresh_wa;
                            $stocks->save();
                        } else {
// now check if stock exist
                            $existed_stock = Stock::find($stocks->id);

// get the W/A Formular f1 + f2 / fQty
// capture incoming stock qty*price 
                            $f2 = $trade_qty * $trade_price;

// get 
                            $f1 = $existed_stock->qty * $existed_stock->w_average;

# w/average
                            $f3 = $f1 + $f2; // compute net old qty * old price
                            $f4 = $existed_stock->qty + $trade_qty; // compute net qty
                            $f5 = $f3 / $f4;

                            $existed_stock->price = $trade_price;
                            $existed_stock->w_average = $f5;
                            $existed_stock->qty = $f4;
                            $existed_stock->amount = $f3;
                            $existed_stock->update();
                        }

// Get Account Balances
                        $account_balance = Account::where('user_id', $id)->first();
                        $act_bal = $account_balance->account_balance;
// Fire Account Balance

                        \Event::fire(new AccountUpdate($act_bal));

// find transactions 
                        $all_stocks = Stock::where('user_id', $id)->orderBy('id', 'desc')->get();
                        $total_stock = count($all_stocks);
                        $stock_box = [];
                        foreach ($all_stocks as $stocks) {
                            if ($stocks->qty > 0) {
# code...
                                $data = array(
                                    'id' => $stocks->id,
                                    'name' => $stocks->name,
                                    'qty' => number_format($stocks->qty),
                                    'price' => number_format($stocks->price, 2),
                                    'total_qty' => $total_stock
                                );
                                array_push($stock_box, $data);
                            }
                        }

// Fire Stock balance info
                        \Event::fire(new StockBalanceNotifications($stock_box));

// Get transactions 
                        $transaction = Transaction::where('user_id', $id)->orderBy('id', 'desc')->take('1')->first();
                        $trans_box = array(
                            'stock_name' => $transaction->stock_name,
                            'stock_unit' => number_format($transaction->stock_unit, 2),
                            'stock_qty' => number_format($transaction->stock_qty),
                            'stock_trade' => $transaction->stock_trade,
                            'stock_amount' => number_format($transaction->stock_amount, 2),
                            'stock_date' => $transaction->created_at->diffForHumans()
                        );

// Fire Transactions info
                        \Event::fire(new TransactionNotifications($trans_box));


                        /*
                         * * After all check is done
                         * * Return message to trade screen
                         * * Successful Trade
                         * *
                         */

// Send user a message !
                        $response["status"] = "00";
                        $response["message"] = $pair_name . ' Trade Successful ! ';
                        return json_encode($response);
                    }
                } elseif ($trade_type == "sell") {


// check if user have the stock..
                    $stocks = Stock::where([['name', $pair_name], ['user_id', $id]])->get();

// check if user has the stock
                    $total = count($stocks);
// check if stock is less than request qty
                    if ($total > 0) {

// get the temp stock id
                        foreach ($stocks as $tem_stock) {
# catch the current stock id
                            $tem_id = $tem_stock->id;
                            $tem_qty = $tem_stock->qty;

// check if old qty is less than requested trade qty
                            if ($tem_qty < $trade_qty) {

                                $response["status"] = "96";
                                $response["message"] = $pair_name . 'Trade Unsuccessful due to low stock balance. please check ' . $pair_name . ' balance ';
                                return json_encode($response);
                            } else {

// sell amount 
                                $sell_amount = $sell_price * $trade_qty;

// update transactions logs
                                $transaction = new Transaction();
                                $transaction->user_id = $id;
                                $transaction->stock_name = $pair_name;
                                $transaction->stock_unit = $sell_price;
                                $transaction->stock_qty = $trade_qty;
                                $transaction->stock_trade = $trade_type;
                                $transaction->stock_amount = $sell_amount;
                                $transaction->stock_timing = time();
                                $transaction->save();

// update user account balance
                                $update_account = Account::find($id);
                                $update_account->account_balance = $update_account->account_balance + $sell_amount;
                                $update_account->update();

// now update if stock exist
                                $stocks = Stock::find($tem_id);
                                $stocks->qty = $stocks->qty - $trade_qty; // keypiece when users sell-> qty update
                                $stocks->update();

// get stock w/a on statements 
                                $stocks = Stock::where([['name', $pair_name], ['user_id', $id]])->get();
                                foreach ($stocks as $stock) {
# code...
                                    $w_a = $stock->w_average; // w/a
// remove negative return...
                                    if ($w_a > $sell_price) {
                                        $gap = $w_a - $sell_price; // trade gap on w/a
                                    } elseif ($w_a < $sell_price) {
                                        $gap = $sell_price - $w_a; // trade gap on w/a
                                    } elseif ($w_a == $sell_price) {
                                        $gap = $w_a - $sell_price; // trade gap on w/a
                                    }

                                    $f1 = $w_a * $trade_qty; // on weighted average (holding value)
                                    $f2 = $sell_price * $trade_qty; // on demand (market value)


                                    if ($f1 > $f2) {
                                        $comments = " trade Successful with loss gap of " . $gap;
                                        $status = "loss";
                                    } elseif ($f2 > $f1) {
                                        $comments = " trade Successful with profit gap of " . $gap;
                                        $status = "profit";
                                    } elseif ($f2 == $f1) {
                                        $comments = " trade Successful with no profit/loss with return gap of " . $gap;
                                        $status = "break-even";
                                    }


                                    $top_bal = $gap * $trade_qty;

// get id and update state
                                    $account_statements = new Statement();
                                    $account_statements->equity = $pair_name;
                                    $account_statements->w_average = $w_a;
                                    $account_statements->user_id = $id;
                                    $account_statements->last_sold_price = $sell_price;
                                    $account_statements->last_sold_qty = $trade_qty;
                                    $account_statements->amount = $sell_amount;
                                    $account_statements->gap = $gap;
                                    $account_statements->balance = $top_bal;
                                    $account_statements->status = $status;
                                    $account_statements->comments = $comments;
                                    $account_statements->save();
                                }

// a little house cleaning..
                                $stocks = Stock::where([['user_id', $id], ['qty', 0]])->first();
                                if ($stocks !== null) {
                                    $refresh_stocks = Stock::find($stocks->id);
                                    $refresh_stocks->w_average = 0;
                                    $refresh_stocks->price = 0;
                                    $refresh_stocks->amount = 0;
                                    $refresh_stocks->update();
                                }

// proceed to trade
                                $response["status"] = "00";
                                $response["message"] = 'Sold ' . $pair_name . ' at ' . number_format($sell_price, 2) . ', Trade Successful ';
                                ;
                                return json_encode($response);
                            }
                        }
                    } else {

// response
                        $response["status"] = "96";
                        $response["message"] = $pair_name . ' Trade Unsuccessful, you do not have this stock for sale ';
                        return json_encode($response);
                    }
                }
            }
        }
    }

    public function getTrans(Request $request) {

        if ($request->header("User-Agent") == APP_TOKEN) {
            
            $data = json_decode($request->getContent(), true);
            $username = $data['email'];
            $password = $this->func->decodePassword($data['password']);


            $user = User::where('email', $username)->first();

            if ($user !== null && password_verify($password, $user['password'])) {
                $id = $user->id;

// find transactions 
                $all_transactions = Transaction::where('user_id', $id)->orderBy('id', 'desc')->take('20')->get();

                return json_encode($all_transactions);
            }
        }
    }

    public function updateProfile(Request $request) {
        if ($request->header("User-Agent") == APP_TOKEN) {
            $response = array();
            $data = json_decode($request->getContent(), true);
            $username = $data['email'];
            $password = $this->func->decodePassword($data['password']);

            $user = User::where('email', $username)->first();

            if ($user !== null && password_verify($password, $user['password'])) {
                    $id = $user->id;

                    $firstname = $data['first_name'];
                    $lastname = $data['last_name'];

// form request
                    $name = $firstname . ' ' . $lastname;
                    $gender = $data['gender'];
                    $address = $data['address'];
                    $state = $data['state'];
                    $zipcode = $data['zip_code'];
                    $phone = $data['phone'];

// get user stock balance 
// get basic id rep
                    $basic_id = Basic::where('user_id', $id)->first();
                    $b_id = $basic_id->id;

// find id and update
                    $basic = Basic::find($b_id);
                    $basic->name = $name;
                    $basic->gender = $gender;
                    $basic->address = $address;
                    $basic->state = $state;
                    $basic->zip_code = $zipcode;
                    $basic->phone = $phone;
                    $basic->update();

                    $msg = " Profile details updated successfully ";
                    $response["status"] = "00";
                    $response["message"] = $msg;
                    return json_encode($response);
                
            }
        }
    }

    public function savePersonalStock(Request $request) {

        if ($request->header("User-Agent") == APP_TOKEN) {
            $response = array();
            $data = json_decode($request->getContent(), true);
            $username = $data['email'];
            $password = $this->func->decodePassword($data['password']);
            $user = User::where('email', $username)->first();


            if ($user !== null && password_verify($password, $user['password'])) {
                $id = $user->id;

                $name = $data['equity'];
                $buy = $data['acq_price']; //acquired price
                $sell = $data['rel_price']; //released price
                $Qty = $data['qty'];


// check for user personal assets
                $check_assets = Asset::where([['name', $name], ['user_id', $id]])->first();

                if ($check_assets == null) {
// this is a new assets
                    $assets = new Asset();
                    $assets->user_id = $id;
                    $assets->name = $name;
                    $assets->buy = $buy;
                    $assets->sell = $sell;
                    $assets->Qty = $Qty;
                    $assets->save();

// return msg
                    $msg = $name . " added to your realtime stock record!";
                } else {
// update the existing assets with assets id
// temp save asset id if found
                    $assets_id = $check_assets->id;
                    $assets = Asset::find($assets_id);
                    $assets->user_id = $id;
                    $assets->name = $name;
                    $assets->buy = $buy;
                    $assets->sell = $sell;
                    $assets->Qty = $assets->qty + $Qty;
                    $assets->update();

                    $msg = $name . " added to your realtime stock record!";
                }

// fetch last updated and feed events
                $stocks = Asset::where([['name', $name], ['user_id', $id]])->first();

// to array
                $data = array(
                    'id' => $stocks->id,
                    'name' => $stocks->name,
                    'buy' => number_format($stocks->buy, 2),
                    'sell' => number_format($stocks->sell, 2),
                    'qty' => number_format($stocks->qty),
                    'date' => $stocks->updated_at->diffForHumans(),
                );

// call events
                // \Event::fire(new NewAssets($data));
// res msg
                $response["status"] = "00";
                $response["message"] = $msg;
                return json_encode($response);
            }
        }
    }

    // load personal stock
    public function loadPersonalStock(Request $request) {

        if ($request->header("User-Agent") == APP_TOKEN) {
            $realtime_stocks = array();
            $data = json_decode($request->getContent(), true);
            $username = $data['email'];
            $password = $this->func->decodePassword($data['password']);
            $user = User::where('email', $username)->first();


            if ($user !== null && password_verify($password, $user['password'])) {
                $id = $user->id;

                // check if that Security Exist
                $securites = Security::all();

                foreach ($securites as $security) {
                    # code...
                    // fetch stock
                    $all_realtime_assets = Asset::where('user_id', $id)->orderBy('id', 'desc')->get();

                    foreach ($all_realtime_assets as $stocks) {

                        if ($stocks->name == $security->security) {
                            // get security market price
                            $mkt_price = $security->close_price;

                            $x_buy = $stocks->buy * $stocks->qty;
                            $bal = $mkt_price * $stocks->qty;

                            if ($bal > $x_buy) {
                                $status = "Profit";
                            } elseif ($bal < $x_buy) {
                                $status = "Loss";
                            } elseif ($bal == $x_buy) {
                                $status = "Break-even point";
                            }
                        } else {
                            $status = 'Not avialable in the market.';
                            $bal = 000;
                        }

                        // calculate accrue assets
                        # code...
                        $data = array(
                            'id' => $stocks->id,
                            'name' => $stocks->name,
                            'buy' => number_format($stocks->buy, 2),
                            'sell' => number_format($stocks->sell, 2),
                            'bal' => number_format($bal, 2),
                            'status' => $status,
                            'qty' => number_format($stocks->qty),
                            'date' => $stocks->updated_at->diffForHumans(),
                        );

                        array_push($realtime_stocks, $data);
                    }



                    return json_encode($realtime_stocks);
                }
            }
        }
    }

    public function uploadAvatar(Request $request) {
        if ($request->header("User-Agent") == APP_TOKEN) {
            $response = array();
            $username = $request->header("email");

            $user = User::where('email', $username)->first();
            $userId = $user->id;

//upload and stored images
            $target_dir = public_path() . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR;
            $ext = strtolower(substr($_FILES['file']['name'], strpos($_FILES['file']['name'], '.')));
// accepted extension
            $accepted_ext = array(".jpg", ".jpeg", ".png");
            if (in_array($ext, $accepted_ext)) {
                $new_name = time() . rand(000, 999) . $ext;
                $target_file = $target_dir . $new_name;
                move_uploaded_file($_FILES['file']['tmp_name'], $target_file);

                $msg = " Profile details updated successfully ";

// save profile avatar
                $basic = Basic::where('user_id', $userId)->first();
                $basic->avatar = $new_name;
                $basic->update();

                $response["status"] = "00";
                $response["message"] = $msg;
                $response["avatar"] = $new_name;
            } else {
// return with error in extenstion
                $msg = "Invalid image type, please uploada a valid image";
                $response["status"] = "96";
                $response["message"] = $msg;
            }
            return json_encode($response);
        }
    }

    public function getBasicInfo(Request $request) {

        if ($request->header("User-Agent") == APP_TOKEN) {
            $data = json_decode($request->getContent(), true);
           
            $username = $data['email'];
            $password = $this->func->decodePassword($data['password']);

           
            $user = User::where('email', $username)->first();
              
            if ($user !== null && password_verify($password, $user['password'])) {
                $id = $user->id;
// find basic information
                $basic_information = Basic::where('user_id', $id)->first();
                
                return json_encode($basic_information);
            }
        }
    }

    public function recentTrans(Request $request) {

        if ($request->header("User-Agent") == APP_TOKEN) {
            $response = array();
            $data = json_decode($request->getContent(), true);
            $username = $data['email'];
            $password = $this->func->decodePassword($data['password']);
            $user = User::where('email', $username)->first();

            if ($user !== null && password_verify($password, $user['password'])) {
                $id = $user->id;

// find transactions 
                $all_transactions = Transaction::where('user_id', $id)->orderBy('id', 'desc')->take('5')->get();

                return json_encode($all_transactions);
            }
        }
    }

    public function stocksBalance(Request $request) {

        if ($request->header("User-Agent") == APP_TOKEN) {
// get user stock balance 
            $response = array();
            $data = json_decode($request->getContent(), true);
            $username = $data['email'];
            $password = $this->func->decodePassword($data['password']);


            $user = User::where('email', $username)->first();

            if ($user !== null && password_verify($password, $user['password'])) {
                $id = $user->id;

// fetch all stocks
                $stocks = Stock::where('user_id', $id)->orderBy('id', 'desc')->get();
                $stock_basket = [];
                foreach ($stocks as $stock) {
# code...
// get last price from pairs
                    $equity_market = Security::where('security', $stock->name)->get();

                    foreach ($equity_market as $market) {

// check on formular
// $wa = (float)$stock->qty * $stock->w_average;

                        if ($stock->w_average > $market->close_price) {
                            $wa = $stock->w_average - $market->close_price;
                        } elseif ($stock->w_average < $market->close_price) {
                            $wa = $market->close_price - $stock->w_average;
                        } elseif ($stock->w_average = $market->close_price) {
                            $wa = $market->close_price - $stock->w_average;
                        }

                        $swa = $wa * $stock->qty;

// market price + charges
                        $two_percent = 2.00;
                        $total_percent = 100;
                        $diff = $total_percent - $two_percent;

                        $charge_percent = $diff / $total_percent;
                        $map_price = $charge_percent * $market->close_price;
                        $map_diff = $market->close_price - $map_price;

                        $real_price = $map_diff + $market->close_price;

# code...
                        $data = array(
                            "equity" => $stock->name,
                            "price" => $real_price,
                            "stock" => $stock->qty,
                            "w_average" => $stock->w_average,
                            "amount" => $stock->qty * $market->close_price,
                            "swa" => $swa,
                            "date" => $market->updated_at
                        );
                        array_push($stock_basket, $data);
                    }
                }


// fetch account information 
                $account_info = Account::where('user_id', $id)->get();

// fetch group trade
                $groups = Group::where('user_id', $id)->get();

// show users stocks bal.
                $response = array();
                $response['stocks'] = $stocks;
                $response['stock_basket'] = $stock_basket;
                return json_encode($response);
//  return view('internal-pages.stock-balance', compact('stocks', 'account_info', 'groups', 'stock_basket'));
            }
        }
    }

//Yet to be used meth
    public function tradeHistory(Request $request) {
        if ($request->header("User-Agent") == APP_TOKEN) {
            $response = array();
            $data = json_decode($request->getContent(), true);
            $username = $data['email'];
            $password = $this->func->decodePassword($data['password']);
            if ($user !== null && password_verify($password, $user['password'])) {
                $id = $user->id;
                $pair = $data['equity'];
                $transaction_history = Transaction::where('stock_name', $equity)->orderBy('id', 'desc')->get();

                $transaction_history['wa'] = loadStockBal($id)[0]['w_average'];

                return json_encode($transaction_history);
            }
        }
    }

    public function loadStockBal($id) {
// get user stock balance 
// fetch all stocks
        $stocks = Stock::where('user_id', $id)->orderBy('id', 'desc')->get();
        $stock_basket = [];
        foreach ($stocks as $stock) {
# code...
// get last price from pairs
            $equity_market = Security::where('security', $stock->name)->get();

            foreach ($equity_market as $market) {

// get all the W/A

                if ($stock->w_average > $market->close_price) {
                    $wa = $stock->w_average - $market->close_price;
                } elseif ($stock->w_average < $market->close_price) {
                    $wa = $market->close_price - $stock->w_average;
                } elseif ($stock->w_average = $market->close_price) {
                    $wa = $market->close_price - $stock->w_average;
                }

                $swa = $wa * $stock->qty;

# code...
                $data = array(
                    "equity" => $stock->name,
                    "price" => (float) $market->close_price,
                    "stock" => (float) $stock->qty,
                    "w_average" => number_format($stock->w_average, 2),
                    "amount" => $stock->qty * $market->close_price,
                    "swa" => $swa,
                    "date" => $market->updated_at->diffForHumans()
                );
                array_push($stock_basket, $data);
            }
        }

        return json_encode($stock_basket);
    }

    public function getAccountStatement(Request $request) {
        if ($request->header("User-Agent") == APP_TOKEN) {
            $data = json_decode($request->getContent(), true);
            $username = $data['email'];
            $password = $this->func->decodePassword($data['password']);


            $user = User::where('email', $username)->first();

            if ($user !== null && password_verify($password, $user['password'])) {
                $id = $user->id;
// find transactions 
// accounts
                $trading_statements = Statement::where('user_id', $id)->get();

                return json_encode($trading_statements);
            }
        }
    }

    public function getAccountDetail(Request $request) {
        if ($request->header("User-Agent") == APP_TOKEN) {

            $data = json_decode($request->getContent(), true);
            $username = $data['email'];
            $password = $this->func->decodePassword($data['password']);

            $user = User::where('email', $username)->first();

            if ($user !== null && password_verify($password, $user['password'])) {
                $id = $user->id;
// find transactions 
// accounts
                $account_info = Account::where('user_id', $id)->get();
                $account_info[0]['name'] = $user->name;

                return json_encode($account_info);
            }
        }
    }

    public function changePassword(Request $request) {
        if ($request->header("User-Agent") == APP_TOKEN) {
            $response = array();
            $data = json_decode($request->getContent(), true);
            $username = $data['email'];
            $password = $this->func->decodePassword($data['password']);
            $newPassword = $this->func->decodePassword($data['new_password']);

            $user = User::where('email', $username)->first();

            if ($user !== null && password_verify($password, $user['password'])) {
                $newPassword = bcrypt($newPassword);

                $user->password = $newPassword;
                $user->update();

                $msg = "Password changed successfully";
                $response["status"] = "00";
                $response["message"] = $msg;
                return json_encode($response);
            } else {
                $msg = "Failed: wrong password inputed";
                $response["status"] = "96";
                $response["message"] = $msg;
                return json_encode($response);
            }
        }
    }

}
