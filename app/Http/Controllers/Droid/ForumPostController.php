<?php

namespace CATSS\Http\Controllers\Droid;

use Illuminate\Http\Request;
use CATSS\Http\Controllers\Controller;
use CATSS\Http\Controllers\Droid\Functions\BasicFunc;
use CATSS\Events\NewForumPost;
use CATSS\Events\NewComment;
use CATSS\Comment;
use CATSS\User;
use CATSS\Forum;
use DB;

class ForumPostController extends Controller {

    private $func;

    public function __construct() {
        $this->func = new BasicFunc();
    }

    // create a new forum post
    public function createPost(Request $request) {
        if ($request->header("User-Agent") == APP_TOKEN) {
            $data = json_decode($request->getContent(), true);

            $email = $data['email'];
            $post = $data['post'];

            $user = User::where('email', $email)->first();
            $name = $user->name;
            // check if type post already exits
            $post_exits = DB::table('forums')->where([['name', $name], ['email', $email], ['title', $post]])->first();

            if ($post_exits == null) {
                // create new post
                $post = DB::table('forums')->insert([
                    'name' => $name,
                    'email' => $email,
                    'title' => $post,
                    'vote' => 0,
                    'created_at' => NOW(),
                    'updated_at' => NOW()
                ]);
                if (!$post) {
                    $data = array(
                        'status' => 'error',
                        'message' => 'Fail to send post !'
                    );
                    return json_encode($data);
                } else {
                    // fetch last post
                    $last_post = DB::table('forums')->orderBy('id', 'desc')->first();

                    $data = array(
                        'id' => $last_post->id,
                        'name' => $last_post->name,
                        'email' => $last_post->email,
                        'title' => $last_post->title,
                        'vote' => $last_post->vote,
                        'date' => date('M D h:i a', strtotime($last_post->created_at))
                    );

                    \Event::fire(new NewForumPost($data));

                    $data = array(
                        'status' => 'success',
                        'message' => '1 new topic created !'
                    );
                    return json_encode($data);
                }
            } else {
                $data = array(
                    'status' => 'error',
                    'message' => 'This post already exits !'
                );
                return json_encode($data);
            }
        }
    }

    // comments 
    // load all forum topics
    public function loadPosts(Request $request) {

        if ($request->header("User-Agent") == APP_TOKEN) {
            $data = json_decode($request->getContent(), true);

            $email = $data['email'];

            $user = User::where('email', $email)->first();
            $userId = $user->id;
            // load all 
            $all_posts = DB::table('forums')->orderBy('id', 'desc')->get();

            // push json
            $post_box = [];
            foreach ($all_posts as $posts) {
                # code...
                // get no of comments
                $comments = Comment::where('post_id', $posts->id)->get();
              
                $no_of_com = count($comments);
                  
                if ($no_of_com <= 0) {
                    $no_of_com = "";
                }
                
                $no_of_votes = $posts->vote;
                if ($no_of_votes <= 0) {
                    $no_of_votes = "";
                }
                $user_voted = 0;
                $formatedID = ":" . $userId . ":";
                if (strpos($posts->voters_id, $formatedID) !== false) {
                    $user_voted = 1;
                }



                $data = array(
                    'id' => $posts->id,
                    'name' => $posts->name,
                    'email' => $posts->email,
                    'title' => $posts->title,
                    'vote' => $no_of_votes,
                    'no_comment' => $no_of_com,
                    'user_voted' => $user_voted,
                    'date' => $posts->created_at,
                );

                array_push($post_box, $data);
            }

            // return post box response
            return json_encode($post_box);
        }
    }

    public function votePost(Request $request) {
        if ($request->header("User-Agent") == APP_TOKEN) {
            $data = json_decode($request->getContent(), true);
            $response = array();

            $email = $data['email'];
            $password = $this->func->decodePassword($data['password']);
            $postID = $data['post_id'];

            $user = User::where('email', $email)->first();

            $response["status"] = "96";
            if ($user !== null && password_verify($password, $user['password'])) {
                $userId = $user->id;
                $post = Forum::find($postID);
                $post->vote = 1 + $post->vote;
                $post->voters_id = $post->voters_id . ':' . $userId . ':,';
                $post->update();
                $response["status"] = "00";
                $response["message"] = 'Successful';
            }
            return json_encode($response);
        }
    }

    // load title posts
    public function loadCard($id) {
        // fetch title post
        $cards = DB::table('forums')->where('id', $id)->first();
        $data = array(
            'id' => $cards->id,
            'name' => $cards->name,
            'email' => $cards->email,
            'title' => $cards->title,
            'vote' => $cards->vote,
            'date' => date('M D h:i a', strtotime($cards->created_at))
        );

        return json_encode($data);
    }

    // post users comments 
    public function postComment(Request $request) {
        if ($request->header("User-Agent") == APP_TOKEN) {
            $data = json_decode($request->getContent(), true);


            $email = $data['email'];
            $comment = $data['comment'];
            $id = $data['post_id'];
            $user = User::where('email', $email)->first();
            // check if type comments already exits
            $com_exits = Comment::where([['post_id', $id], ['user_id', $user->id], ['body', $comment]])->first();

            if ($com_exits == null) {
                // create new comment
                $com_new = new Comment();
                $com_new->post_id = $id;
                $com_new->user_id = $user->id;
                $com_new->body = $comment;
                $com_new->save();
            }
              return $this->getComments($id);
        }
    }

    // load comments for posts 
    public function loadComments(Request $request) {

        if ($request->header("User-Agent") == APP_TOKEN) {
            $data = json_decode($request->getContent(), true);
            $email = $data['email'];
            $postID = $data['post_id'];

            $user = User::where('email', $email)->first();
            // load all comments to posts
           
            return $this->getComments($postID);
        }
    }

    private function getComments($postID){
         $all_comments = Comment::where('post_id', $postID)->get();

            $com_box = [];
            foreach ($all_comments as $comment) {
                # code...
                $from = User::find($comment->user_id);
                $data = array(
                    'id' => $comment->id,
                    'name' => $from->name,
                    'email' => $from->email,
                    'body' => $comment->body,
                    'date' => $comment->created_at->diffForHumans()
                );

                array_push($com_box, $data);
            }
            return json_encode($com_box);
    }
}
