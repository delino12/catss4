<?php

namespace CATSS\Http\Controllers\Droid;

use Illuminate\Http\Request;
use CATSS\Http\Controllers\Controller;
use CATSS\Http\Controllers\Droid\ForumPostController;
use CATSS\Http\Controllers\Droid\GenController;

class DataFetchController extends Controller {

#
     public function fetchData(Request $request) {
         $data = json_decode($request->getContent(), true);
         $token = $data['token'];
        if ($token == APP_TOKEN) {
          $response = array();
          $genCon = new GenController();
          $forumCon = new ForumPostController();
          $response["stock_news"] = $genCon->getStockNews($request);
          $response["news"] = $genCon->getNews($request);
          $response["experts"] = $genCon->getExperts($request);
          $response["equity_pairs"] = $genCon->equityPairs($request);
          $response["trans"] = $genCon->getTrans($request);
          $response["realtime-stock"] = $genCon->loadPersonalStock($request);
          $response["basic_info"] = $genCon->getBasicInfo($request);
          $response["recent_trans"] = $genCon->recentTrans($request);
          $response["stock_bal"] = $genCon->stocksBalance($request);
        //  $response["trade_history"] = $genCon->tradeHistory($request);
          $response["account_statement"] = $genCon->getAccountStatement($request);
          $response["account_info"] = $genCon->getAccountDetail($request);
          $response["daily_feeds"] = $forumCon->loadPosts($request);
          
          return json_encode($response);
     }
     }
}
