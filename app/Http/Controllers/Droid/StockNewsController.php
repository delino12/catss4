<?php

namespace CATSS\Http\Controllers\Droid;

use Illuminate\Http\Request;
use CATSS\Http\Controllers\Controller;
use CATSS\Security;


class StockNewsController extends Controller
{
    public function __construct() {
        $this->middleware('guest');
    }

     public function stockNews(Request $request) {
         
      if ($request->header("User-Agent") == APP_TOKEN) {
    	// load pairs
        $pairs = Security::orderBy('id', 'desc')->get();

        $response = array();
        foreach ($pairs as $pair) {
            # code...
            $data = array(
                "id" => $pair->id,
                "pairs" => $pair->security,
                "open" => number_format($pair->close_price, 2),
                "close" => number_format($pair->previous_close, 2)
            );
            array_push($response, $data);
        }

        // return json response in array box
        return json_encode($response) ;
    }  
   
  }
    
}
