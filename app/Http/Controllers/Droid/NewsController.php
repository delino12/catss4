<?php

namespace CATSS\Http\Controllers\Droid;

use Illuminate\Http\Request;
use CATSS\Http\Controllers\Controller;
use CATSS\News;
use CATSS\Security;

class NewsController extends Controller
{
    

    // get news
    public function getNews(Request $request){
    	 
         if ($request->header("User-Agent") == APP_TOKEN) {
            $response = array();
             $news = News::orderBy('id','desc')->take('10')->get();
             
             $pairs = Security::orderBy('id', 'desc')->get();

        $securities = array();
        foreach ($pairs as $pair) {
            # code...
            $data = array(
                "id" => $pair->id,
                "pairs" => $pair->security,
                "open" => number_format($pair->close_price, 2),
                "close" => number_format($pair->previous_close, 2),
            );
            array_push($securities, $data);
        }
             $response["news"] = $news;
             $response["stock_news"] = $securities;

    	return json_encode($response);
    }
    
}#
}
