<?php

namespace CATSS\Http\Controllers;

use Illuminate\Http\Request;
use CATSS\User;
use CATSS\Account;
use CATSS\Basic;
use CATSS\Sanitize;
use CATSS\Security;
use CATSS\News;
use CATSS\Transaction;
use CATSS\Stock;
use Session;
use Auth;

class CatssApiController extends Controller
{
    // Register Users
    public function registerUser(Request $request)
    {
		// re-assgin requested data
        $token  = $request->token;
        $name   = $request->name;
        $email  = $request->email;
        $password = $request->password;

		if($token !== '6b971eac2f876685b4ff2d07ffeb545c41B756F2DCAC80BFD910D1BED0633974'){
			$data = array(
				'status' 	=> 'error',
				'message' 	=> 'Error incorrect access token',
				'data'		=> [
					'token' 	=> $token,
					'name'		=> $name,
					'email'		=> $email
				]
			);
			return $this->toJson($data);
		}else{


			// sanitize data
			// $filter_data = new Sanitize();
			// $data = $filter_data->scanInputs($data);

	        // encrypt password 
	        $password = bcrypt($password);

	        // generate unique account id
	        $account_id = "NTD".rand(000,999).rand(111,999);
	        $account_bal = 10000000.00; // set default account balance
	        $account_status = "open"; // account status set to open
	        $account_timing = time(); // account opening date

	        // check if user already register 
	        $check_user = User::where([['email', $email], ['name', $name]])->first();
	        if($check_user == null){
	        	// multible
		        $user = new User();
		        $user->account_id = $account_id;
		        $user->name = $name;
		        $user->email = $email;
		        $user->password = $password;
		        $user->save();


		        // After saving the user fetch and get user id 
		        $users = User::where('email', $email)->get();

		        // check for id and create relationship
		        foreach ($users as $user) {
		            // init account model
		            $account = new Account();
		            $account->user_id = $user->id;
		            $account->account_id = $account_id;
		            $account->account_balance = $account_bal;
		            $account->account_status = $account_status;
		            $account->timing = time();
		            $account->save();

		            // init a new basic informations
		            $basic_info = new Basic();
		            $basic_info->user_id = $user->id;
		            $basic_info->name = $user->name;
		            $basic_info->save();
		        }

		        $data = array(
	        		'status'  => 'success',
	        		'message' => 'Registration successful !'
	        	);
	        	return $this->toJson($data);
	        }else{
	        	$data = array(
	        		'status'  => 'error',
	        		'message' => 'User already registered !'
	        	);
	        	return $this->toJson($data);
	        }  
	    } 
    }

	// login User 
    public function loginUser(Request $request)
    {
    	// re-assgin requested data
        $token  = $request->token ;
        $email  = $request->email;
        $password = $request->password;
        $rememberToken = $request->remember;

		if($token !== '6b971eac2f876685b4ff2d07ffeb545c41B756F2DCAC80BFD910D1BED0633974'){
			$data = array(
				'status' 	=> 'error',
				'message' 	=> 'Error incorrect access token',
				'data'		=> [
					'token' 	=> $token,
					'email'		=> $email
				]
			);
			return $this->toJson($data);
		}else{
			if(Auth::guard('web')->attempt(['email' => $email, 'password' => $password], $rememberToken)){
				$data = array(
				'status' 	=> 'success',
				'message' 	=> 'login successful',
					'data'		=> [
						'id' 	=> Auth::user()->id,
						'email'	=> Auth::user()->email
					]
				);
				return $this->toJson($data);
			}else{
				$data = array(
					'status' 	=> 'error',
					'message' 	=> 'login fail, invalid email/password'
				);
				return $this->toJson($data);
			}
		}
    }

    // login via email
    public function loginViaEmail(Request $request)
    {
        # code...
        $email 	= $request->email;
        $user 	= User::where("email", $email)->first();
        
        // check if user exits
        if($user !== null){
        	// login using collect 
	        if(Auth::loginUsingId($user->id)){
	        	// on login successful !
	            $data = array(
	                'status'    => 'success',
	                'message'   => 'Login successful !',
	                'state'     => true
	            );

	        }else{
	           	
	           	// on login failure
	            $data = array(
	                'status'    => 'error',
	                'message'   => 'Login fail !',
	                'state'     => false
	            );
	        }
        }else{

        	// user not found
        	$data = array(
                'status'    => 'error',
                'message'   => 'Invalid email, Login fail !',
                'state'     => false
            );
        }

        // return response
        return response()->json($data);
        // return response()->json($user);
    }

    // Load the API for Securities
    public function liveMarket(Request $request, $security)
    {
    	$token = $request->token;
    	if($token !== '6b971eac2f876685b4ff2d07ffeb545c41B756F2DCAC80BFD910D1BED0633974'){
    		$data = array(
        		'status'  => 'error',
        		'message' => 'invalid token access !'
        	);
        	return $this->toJson($data);
    	}else{
    		// check for equity
	    	if($security == 'equities'){
	    		// load pairs
		        $pairs = Security::all();

		        $pair_box = [];
		        foreach ($pairs as $equity) {

		            $two_percent = 2.00;
		            $total_percent = 100;
		            $diff = $total_percent - $two_percent;
		            
		            $charge_percent = $diff / $total_percent;
		            $map_price = $charge_percent * $equity->close_price;
		            $map_diff = $equity->close_price - $map_price;

		            $real_price = $map_diff + $equity->close_price; 

		            $gap = $real_price - $equity->previous_close;

		            $data = array(
		                'id'                => $equity->id,
		                'board'             => $equity->board,
		                'security'          => $equity->security,
		                'ref_price'         => number_format($equity->ref_price, 2),
		                'open_price'        => number_format($equity->open_price, 2),
		                'high_price'        => number_format($equity->high_price, 2),
		                'low_price'         => number_format($equity->low_price, 2),
		                'close_price'       => number_format($equity->close_price, 2),
		                'change_price'      => number_format($gap, 2),
		                'daily_volume'      => number_format($equity->daily_volume, 2),
		                'daily_value'       => number_format($equity->daily_value, 2),
		                'dvmv_today'        => $equity->dvmv_today,
		                'mvtn_trade'        => $equity->mvtn_trade,
		                'previous_close'    => number_format($equity->previous_close, 2),
		                'no_of_trade'       => number_format($equity->no_of_trade),
		                'real_price'        => number_format($real_price, 2),
		                'status'            => $equity->status,
		                'date'              => $equity->created_at->toFormattedDateString()
		            );
		            array_push($pair_box, $data);
		        }

		        // return json response in array box
		        return $this->toJson($pair_box);

		        // check for bonds 
	    	}else if($security == 'bonds'){
	    		$data = array(
	        		'status'  => 'error',
	        		'message' => 'No trade type for '.$security.''
	        	);
	        	return $this->toJson($data);

	        	// check for tbills
	    	}else if($security == 'tbils'){
	    		$data = array(
	        		'status'  => 'error',
	        		'message' => 'No trade type for '.$security.''
	        	);
	        	return $this->toJson($data);
	    	}else{

	    		$data = array(
	        		'status'  => 'error',
	        		'message' => 'invalid request, check parameters'
	        	);
	        	return $this->toJson($data);
	    	}
    	}	
    }

    // cavidel live equities
    public function liveMarketCavidel()
    {

		// load pairs
        $pairs = Security::all();

        $pair_box = [];
        foreach ($pairs as $equity) {

            $two_percent = 2.00;
            $total_percent = 100;
            $diff = $total_percent - $two_percent;
            
            $charge_percent = $diff / $total_percent;
            $map_price = $charge_percent * $equity->close_price;
            $map_diff = $equity->close_price - $map_price;

            $real_price = $map_diff + $equity->close_price; 

            $gap = $real_price - $equity->previous_close;


            $data = array(
                'id'                => $equity->id,
                'board'             => $equity->board,
                'security'          => $equity->security,
                'ref_price'         => number_format($equity->ref_price, 2),
                'open_price'        => number_format($equity->open_price, 2),
                'high_price'        => number_format($equity->high_price, 2),
                'low_price'         => number_format($equity->low_price, 2),
                'close_price'       => number_format($equity->close_price, 2),
                'change_price'      => number_format($gap, 2),
                'daily_volume'      => number_format($equity->daily_volume, 2),
                'daily_value'       => number_format($equity->daily_value, 2),
                'dvmv_today'        => $equity->dvmv_today,
                'mvtn_trade'        => $equity->mvtn_trade,
                'previous_close'    => number_format($equity->previous_close, 2),
                'no_of_trade'       => number_format($equity->no_of_trade),
                'real_price'        => number_format($real_price, 2),
                'status'            => $equity->status,
                'date'              => $equity->created_at->toFormattedDateString()
            );
            array_push($pair_box, $data);
        }

        // return json response in array box
		return response()->json($pair_box);
    }

    // load users Account details
	public function userAccount(Request $request)
	{
		// re-assgin requested data
        $token  = $request->token;
        $id 	= $request->id;

		if($token !== '6b971eac2f876685b4ff2d07ffeb545c41B756F2DCAC80BFD910D1BED0633974'){
			$data = array(
				'status' 	=> 'error',
				'message' 	=> 'Error incorrect access token',
				'data'		=> [
					'token' 	=> $token,
				]
			);
			return $this->toJson($data);
		}else{
			$account_balance = Account::where('user_id', $id)->get();

	        // get user profile image
	        $basic = Basic::where('user_id', $id)->first();

	        foreach ($account_balance as $balance) {
	            # code...
	            $data = array(
	                "account_balance" => number_format($balance->account_balance, 2),
	                "profile_image" => $basic->avatar
	            );
	        }
	        return response()->json($data);
	    }
	}

	// load userTransactions Details
	public function userTransactions(Request $request)
	{
		// re-assgin requested data
        $token  = $request->token;
        $id 	= $request->id;

		if($token !== '6b971eac2f876685b4ff2d07ffeb545c41B756F2DCAC80BFD910D1BED0633974'){
			$data = array(
				'status' 	=> 'error',
				'message' 	=> 'Error incorrect access token',
				'data'		=> [
					'token' 	=> $token,
					'name'		=> $name,
					'email'		=> $email
				]
			);
			return $this->toJson($data);
		}else{
			// find transactions 
	        $all_transactions = Transaction::where('user_id', $id)->orderBy('id', 'desc')->take('1')->get();
	        
	        $transaction_box = [];
	        foreach($all_transactions as $transaction){
	            $data = array(
	                'stock_name' => $transaction->stock_name,
	                'stock_unit' => number_format($transaction->stock_unit, 2),
	                'stock_qty' => number_format($transaction->stock_qty),
	                'stock_trade' => $transaction->stock_trade,
	                'stock_amount' => number_format($transaction->stock_amount, 2),
	                'stock_date' => $transaction->created_at->diffForHumans()
	            );

	            array_push($transaction_box, $data);
	        }

	        // Json Url Response for transactions notifications
	        return response()->json($transaction_box);
	    }
	}

	// load users Stock balance 
	public function userStock(Request $request, $id)
	{
		// re-assgin requested data
        $token  = $request->token;

		if($token !== '6b971eac2f876685b4ff2d07ffeb545c41B756F2DCAC80BFD910D1BED0633974'){
			$data = array(
				'status' 	=> 'error',
				'message' 	=> 'Error incorrect access token',
				'data'		=> [
					'token' 	=> $token,
					'name'		=> $name,
					'email'		=> $email
				]
			);
			return $this->toJson($data);
		}else{
			// find transactions 
	        $all_stocks = Stock::where('user_id', $id)->orderBy('id', 'desc')->get();
	        
	        $stock_box = [];

	        foreach ($all_stocks as $stocks) {
	            if($stocks->qty > 0){
	                # code...
	                $data = array(
	                    'name' => $stocks->name,
	                    'qty'  => number_format($stocks->qty),
	                    'price' => number_format($stocks->price, 2) 
	                );
	                array_push($stock_box, $data);
	            }   
	        }

	        // Json Url Response for transactions notifications
	        return response()->json($stock_box);
	    }
	}

	// load news JSON
    public function newsUpdates(Request $request)
    {
        // fetch all recent news
        $latest_news = News::orderBy('id','desc')->take('10')->get();

        $news_box = [];
        foreach ($latest_news as $news) {
            # code...
            $data = array(
                'news_title' => $news->title,
                'news_body' => $news->body,
                'news_date' => $news->created_at->diffForHumans() 
            );

            array_push($news_box, $data);
        }

        // Return json url response for news
        return $this->toJson($news_box);
    }

    // respond with json 
    public function toJson($data)
    {
    	// set headers and respond data
    	header("Access-Control-Allow-Origin: *");
		header('Content-Type: application/json');
		echo json_encode($data);
    }
}
