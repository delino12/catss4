<?php

namespace CATSS\Http\Controllers;
use CATSS\Account;
use CATSS\Demotrade;
use CATSS\User;
use CATSS\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class DemoTradeController extends Controller {

	public function __construct() {
		$this->middleware('auth');
	}

	public function startDemo() {
		$user_id = User::find(Auth::user()->id);
		$id = $user_id->id;

		// fetch account information
		$account_info = Account::where('user_id', $id)->get();

		// load pairs
		$pairs = Demotrade::orderBy('equity', 'desc')->get();

		return view('internal-pages.demo-setup-live', compact('pairs', 'account_info'));
	}

	public function demo() {

		$user_id = User::find(Auth::user()->id);
		$id = $user_id->id;

		// fetch account information
		$account_info = Account::where('user_id', $id)->get();

		// load pairs
		$pairs = Demotrade::orderBy('equity', 'desc')->get();

		$msg = "You are trading on CATSS ";
		Session::flash('flash_msg', $msg);

		return view('internal-pages.demo-ntrade-live', compact('pairs', 'account_info'));
	}

	public function createTradeGroup(Request $request, Group $groups) {
		// auth data
		$user_id = Auth::user()->id;
		// form data
		$name = $request->group_name;
		$level = $request->group_level;
		$members = $request->group_memebers;
		// generated data
		$status = 'open';
		$link = 'http://catss.com/'.$request->_token;

		// check if group is already create 
		$already_exits = Group::where('name', $name)->first();
		if($already_exits !== null){
			$msg = $name." already_exits !";	
			return redirect()->back()->with("error_status", $msg);
		}else{

			// save and create trade group
			$groups = new Group();
			$groups->user_id = $user_id;
			$groups->name = $name;
			$groups->level = $level;
			$groups->members = $members;
			$groups->status = $status;
			$groups->links = $link;
			$groups->save();

			// fetch group id from group also create new trading board


			$msg = $name." successfully created !, invitations link has been sent !";	
			return redirect()->back()->with("update_status", $msg);
		}
	}
}
