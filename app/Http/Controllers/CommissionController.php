<?php

namespace CATSS\Http\Controllers;

use Illuminate\Http\Request;
use CATSS\Commission;
use CATSS\Account;
use CATSS\User;

class CommissionController extends Controller
{
    // load commission 
    public function all_charges()
    {
    	# load all charges
    	$commission = Commission::orderBy('id', 'desc')->get();
        $count_commission = Commission::all();

    	$com_box = [];
    	$total_charges = [];
    	foreach ($commission as $fee) {
    		$account_code = Account::where('user_id', $fee->user_id)->first();
            $user_info = User::where('id', $fee->user_id)->first();
    		# code...
    		$data = array(
    			'id'      => $fee->id,
                'name'    => $user_info->name,
    			'acc_id'  => $account_code->account_id,
    			'acc_bal' => number_format($account_code->account_balance, 2),
    			'percent' => number_format($fee->percent, 4),
    			'qty'     => number_format($fee->qty),
    			'type'    => $fee->type,
    			'amount'  => number_format($fee->amount, 2),
    			'date'    => $fee->created_at->diffForHumans()
    		);

    		$charges = array(
    			'amount' => $fee->amount
    		);

    		array_push($com_box, $data);
    		array_push($total_charges, $charges);
    	}

    	// collect total charges
    	$total = collect($total_charges);
    	$total = $total->sum('amount');
    	$count = count($count_commission);

        // 
    	$data = array(
    		'details' => $com_box,
    		'total' => number_format($total, 2),
    		'count' => $count
    	);

    	// return response
    	return response()->json($data);
    }
}
