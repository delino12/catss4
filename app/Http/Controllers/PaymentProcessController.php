<?php

namespace CATSS\Http\Controllers;

use Illuminate\Http\Request;
use CATSS\Account;
use CATSS\Transaction;
use CATSS\Stock;
use CATSS\Statement;
use CATSS\Payment;
use CATSS\Vault;
use Auth;

class PaymentProcessController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    // start process 
    public function startProcess(Request $request)
    {
    	# code...
    	$amount = $request->email;
    	$email  = $request->amount;

    	return view('internal-pages.payment-process', compact('email', 'amount'));
    }

	// start process 
    public function logPayment(Request $request)
    {
    	// user id
    	$id = Auth::user()->id;

    	# code...
    	$ref_id = $request->refId;
    	$amount = $request->amount;
    	$email  = $request->email;
    	$status = 'paid';


        # reset account 
        $account = Account::where('user_id', $id)->first();

        // reset account balance
        $reset_account                  = Account::find($account->id);
        $reset_account->account_balance = $amount + $reset_account->account_balance;
        $reset_account->update();

        // update double payment
        $payment = Payment::where('user_id', $id)->first();

        if($payment == null){

            // record payment quota
            $log_payments           = new Payment();
            $log_payments->trans_id = $ref_id;
            $log_payments->user_id  = $id;
            $log_payments->amount   = $amount;
            $log_payments->balance  = $amount;
            $log_payments->quota    = 4;
            $log_payments->status   = $status;
            $log_payments->save();

        }else{
            $log_payments = Payment::find($payment->id);
            $log_payments->trans_id = $ref_id;
            $log_payments->amount   = $log_payments->amount + $amount;
            $log_payments->balance  = $log_payments->balance + $amount;
            $log_payments->quota    = $log_payments->quota + 4;
            $log_payments->update();
        }

    	
    	// vault type
    	$type = 'deposit';

    	// log vault
    	$log_vault          = new Vault();
    	$log_vault->user_id = $id;
    	$log_vault->type    = $type;
    	$log_vault->amount  = $amount;
    	$log_vault->save();

        // reset account 
        // $this->refreshTrade($id);

    	// response message
    	$data = array(
    		'status'  => 'success',
    		'message' => 'Payment successful !'
    	);

    	// return response
    	return response()->json($data);
    }


    // reset account information
    public function refreshTrade($id)
    {
        # reset account 
        $account = Account::where('user_id', $id)->first();

        // reset account balance
        $reset_account = Account::find($account->id);
        $reset_account->account_balance = 100000000;
        $reset_account->update();

        // delete all transaction
        $transactions = Transaction::where('user_id', $id)->get();
        if(count($transactions) > 0){
            foreach ($transactions as $trans) {
                # code...
                $delete_transaction = Transaction::find($trans->id);
                $delete_transaction->delete();
            }
        }

        // delete all stocks
        $stocks = Stock::where('user_id', $id)->get();
        if(count($stocks) > 0){
            foreach ($stocks as $stock) {
                # code...
                $delete_stock = Stock::find($stock->id);
                $delete_stock->delete();
            }
        }

        // delete all statement
        $statements = Statement::where('user_id', $id)->get();
        if(count($statements) > 0){
            foreach ($statements as $stat) {
                # code...
                $delete_statement = Statement::find($stat->id);
                $delete_statement->delete();
            }
        }
    }

    // load payment balance 
    public function loadBalance()
    {
    	# code...
    	$id = Auth::user()->id; 
    	$payments = Payment::where('user_id', $id)->first();

    	// return balance
    	return response()->json($payments);
    }
}
