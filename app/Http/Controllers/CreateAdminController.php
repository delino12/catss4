<?php

namespace CATSS\Http\Controllers;

use CATSS\Admin;

class CreateAdminController extends Controller {
	//
	public function createAdmin(Admin $admin) {
		// create default admin
		$admin_name = "Bernard Cavidel";
		$admin_email = "admin@ntrade.com";
		$admin_password = "password12345";
		$admin_level = "omega";

		// store admin
		$admin = new Admin;
		$admin->name = $admin_name;
		$admin->email = $admin_email;
		$admin->password = bcrypt($admin_password);
		$admin->level = $admin_level;
		$admin->save();

		return redirect('/admin/credentials');
	}

	public function showDetails(Admin $details) {
		// return view with admin login details
		$details = Admin::all();

		return view('install.install', compact('details'));
	}
}
