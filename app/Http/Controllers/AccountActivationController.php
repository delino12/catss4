<?php

namespace CATSS\Http\Controllers;

use Illuminate\Http\Request;
use CATSS\Mail\WelcomeMail;
use CATSS\Mail\NewUser;
use CATSS\Activation;
use Auth;

class AccountActivationController extends Controller
{
    // Activate Users Accounts
    public function activateByLink(Request $request)
    {
    	// credentials
    	$id   = $request->id;
    	$code = $request->code;

    	if(!is_numeric($id) && !is_numeric($code)){
    		return redirect('/');
    	}else{
    		// activated the users account
    		$activate = Activation::where([['id', $id], ['token', $code]])->first();
    		if($activate == null){
    			return redirect('/');
    		}else{
    			$activate_account = Activation::find($id);
    			$activate_account->status = 'active';
    			$activate_account->update();

    			return redirect('/');
    		}
    	}
    }

    // Activate Users Accounts by Code
    public function activateByCode(Request $request)
    {
        //logged user
        $code = $request->code;
        $email = Auth::user()->email;

        // activated the users account
        $activate = Activation::where([['email', $email], ['token', $code]])->first();
        if($activate == null){
            return redirect()->back();
        }else{
            $activate_account = Activation::find($activate->id);
            $activate_account->status = 'active';
            $activate_account->update();
        }
        return redirect()->back();
    }

    // resend activation code
    public function resendActivationCode()
    {
        $email = Auth::user()->email;
        $name  = Auth::user()->name;

        // check if user has activation link already
        $activation_link = Activation::where('email', $email)->first();
        if($activation_link !== null){

            // update existing link
            $update_activation              = Activation::find($activation_link->id);
            $update_activation->token       = rand(000, 999).'-'.rand(111, 555);
            $update_activation->status      = 'inactive';
            $update_activation->update();

            // get the users activations code details
            $activation_code = Activation::where('email', $email)->first();

            // build res data
            $data = array(
                'id' => $activation_code->id,
                'name' => $name,
                'code' => $activation_code->token
            );

            // send User an Email
            \Mail::to($email)->send(new NewUser($data));

        }else{

            // set account for activations
            $activate           = new Activation();
            $activate->email    = $email;
            $activate->token    = rand(000, 999).'-'.rand(111, 555);
            $activate->status   = 'inactive';
            $activate->save();

            // get the users activations code details
            $activation_code = Activation::where('email', $email)->first();

            // build res data
            $data = array(
                'id' => $activation_code->id,
                'name' => $name,
                'code' => $activation_code->token
            );

            // send User an Email
            \Mail::to($email)->send(new NewUser($data));
        }

        // return redirect back
        return redirect()->back()->with('success_msg', 'Activation link has been sent !');
    }
}
