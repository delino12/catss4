<?php

namespace CATSS\Http\Controllers\Auth;

use Illuminate\Http\Request;
use CATSS\Http\Controllers\Controller;
use CATSS\Basic;
use CATSS\User;
use Spatie\Activitylog\Models\Activity;
use Auth;

class LoginController extends Controller
{
    protected $redirectTo = '/dashboard';
    
    public function __construct(){
        $this->middleware('guest')->except('logoutUsers');
    }

    public function showlogin(){
        return view('internal-pages.login');
    }

    public function showLoginForm(){
        return view('internal-pages.login');
    }

    public function loginUser(Request $request){
        // filter and re-assign request
        $username       = $request->email;
        $password       = $request->password;
        $rememberToken  = $request->remember;

        // Attemp to logged the user in
        if(Auth::guard('web')->attempt(['email' => $username, 'password' => $password], $rememberToken)){

            $log_message = 'successfully logged in to CATSS !';
            activity()->causedBy(Auth::user()->id)->log($log_message);

            return redirect()->intended('/dashboard');
        }else{
            
            $log_message = $username.' attempted to login !!';
            activity()->log($log_message);

            return redirect()
                ->back()
                ->withInput($request->only('email', 'remember'))
                ->with('error_status', 'Fail to login, please check your login credentials, CaSE-seNsiTive  ');
        }
    }

    // login via phone number
    public function loginViaPhone(Request $request){
        # code...
        $phone          = $request->phone;
        $rememberToken  = $request->remember;

        // phone 
        $basic_info = Basic::where('phone', $phone)->first();
        if($basic_info !== null){
            $user   = User::where('id', $basic_info->user_id)->first();
            $email  = $user->email;

            // Attemp to logged the user in
            if(Auth::login($user)){
                $data = array(
                    'status'    => 'success', 
                    'message'   => 'login successful !', 
                    'state'     => true 
                );
            }else{
                
                $data = array(
                    'status'    => 'error', 
                    'message'   => 'fail to login !', 
                    'state'     => false
                );
            }
        }else{

            $data = array(
                'status'    => 'error', 
                'message'   => 'Phone number does not exits !', 
                'state'     => false
            );
        }

        // return response
        return response()->json($data);
    }

    // login via facebook 
    public function loginViaEmail(Request $request){
        # code...
        $email  = $request->email;
        $user   = User::where("email", $email)->first();
        
        // check if user exits
        if($user !== null){
            // login using collect 
            if(Auth::loginUsingId($user->id)){
                // on login successful !
                $data = array(
                    'status'    => 'success',
                    'message'   => 'Login successful !',
                    'state'     => true
                );

            }else{
                
                // on login failure
                $data = array(
                    'status'    => 'error',
                    'message'   => 'Login fail !',
                    'state'     => false
                );
            }
        }else{

            // user not found
            $data = array(
                'status'    => 'info',
                'message'   => 'Invalid email, Login fail !',
                'state'     => false
            );
        }

        // return response
        return response()->json($data);
    }

    // signout admin
    public function logoutUsers(){
        $log_message = 'Logout from CATSS';
        activity()->causedBy(Auth::user()->id)->log($log_message);
        
        Auth::guard('web')->logout();
        return redirect('/login-account');
    }
}
