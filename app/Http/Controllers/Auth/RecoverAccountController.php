<?php

namespace CATSS\Http\Controllers\Auth;

use Illuminate\Http\Request;
use CATSS\Http\Controllers\Controller;
use CATSS\User;

class RecoverAccountController extends Controller
{
    // recover account information
    public function recoverAccount(){
    	return view("internal-pages.recover-account");
    }

    // search account information
    public function searchAccount(Request $request){
    	// find mail 
    	$email = $request->email;
    	$user = User::where('email', $email)->get();

    	$total = count($user);
    	if($total > 0){
    		$msg = 'Password reset link has been sent to '.$email;
    	}else{
    		$msg = $email.' does not exit, please check the email and try again ';
    	}

    	/*
    		generate link
    		store a token,
    		generate two way authentication

    	*/
    	return redirect()->back()->with('search_status', $msg);
    }
}
