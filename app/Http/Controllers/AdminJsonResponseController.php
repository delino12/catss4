<?php

namespace CATSS\Http\Controllers;

use Illuminate\Http\Request;
use CATSS\Order;
use CATSS\Transaction;
use CATSS\User;
use CATSS\Security;
use CATSS\Firm;
use CATSS\Account;
use Auth;

class AdminJsonResponseController extends Controller
{
	/*
    |-----------------------------------------
    | authenticate admin route
    |-----------------------------------------
    */
	public function __construct() {
		$this->middleware('auth:admin');
	}

    /*
    |-----------------------------------------
    | load all catss orders
    |-----------------------------------------
    */
    public function loadOrders(){
    	// body
    	$orders = Order::orderBy('id', 'DESC')->get();
    	if(count($orders) > 0){
    		$order_box = [];
    		foreach ($orders as $el) {

    			$user 		= User::where('id', $el->user_id)->first();
    			$security 	= Security::where('id', $el->sec_id)->first();

    			$data = [
    				'id' 		=> $el->id,
    				'sec_id' 	=> $el->sec_id,
    				'user_id' 	=> $el->user_id,
    				'ref_id' 	=> $el->ref_id,
    				'price' 	=> number_format($el->price, 2),
    				'qty' 		=> number_format($el->qty),
    				'amount'    => number_format($el->price * $el->qty, 2),
    				'trade_type'=> ucwords($el->type),
    				'option' 	=> ucwords($el->option),
    				'last_seen' => $el->created_at->diffForHumans(),
    				'date'      => $el->created_at,
    				'owner' 	=> ucwords($user->name),
    				'security'  => $security->security
    			];

    			// fixed order box
    			array_push($order_box, $data);
    		}
    	}else{
    		$order_box = [];
    	}

    	// return response.
    	return response()->json($order_box);
    }

    /*
    |-----------------------------------------
    | load all catss orders
    |-----------------------------------------
    */
    public function loadTransactions(){
    	// body
    	$transactions = Transaction::orderBy('id', 'DESC')->get();
    	if(count($transactions) > 0){
    		$order_box = [];
    		foreach ($transactions as $el) {
    			$user = User::where('id', $el->user_id)->first();
    			$data = [
    				'id' 		=> $el->id,
    				'last_seen' => $el->created_at->diffForHumans(),
    				'date'      => $el->created_at,
    				'owner' 	=> ucwords($user->name),
					'user_id'	=> $el->user_id,
					'security'	=> $el->stock_name,
					'price'		=> number_format($el->stock_unit, 2),
					'qty'		=> number_format($el->stock_qty),
					'trade_type'=> ucwords($el->stock_trade),
					'amount'	=> number_format($el->stock_amount, 2)				
    			];

    			// fixed order box
    			array_push($order_box, $data);
    		}
    	}else{
    		$order_box = [];
    	}

    	// return response.
    	return response()->json($order_box);
    }


    /*
    |-----------------------------------------
    | load all stock firms 
    |-----------------------------------------
    */
    public function stockFirms(){
        // body
        $stock_firms    = new Firm();
        $data           = $stock_firms->getAllFirm();

        // return response.
        return response()->json($data);
    }


    /*
    |-----------------------------------------
    | save new broker
    |-----------------------------------------
    */
    public function saveBroker(Request $request){
        // body
        $broker     = new User();
        $data       = $broker->saveNewBroker($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | save new broker
    |-----------------------------------------
    */
    public function saveClient(Request $request){
        // body
        $broker     = new User();
        $data       = $broker->saveNewClient($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | get all brokers
    |-----------------------------------------
    */
    public function allClients(){
        // body
        $broker     = new User();
        $data       = $broker->getAllClients();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | get all brokers
    |-----------------------------------------
    */
    public function allBrokerAgent(){
        // body
        $broker     = new User();
        $data       = $broker->getAllBrokers();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | get all firms
    |-----------------------------------------
    */
    public function loadAllFirms(){
        // body
        $broker     = new User();
        $data       = $broker->getAllBrokers();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | list Firms
    |-----------------------------------------
    */
    public function listFirms(){
        // body
        $stock_firms    = new Firm();
        $data           = $stock_firms->getFirmNames();

        // return response.
        return response()->json($data);
    }


    /*
    |-----------------------------------------
    | list all users
    |-----------------------------------------
    */
    public function listAllUsers(){
        // body
        $users  = new User();
        $data   = $users->getAllUsers();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | block user account
    |-----------------------------------------
    */
    public function blockUserAccount(Request $request){
        // body
        $account    = new Account();
        $data       = $account->lock($request->user_id);

        // return response.
        return response()->json($data);
    } 

    /*
    |-----------------------------------------
    | Unblock user account
    |-----------------------------------------
    */
    public function unblockUserAccount(Request $request){
        // body
        $account    = new Account();
        $data       = $account->unlock($request->user_id);

        // return response.
        return response()->json($data);
    } 
}
